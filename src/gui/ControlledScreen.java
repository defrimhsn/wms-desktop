/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

/**
 *
 * @author deathrow
 */
@FunctionalInterface
public interface ControlledScreen {

    //This method will allow the injection of the Parent ScreenPane

    /**
     *
     * @param screenPage
     */
    public void setScreenParent(ScreensController screenPage);
}
