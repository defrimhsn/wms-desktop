/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Deathrow
 */
public class WarehouseMain extends Application {

    public static final String LOGIN = "Login";
    public static final String MAINWINDOW = "MainWindow";

    public static final String LOGINFILE = "fxml/Login.fxml";
    public static final String MAINWINDOWFILE = "fxml/MainWindow.fxml";

    @Override
    public void start(Stage primaryStage) throws Exception {

        ScreensController controller = new ScreensController();
        controller.loadScreen(LOGIN, LOGINFILE);
        controller.loadScreen(MAINWINDOW, MAINWINDOWFILE);
        controller.setScreen(MAINWINDOW);

        Group root = new Group();
        root.getChildren().add(controller);

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);
        primaryStage.setMinWidth(1366);
        primaryStage.setMinHeight(768);
        primaryStage.setMaxWidth(1366);
        primaryStage.setMaxHeight(768);
        primaryStage.setTitle("Warehouse Management System");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
