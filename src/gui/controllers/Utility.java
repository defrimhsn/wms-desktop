/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.Category;
import jpa.CategoryDetailsJpaController;
import jpa.CategoryJpaController;
import jpa.GroupsJpaController;
import jpa.Location;
import jpa.LocationJpaController;
import jpa.PermissionsJpaController;
import jpa.ProductDetails;
import jpa.ProductDetailsJpaController;
import jpa.ProductJpaController;
import jpa.ProductUnitJpaController;
import jpa.SalesJpaController;
import jpa.Sector;
import jpa.SectorDetails;
import jpa.SectorDetailsJpaController;
import jpa.SectorJpaController;
import jpa.Shelf;
import jpa.ShelfDetailsJpaController;
import jpa.ShelfJpaController;
import jpa.StockJpaController;
import jpa.UnitJpaController;
import jpa.UserJpaController;
import jpa.Warehouse;
import jpa.WarehouseDetailsJpaController;
import jpa.WarehouseJpaController;

/**
 *
 * @author Deathrow
 */
public class Utility {

    public static final EntityManagerFactory EMF = Persistence.createEntityManagerFactory("WarehouseV2PU");
    public static final LocationJpaController LOCATIONJPACONTROLLER = new LocationJpaController(EMF);
    public static final WarehouseJpaController WAREHOUSEJPACONTROLLER = new WarehouseJpaController(EMF);
    public static final SectorJpaController SECTORJPACONTROLLER = new SectorJpaController(EMF);
    public static final ShelfJpaController SHELFJPACONTROLLER = new ShelfJpaController(EMF);
    public static final CategoryJpaController CATEGORYJPACONTROLLER = new CategoryJpaController(EMF);
    public static final ProductJpaController PRODUCTJPACONTROLLER = new ProductJpaController(EMF);
    public static final StockJpaController STOCKJPACONTROLLER = new StockJpaController(EMF);
    public static final GroupsJpaController GROUPSJPACONTROLLER = new GroupsJpaController(EMF);
    public static final UserJpaController USERJPACONTROLLER = new UserJpaController(EMF);
    public static final PermissionsJpaController PERMISSIONSJPACONTROLLER = new PermissionsJpaController(EMF);
    public static final WarehouseDetailsJpaController WAREHOUSEDETAILSJPACONTROLLER = new WarehouseDetailsJpaController(EMF);
    public static final SectorDetailsJpaController SECTORDETAILSJPACONTROLLER = new SectorDetailsJpaController(EMF);
    public static final ShelfDetailsJpaController SHELFDETAILSJPACONTROLLER = new ShelfDetailsJpaController(EMF);
    public static final CategoryDetailsJpaController CATEGORYDETAILSJPACONTROLLER = new CategoryDetailsJpaController(EMF);
    public static final ProductDetailsJpaController PRODUCTDETAILSJPACONTROLLER = new ProductDetailsJpaController(EMF);
    public static final UnitJpaController UNITJPACONTROLLER = new UnitJpaController(EMF);
    public static final ProductUnitJpaController PRODUCTUNITJPACONTROLLER = new ProductUnitJpaController(EMF);
    public static final SalesJpaController SALESJPACONTROLLER = new SalesJpaController(EMF);

    public static Date nowDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String now = dateFormat.format(date);
        try {
            date = dateFormat.parse(now);
        } catch (ParseException ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return date;
    }

    public static String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static LocalDate fromDate(Date date) {
        Instant instant = Instant.ofEpochMilli(date.getTime());
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
    }

    public static boolean hasSomethingActive(Object obj) {

        boolean hasSomethingActive = false;
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WarehouseV2PU");
        WarehouseJpaController warehouseJpaController = new WarehouseJpaController(emf);
        SectorJpaController sectorJpaController = new SectorJpaController(emf);
        CategoryJpaController categoryJpaController = new CategoryJpaController(emf);
        ShelfJpaController shelfJpaController = new ShelfJpaController(emf);
        ProductJpaController productJpaController = new ProductJpaController(emf);

        if (obj instanceof Location) {
            hasSomethingActive = warehouseJpaController.findWarehouseEntities().stream().filter(wa -> (wa.getLocationId().equals((Location) obj))).count() > warehouseJpaController.findWarehouseEntities().stream().filter(temp -> (temp.getIsActive() == 0 && temp.getLocationId().equals((Location) obj))).count();
        } else if (obj instanceof Warehouse) {
            hasSomethingActive = sectorJpaController.findSectorEntities().stream().filter(sec -> (sec.getWarehouseId().equals((Warehouse) obj))).count() > sectorJpaController.findSectorEntities().stream().filter(sec -> (sec.getWarehouseId().equals((Warehouse) obj) && sec.getIsActive() == 0)).count();
        } else if (obj instanceof Sector) {
            hasSomethingActive = shelfJpaController.findShelfEntities().stream().filter(sh -> (sh.getSectorId().equals((Sector) obj))).count() > shelfJpaController.findShelfEntities().stream().filter(sh -> (sh.getIsActive() == 0 && sh.getSectorId().equals((Sector) obj))).count() || categoryJpaController.findCategoryEntities().stream().filter(cat -> (cat.getSectorId().equals((Sector) obj))).count() > categoryJpaController.findCategoryEntities().stream().filter(sh -> (sh.getIsActive() == 0 && sh.getSectorId().equals((Sector) obj))).count();
        } else if (obj instanceof Shelf) {
            hasSomethingActive = productJpaController.findProductEntities().stream().filter(product -> (product.getShelfId().equals((Shelf) obj))).count() > productJpaController.findProductEntities().stream().filter(pr -> (pr.getIsActive() == 0 && pr.getShelfId().equals((Shelf) obj))).count();
        } else if (obj instanceof Category) {
            hasSomethingActive = productJpaController.findProductEntities().stream().filter(product -> (product.getCategoryId().equals((Category) obj))).count() > productJpaController.findProductEntities().stream().filter(pr -> (pr.getIsActive() == 0 && pr.getCategoryId().equals((Category) obj))).count();
        }

        return hasSomethingActive;
    }

    public static void validateControl(Control control) {
        if (control instanceof TextField) {
            ((TextField) control).textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                if (((TextField) control).getText().trim().isEmpty() || ((TextField) control).getText().trim().length() < 3) {
                    ((TextField) control).setStyle("-fx-border-style: solid; -fx-border-width:1px; -fx-border-color:red; -fx-border-radius:3px;");
                } else {
                    ((TextField) control).setStyle("-fx-border-style: solid; -fx-border-width:1px; -fx-border-color:green; -fx-border-radius:3px;");
                }
            });
        } else if (control instanceof TextArea) {
            ((TextArea) control).textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                if (((TextArea) control).getText().trim().isEmpty()) {
                    ((TextArea) control).setStyle("-fx-border-style: solid; -fx-border-width:1px; -fx-border-color:red; -fx-border-radius:3px;");
                } else {
                    ((TextArea) control).setStyle("-fx-border-style: solid; -fx-border-width:1px; -fx-border-color:green; -fx-border-radius:3px;");
                }
            });
        } else if (control instanceof ComboBox) {
            ComboBox comboBox = (ComboBox) control;
            comboBox.selectionModelProperty().addListener(new ChangeListener() {
                @Override
                public void changed(ObservableValue observable, Object oldValue, Object newValue) {

                }
            });
        }
    }

    public static void validateTextField(TextField textField, boolean allowNumbers) {
        if (allowNumbers) {
            UnaryOperator<Change> allowNum = (Change t) -> {
                String s = t.getText();
                if (s.matches("[0-9]*")) {
                    return t;
                }

                return null;
            };
            textField.setTextFormatter(new TextFormatter<>(allowNum));
        } else {
            UnaryOperator<Change> allowLetters = (Change t) -> {
                String s = t.getText();
                if (s.matches("[a-z|A-Z| ]*")) {
                    return t;
                }

                return null;
            };
            textField.setTextFormatter(new TextFormatter<>(allowLetters));
        }
    }

}
