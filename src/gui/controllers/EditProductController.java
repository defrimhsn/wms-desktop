/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import static gui.controllers.ProductController.editUnit;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.Category;
import jpa.CategoryJpaController;
import jpa.ProductDetailsJpaController;
import jpa.ProductJpaController;
import jpa.ProductUnitJpaController;
import jpa.Sector;
import jpa.SectorJpaController;
import jpa.Shelf;
import jpa.ShelfJpaController;
import jpa.Unit;
import jpa.UnitJpaController;
import jpa.Warehouse;
import jpa.WarehouseJpaController;
import jpa.exceptions.NonexistentEntityException;

/**
 * FXML Controller class
 *
 * @author defri
 */
public class EditProductController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Pane pane;

    @FXML
    private TextField barcodeTextField;

    @FXML
    private TextField nameTextField;

    @FXML
    private ComboBox<Warehouse> chooseWarehouseComboBox;

    @FXML
    private ComboBox<Sector> chooseSectorComboBox;

    @FXML
    private ComboBox<Category> chooseCategoryComboBox;

    @FXML
    private ComboBox<Shelf> chooseShelfComboBox;

    @FXML
    private DatePicker expirationDatePicker;

    @FXML
    private TextField priceTextField;

    @FXML
    private TextArea descriptionTextArea;

    @FXML
    private Button addButton;

    @FXML
    private Button cancelButton;

    @FXML
    private ComboBox<Unit> chooseUnit;

    private WarehouseJpaController warehouseJpaController;
    private SectorJpaController sectorJpaController;
    private ShelfJpaController shelfJpaController;
    private CategoryJpaController categoryJpaController;
    private ProductJpaController productJpaController;
    private ProductDetailsJpaController productDetailsJpaController;
    private ProductUnitJpaController productUnitJpaController;
    private UnitJpaController unitJpaController;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        transaction();
        loadCombos();
        combosListener();
        loadUnits();
        fillFields();
        try {
            editProducts();
        } catch (Exception ex) {
            Logger.getLogger(EditProductController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void transaction() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WarehouseV2PU");
        warehouseJpaController = new WarehouseJpaController(emf);
        sectorJpaController = new SectorJpaController(emf);
        shelfJpaController = new ShelfJpaController(emf);
        categoryJpaController = new CategoryJpaController(emf);
        productJpaController = new ProductJpaController(emf);
        productDetailsJpaController = new ProductDetailsJpaController(emf);
        productUnitJpaController = new ProductUnitJpaController(emf);
        unitJpaController = new UnitJpaController(emf);
    }

    private void loadCombos() {
        ObservableList<Warehouse> list = FXCollections.observableArrayList();
        warehouseJpaController.findWarehouseEntities().stream().filter((warehouse) -> (warehouse.getIsActive() == 1)).forEach((warehouse) -> {
            list.add(warehouse);
        });
        chooseWarehouseComboBox.setItems(list);
    }

    private void combosListener() {
        chooseWarehouseComboBox.setOnAction((ActionEvent event) -> {
            Warehouse choosedWarehouse = chooseWarehouseComboBox.getSelectionModel().getSelectedItem();

            ObservableList<Sector> sec = FXCollections.observableArrayList();
            sectorJpaController.findSectorEntities().stream().filter((sector) -> (sector.getIsActive() == 1 && sector.getWarehouseId().equals(choosedWarehouse))).forEach((sector) -> {
                sec.add(sector);
            });
            chooseSectorComboBox.setItems(sec);

            chooseSectorComboBox.setOnAction((ActionEvent event1) -> {
                Sector choosedSector = chooseSectorComboBox.getSelectionModel().getSelectedItem();
                ObservableList<Shelf> shelves = FXCollections.observableArrayList();
                ObservableList<Category> categories = FXCollections.observableArrayList();
                shelfJpaController.findShelfEntities().stream().filter((sh) -> (sh.getIsActive() == 1 && sh.getSectorId().equals(choosedSector))).forEach((sh) -> {
                    shelves.add(sh);
                });
                chooseShelfComboBox.setItems(shelves);
                categoryJpaController.findCategoryEntities().stream().filter((c) -> (c.getIsActive() == 1 && c.getSectorId().equals(choosedSector))).forEach((c) -> {
                    categories.add(c);
                });
                chooseCategoryComboBox.setItems(categories);
            });
        });
    }

    private void loadUnits() {
        chooseUnit.setItems(FXCollections.observableArrayList(unitJpaController.findUnitEntities()));
    }

    private void fillFields() {
        if (ProductController.choosedProducttoEdit != null) {
            chooseWarehouseComboBox.setValue(ProductController.choosedProducttoEdit.getShelfId().getSectorId().getWarehouseId());
            chooseSectorComboBox.setValue(ProductController.choosedProducttoEdit.getShelfId().getSectorId());
            chooseShelfComboBox.setValue(ProductController.choosedProducttoEdit.getShelfId());
            chooseCategoryComboBox.setValue(ProductController.choosedProducttoEdit.getCategoryId());
            chooseUnit.setValue(ProductController.editUnit);
            barcodeTextField.setText(ProductController.choosedProducttoEdit.getBarcode());
            nameTextField.setText(ProductController.choosedProducttoEdit.getName());
            expirationDatePicker.setValue(Utility.fromDate(ProductController.choosedProducttoEdit.getExpiredDate()));
            priceTextField.setText(ProductController.choosedProducttoEdit.getPrice() + "");
            descriptionTextArea.setText(ProductController.choosedProducttoEdit.getDescription());
        }
    }

    private void editProducts() throws NonexistentEntityException, Exception {
        addButton.setOnAction(e -> {
            if (check()) {
                try {
                    ProductController.choosedProducttoEdit.setName(nameTextField.getText());
                    ProductController.choosedProducttoEdit.setBarcode(barcodeTextField.getText());
                    ProductController.choosedProducttoEdit.setIsActive(1);
                    ProductController.choosedProducttoEdit.setCreatedDate(Utility.nowDate());
                    ProductController.choosedProducttoEdit.setPrice(Double.parseDouble(priceTextField.getText()));
                    ProductController.choosedProducttoEdit.setShelfId(chooseShelfComboBox.getSelectionModel().getSelectedItem());
                    ProductController.choosedProducttoEdit.getShelfId().setSectorId(chooseSectorComboBox.getValue());
                    ProductController.choosedProducttoEdit.getShelfId().getSectorId().setWarehouseId(chooseWarehouseComboBox.getSelectionModel().getSelectedItem());
                    ProductController.choosedProducttoEdit.setExpiredDate(java.sql.Date.valueOf(expirationDatePicker.getValue()));
                    ProductController.choosedProducttoEdit.setCategoryId(chooseCategoryComboBox.getSelectionModel().getSelectedItem());
                    ProductController.choosedProducttoEdit.setDescription(descriptionTextArea.getText());
                    ProductController.choosedProducttoEdit.setUpdatedDate(Utility.nowDate());
                    productJpaController.edit(ProductController.choosedProducttoEdit);
                    Stage s = (Stage) pane.getScene().getWindow();
                    s.close();
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(EditProductController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(EditProductController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        cancelButton.setOnAction(ev -> {
            Stage s = (Stage) pane.getScene().getWindow();
            s.close();
        });

    }

    private boolean check() {
        if (chooseWarehouseComboBox.getSelectionModel().getSelectedItem() == null) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please choose a warehouse!");
            al.showAndWait();
            return false;
        } else if (chooseSectorComboBox.getSelectionModel().getSelectedItem() == null) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please choose a sector!");
            al.showAndWait();
            return false;
        } else if (chooseShelfComboBox.getSelectionModel().getSelectedItem() == null) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please choose a shelf!");
            al.showAndWait();
            return false;
        } else if (chooseCategoryComboBox.getSelectionModel().getSelectedItem() == null) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please choose a category!");
            al.showAndWait();
            return false;
        } else if (barcodeTextField.getText() == null || barcodeTextField.getText().trim().isEmpty()) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please enter a barcode!");
            al.showAndWait();
            return false;
        } else if (nameTextField.getText() == null || nameTextField.getText().trim().isEmpty()) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please enter a name!");
            al.showAndWait();
            return false;
        } else if (expirationDatePicker.getValue() == null) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please select the expiration date of this product!");
            al.showAndWait();
            return false;
        } else if (priceTextField.getText() == null || priceTextField.getText().trim().isEmpty()) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please enter the price for this product!");
            al.showAndWait();
            return false;
        } else if (chooseUnit.getSelectionModel().getSelectedItem() == null) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please choose a unit!");
            al.showAndWait();
            return false;

        }
        return true;
    }

    private void clearAddFields() {
        chooseWarehouseComboBox.setValue(null);
        chooseSectorComboBox.setValue(null);
        chooseShelfComboBox.setValue(null);
        chooseCategoryComboBox.setValue(null);
        nameTextField.clear();
        barcodeTextField.clear();
        priceTextField.clear();
        expirationDatePicker.setValue(null);
        descriptionTextArea.clear();
    }

}
