/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.CategoryDetails;
import jpa.CategoryDetailsJpaController;
import jpa.ProductDetails;
import jpa.ProductDetailsJpaController;
import jpa.SectorDetails;
import jpa.SectorDetailsJpaController;
import jpa.ShelfDetails;
import jpa.ShelfDetailsJpaController;
import jpa.WarehouseDetails;
import jpa.WarehouseDetailsJpaController;

/**
 * FXML Controller class
 *
 * @author defri
 */
public class ViewDetailsController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private AnchorPane anch;

    @FXML
    public TableView<Details> tableView;

    @FXML
    private TableColumn<Details, String> nameCol;

    @FXML
    private TableColumn<Details, String> valueCol;

    @FXML
    private Button addButton;

    @FXML
    private Button cancelButton;

    @FXML
    private MenuItem editMenuItem;

    @FXML
    private MenuItem deleteMenuItem;

    @FXML
    private Label infoLabel;

    @FXML
    private Label label;

    @FXML
    private Button addSecondButton;

    @FXML
    private Button cancelSecondButton;

    @FXML
    private Pane addPane;

    @FXML
    private TextField addDetailsName;

    @FXML
    private TextField addDetailsValue;

    private WarehouseDetailsJpaController warehouseDetailsJpaController;
    private SectorDetailsJpaController sectorDetailsJpaController;
    private ShelfDetailsJpaController shelfDetailsJpaController;
    private CategoryDetailsJpaController categoryDetailsJpaController;
    private ProductDetailsJpaController productDetailsJpaController;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        transaction();
        initTable();
        loadTable();
        labelAnimation();
        tableListener();
        addAction();
        addDetails();
        cancelButton();

    }

    //creates a connection to DB, Initializes JPA controllers
    private void transaction() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WarehouseV2PU");
        warehouseDetailsJpaController = new WarehouseDetailsJpaController(emf);
        sectorDetailsJpaController = new SectorDetailsJpaController(emf);
        shelfDetailsJpaController = new ShelfDetailsJpaController(emf);
        categoryDetailsJpaController = new CategoryDetailsJpaController(emf);
        productDetailsJpaController = new ProductDetailsJpaController(emf);
    }

    //creates an opacity animation for the infoLabel
    private void labelAnimation() {
        Timeline tim = new Timeline();
        tim.setAutoReverse(true);
        tim.setCycleCount(-1);
        tim.getKeyFrames().add(new KeyFrame(Duration.millis(900), new KeyValue(infoLabel.opacityProperty(), 0.5)));
        tim.play();
    }

    // Initializes the table columns
    private void initTable() {
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        valueCol.setCellValueFactory(new PropertyValueFactory<>("value"));
    }

    //loads the tableView from the database
    private void loadTable() {
        if (WarehouseController.isWarehouse) {
            ObservableList<Details> list = FXCollections.observableArrayList();
            warehouseDetailsJpaController.findWarehouseDetailsEntities().stream().filter((warehouseDetails) -> (warehouseDetails.getIsActive() == 1 && warehouseDetails.getWarehouseId().equals(WarehouseController.choosedWarehouse))).forEach((warehouseDetails) -> {
                list.add(new Details(warehouseDetails));
            });
            tableView.setItems(list);
            WarehouseController.isWarehouse = false;
        } else if (SectorController.isSector) {
            ObservableList<Details> list = FXCollections.observableArrayList();
            sectorDetailsJpaController.findSectorDetailsEntities().stream().filter((sectorDetails) -> (sectorDetails.getIsActive() == 1 && sectorDetails.getSectorId().equals(SectorController.choosedSector))).forEach((sectorDetails) -> {
                list.add(new Details(sectorDetails));
            });
            tableView.setItems(list);
            SectorController.isSector = false;
        } else if (ShelfController.isShelf) {
            ObservableList<Details> list = FXCollections.observableArrayList();
            shelfDetailsJpaController.findShelfDetailsEntities().stream().filter((shelfDetails) -> (shelfDetails.getIsActive() == 1 && shelfDetails.getShelfId().equals(ShelfController.choosedShelf))).forEach((shelfDetails) -> {
                list.add(new Details(shelfDetails));
            });
            tableView.setItems(list);
            ShelfController.isShelf = false;
        } else if (CategoryController.isCategory) {
            ObservableList<Details> list = FXCollections.observableArrayList();
            categoryDetailsJpaController.findCategoryDetailsEntities().stream().filter((categoryDetails) -> (categoryDetails.getIsActive() == 1 && categoryDetails.getCategoryId().equals(CategoryController.choosedCategory))).forEach((categoryDetails) -> {
                list.add(new Details(categoryDetails));
            });
            tableView.setItems(list);
            CategoryController.isCategory = false;
        } else if (ProductController.isProduct) {
            ObservableList<Details> list = FXCollections.observableArrayList();
            productDetailsJpaController.findProductDetailsEntities().stream().filter((productDetails) -> (productDetails.getIsActive() == 1 && productDetails.getProductId().equals(ProductController.choosedProduct))).forEach((productDetails) -> {
                list.add(new Details(productDetails));
            });
            tableView.setItems(list);
            ProductController.isProduct = false;
        }

    }

    //cancel
    private void cancelButton() {
        cancelButton.setOnAction(e -> {
            Stage s = (Stage) anch.getScene().getWindow();
            s.close();
        });
    }

    //adds
    private void addDetails() {
        addSecondButton.setOnAction(e -> {
            if (checkforAdd()) {
                if (ProductController.choosedProduct != null) {
                    ProductDetails pd = new ProductDetails();
                    pd.setIsActive(1);
                    pd.setName(addDetailsName.getText());
                    pd.setValue(addDetailsValue.getText());
                    pd.setCreatedDate(Utility.nowDate());
                    pd.setProductId(ProductController.choosedProduct);
                    productDetailsJpaController.create(pd);
                    for (ProductDetails p : productDetailsJpaController.findProductDetailsEntities()) {
                        if (p.equals(pd) && p.getIsActive() == 1) {
                            Alert al = new Alert(Alert.AlertType.INFORMATION);
                            al.setTitle("Congratulations!");
                            al.setContentText("Product Detail added Successfully!");
                            al.showAndWait();
                            break;
                        }
                    }
                    clearAdd();
                    ObservableList<Details> list = FXCollections.observableArrayList();
                    productDetailsJpaController.findProductDetailsEntities().stream().filter((jpa.ProductDetails productDetails1) -> (productDetails1.getIsActive() == 1 && productDetails1.getProductId().equals(ProductController.choosedProduct))).forEach((jpa.ProductDetails productDetails2) -> {
                        list.add(new Details(productDetails2));
                    });
                    tableView.setItems(list);
                } else if (CategoryController.choosedCategory != null) {
                    CategoryDetails categoryDetails = new CategoryDetails();
                    categoryDetails.setIsActive(1);
                    categoryDetails.setName(addDetailsName.getText());
                    categoryDetails.setValue(addDetailsValue.getText());
                    categoryDetails.setCreatedDate(Utility.nowDate());
                    categoryDetails.setCategoryId(CategoryController.choosedCategory);
                    categoryDetailsJpaController.create(categoryDetails);
                    for (CategoryDetails c : categoryDetailsJpaController.findCategoryDetailsEntities()) {
                        if (c.equals(categoryDetails) && c.getIsActive() == 1) {
                            Alert al = new Alert(Alert.AlertType.INFORMATION);
                            al.setTitle("Congratulations!");
                            al.setContentText("Category Detail added Successfully!");
                            al.showAndWait();
                            break;
                        }
                    }
                    clearAdd();
                    ObservableList<Details> list = FXCollections.observableArrayList();
                    categoryDetailsJpaController.findCategoryDetailsEntities().stream().filter((productDetails) -> (productDetails.getIsActive() == 1 && productDetails.getCategoryId().equals(CategoryController.choosedCategory))).forEach((productDetails) -> {
                        list.add(new Details(productDetails));
                    });
                    tableView.setItems(list);
                } else if (ShelfController.choosedShelf != null) {
                    ShelfDetails shelfDetails = new ShelfDetails();
                    shelfDetails.setIsActive(1);
                    shelfDetails.setName(addDetailsName.getText());
                    shelfDetails.setValue(addDetailsName.getText());
                    shelfDetails.setCreatedDate(Utility.nowDate());
                    shelfDetails.setShelfId(ShelfController.choosedShelf);
                    shelfDetailsJpaController.create(shelfDetails);
                    for (ShelfDetails sh : shelfDetailsJpaController.findShelfDetailsEntities()) {
                        if (sh.equals(shelfDetails) && sh.getIsActive() == 1) {
                            Alert al = new Alert(Alert.AlertType.INFORMATION);
                            al.setTitle("Congratulations!");
                            al.setContentText("Shelf Detail added Successfully!");
                            al.showAndWait();
                            break;
                        }
                    }
                    clearAdd();
                    loadTable();
                    ObservableList<Details> list = FXCollections.observableArrayList();
                    shelfDetailsJpaController.findShelfDetailsEntities().stream().filter((productDetails) -> (productDetails.getIsActive() == 1 && productDetails.getShelfId().equals(ShelfController.choosedShelf))).forEach((productDetails) -> {
                        list.add(new Details(productDetails));
                    });
                    tableView.setItems(list);
                } else if (SectorController.choosedSector != null) {
                    SectorDetails sectorDetails = new SectorDetails();
                    sectorDetails.setIsActive(1);
                    sectorDetails.setName(addDetailsName.getText());
                    sectorDetails.setValue(addDetailsName.getText());
                    sectorDetails.setCreatedDate(Utility.nowDate());
                    sectorDetails.setSectorId(SectorController.choosedSector);
                    sectorDetailsJpaController.create(sectorDetails);
                    for (SectorDetails sh : sectorDetailsJpaController.findSectorDetailsEntities()) {
                        if (sh.equals(sectorDetails) && sh.getIsActive() == 1) {
                            Alert al = new Alert(Alert.AlertType.INFORMATION);
                            al.setTitle("Congratulations!");
                            al.setContentText("Sector Detail added Successfully!");
                            al.showAndWait();
                            break;
                        }
                    }
                    clearAdd();
                    ObservableList<Details> list = FXCollections.observableArrayList();
                    sectorDetailsJpaController.findSectorDetailsEntities().stream().filter((productDetails) -> (productDetails.getIsActive() == 1 && productDetails.getSectorId().equals(SectorController.choosedSector))).forEach((productDetails) -> {
                        list.add(new Details(productDetails));
                    });
                    tableView.setItems(list);
                } else if (WarehouseController.choosedWarehouse != null) {
                    WarehouseDetails warehouseDetails = new WarehouseDetails();
                    warehouseDetails.setIsActive(1);
                    warehouseDetails.setName(addDetailsName.getText());
                    warehouseDetails.setValue(addDetailsName.getText());
                    warehouseDetails.setCreatedDate(Utility.nowDate());
                    warehouseDetails.setWarehouseId(WarehouseController.choosedWarehouse);
                    warehouseDetailsJpaController.create(warehouseDetails);
                    for (WarehouseDetails w : warehouseDetailsJpaController.findWarehouseDetailsEntities()) {
                        if (w.equals(warehouseDetails) && w.getIsActive() == 1) {
                            Alert al = new Alert(Alert.AlertType.INFORMATION);
                            al.setTitle("Congratulations!");
                            al.setContentText("Warehouse Detail added Successfully!");
                            al.showAndWait();
                            break;
                        }
                    }
                    clearAdd();
                    ObservableList<Details> list = FXCollections.observableArrayList();
                    warehouseDetailsJpaController.findWarehouseDetailsEntities().stream().filter((productDetails) -> (productDetails.getIsActive() == 1 && productDetails.getWarehouseId().equals(WarehouseController.choosedWarehouse))).forEach((productDetails) -> {
                        list.add(new Details(productDetails));
                    });
                    tableView.setItems(list);
                }
            }
        });
    }

    //adds a table listener for editing and deleting details..
    private void tableListener() {
        tableView.setOnMousePressed((MouseEvent event) -> {
            Details details = tableView.getSelectionModel().getSelectedItem();
            if (details.get() instanceof ProductDetails) {
                ProductDetails productDetails = details.getProductDetails();

                deleteMenuItem.setOnAction((ActionEvent event1) -> {
                    productDetails.setIsActive(0);
                    productDetails.setUpdatedDate(Utility.nowDate());
                    try {
                        productDetailsJpaController.edit(productDetails);
                    } catch (Exception ex) {
                        Logger.getLogger(ViewDetailsController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    ObservableList<Details> list = FXCollections.observableArrayList();
                    productDetailsJpaController.findProductDetailsEntities().stream().filter((jpa.ProductDetails productDetails1) -> (productDetails1.getIsActive() == 1 && productDetails1.getProductId().equals(ProductController.choosedProduct))).forEach((jpa.ProductDetails productDetails2) -> {
                        list.add(new Details(productDetails2));
                    });
                    tableView.setItems(list);
                });

                editMenuItem.setOnAction(e -> {
                    addButton.fire();
                    label.textProperty().set("Edit Details");
                    addDetailsName.setText(productDetails.getName());
                    addDetailsValue.setText(productDetails.getValue());
                    addSecondButton.setOnAction(ev -> {
                        if (checkforAdd()) {
                            productDetails.setUpdatedDate(Utility.nowDate());
                            productDetails.setName(addDetailsName.getText());
                            productDetails.setValue(addDetailsValue.getText());
                            try {
                                productDetailsJpaController.edit(productDetails);
                                cancelSecondButton.fire();
                            } catch (Exception ex) {
                                Logger.getLogger(ViewDetailsController.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            ObservableList<Details> list = FXCollections.observableArrayList();
                            productDetailsJpaController.findProductDetailsEntities().stream().filter((jpa.ProductDetails productDetails1) -> (productDetails1.getIsActive() == 1 && productDetails1.getProductId().equals(ProductController.choosedProduct))).forEach((jpa.ProductDetails productDetails2) -> {
                                list.add(new Details(productDetails2));
                            });
                            tableView.setItems(list);
                        }
                    });

                });

            } else if (details.get() instanceof CategoryDetails) {
                CategoryDetails categoryDetails = details.getCategoryDetails();

                deleteMenuItem.setOnAction((ActionEvent event1) -> {
                    categoryDetails.setIsActive(0);
                    categoryDetails.setUpdatedDate(Utility.nowDate());
                    try {
                        categoryDetailsJpaController.edit(categoryDetails);
                    } catch (Exception ex) {
                        Logger.getLogger(ViewDetailsController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    ObservableList<Details> list = FXCollections.observableArrayList();
                    categoryDetailsJpaController.findCategoryDetailsEntities().stream().filter((productDetails) -> (productDetails.getIsActive() == 1 && productDetails.getCategoryId().equals(CategoryController.choosedCategory))).forEach((productDetails) -> {
                        list.add(new Details(productDetails));
                    });
                    tableView.setItems(list);
                });

                editMenuItem.setOnAction(e -> {
                    addButton.fire();
                    label.textProperty().set("Edit Details");
                    addDetailsName.setText(categoryDetails.getName());
                    addDetailsValue.setText(categoryDetails.getValue());
                    addSecondButton.setOnAction(ev -> {
                        if (checkforAdd()) {
                            categoryDetails.setUpdatedDate(Utility.nowDate());
                            categoryDetails.setName(addDetailsName.getText());
                            categoryDetails.setValue(addDetailsValue.getText());
                            try {
                                categoryDetailsJpaController.edit(categoryDetails);
                                cancelSecondButton.fire();
                            } catch (Exception ex) {
                                Logger.getLogger(ViewDetailsController.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            ObservableList<Details> list = FXCollections.observableArrayList();
                            categoryDetailsJpaController.findCategoryDetailsEntities().stream().filter((productDetails) -> (productDetails.getIsActive() == 1 && productDetails.getCategoryId().equals(CategoryController.choosedCategory))).forEach((productDetails) -> {
                                list.add(new Details(productDetails));
                            });
                            tableView.setItems(list);
                        }
                    });

                });

            } else if (details.get() instanceof ShelfDetails) {
                ShelfDetails shelfDetails = details.getShelfDetails();

                deleteMenuItem.setOnAction((ActionEvent event1) -> {
                    shelfDetails.setIsActive(0);
                    shelfDetails.setUpdatedDate(Utility.nowDate());
                    try {
                        shelfDetailsJpaController.edit(shelfDetails);
                    } catch (Exception ex) {
                        Logger.getLogger(ViewDetailsController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    ObservableList<Details> list = FXCollections.observableArrayList();
                    shelfDetailsJpaController.findShelfDetailsEntities().stream().filter((productDetails) -> (productDetails.getIsActive() == 1 && productDetails.getShelfId().equals(ShelfController.choosedShelf))).forEach((productDetails) -> {
                        list.add(new Details(productDetails));
                    });
                    tableView.setItems(list);
                });

                editMenuItem.setOnAction(e -> {
                    addButton.fire();
                    label.textProperty().set("Edit Details");
                    addDetailsName.setText(shelfDetails.getName());
                    addDetailsValue.setText(shelfDetails.getValue());
                    addSecondButton.setOnAction(ev -> {
                        if (checkforAdd()) {
                            shelfDetails.setUpdatedDate(Utility.nowDate());
                            shelfDetails.setName(addDetailsName.getText());
                            shelfDetails.setValue(addDetailsValue.getText());
                            try {
                                shelfDetailsJpaController.edit(shelfDetails);
                                cancelSecondButton.fire();
                            } catch (Exception ex) {
                                Logger.getLogger(ViewDetailsController.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            ObservableList<Details> list = FXCollections.observableArrayList();
                            shelfDetailsJpaController.findShelfDetailsEntities().stream().filter((productDetails) -> (productDetails.getIsActive() == 1 && productDetails.getShelfId().equals(ShelfController.choosedShelf))).forEach((productDetails) -> {
                                list.add(new Details(productDetails));
                            });
                            tableView.setItems(list);
                        }
                    });

                });

            } else if (details.get() instanceof SectorDetails) {
                SectorDetails sectorDetails = details.getSectorDetails();

                deleteMenuItem.setOnAction((ActionEvent event1) -> {
                    sectorDetails.setIsActive(0);
                    sectorDetails.setUpdatedDate(Utility.nowDate());
                    try {
                        sectorDetailsJpaController.edit(sectorDetails);
                    } catch (Exception ex) {
                        Logger.getLogger(ViewDetailsController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    ObservableList<Details> list = FXCollections.observableArrayList();
                    sectorDetailsJpaController.findSectorDetailsEntities().stream().filter((productDetails) -> (productDetails.getIsActive() == 1 && productDetails.getSectorId().equals(SectorController.choosedSector))).forEach((productDetails) -> {
                        list.add(new Details(productDetails));
                    });
                    tableView.setItems(list);
                });

                editMenuItem.setOnAction(e -> {
                    addButton.fire();
                    label.textProperty().set("Edit Details");
                    addDetailsName.setText(sectorDetails.getName());
                    addDetailsValue.setText(sectorDetails.getValue());
                    addSecondButton.setOnAction(ev -> {
                        if (checkforAdd()) {
                            sectorDetails.setUpdatedDate(Utility.nowDate());
                            sectorDetails.setName(addDetailsName.getText());
                            sectorDetails.setValue(addDetailsValue.getText());
                            try {
                                sectorDetailsJpaController.edit(sectorDetails);
                                cancelSecondButton.fire();
                            } catch (Exception ex) {
                                Logger.getLogger(ViewDetailsController.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            ObservableList<Details> list = FXCollections.observableArrayList();
                            sectorDetailsJpaController.findSectorDetailsEntities().stream().filter((productDetails) -> (productDetails.getIsActive() == 1 && productDetails.getSectorId().equals(SectorController.choosedSector))).forEach((productDetails) -> {
                                list.add(new Details(productDetails));
                            });
                            tableView.setItems(list);
                        }
                    });

                });

            } else if (details.get() instanceof WarehouseDetails) {
                WarehouseDetails warehouseDetails = details.getWarehouseDetails();

                deleteMenuItem.setOnAction((ActionEvent event1) -> {
                    warehouseDetails.setIsActive(0);
                    warehouseDetails.setUpdatedDate(Utility.nowDate());
                    try {
                        warehouseDetailsJpaController.edit(warehouseDetails);
                    } catch (Exception ex) {
                        Logger.getLogger(ViewDetailsController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    ObservableList<Details> list = FXCollections.observableArrayList();
                    warehouseDetailsJpaController.findWarehouseDetailsEntities().stream().filter((productDetails) -> (productDetails.getIsActive() == 1 && productDetails.getWarehouseId().equals(WarehouseController.choosedWarehouse))).forEach((productDetails) -> {
                        list.add(new Details(productDetails));
                    });
                    tableView.setItems(list);
                });

                editMenuItem.setOnAction(e -> {
                    addButton.fire();
                    label.textProperty().set("Edit Details");
                    addDetailsName.setText(warehouseDetails.getName());
                    addDetailsValue.setText(warehouseDetails.getValue());
                    addSecondButton.setOnAction(ev -> {
                        if (checkforAdd()) {
                            warehouseDetails.setUpdatedDate(Utility.nowDate());
                            warehouseDetails.setName(addDetailsName.getText());
                            warehouseDetails.setValue(addDetailsValue.getText());
                            try {
                                warehouseDetailsJpaController.edit(warehouseDetails);
                                cancelSecondButton.fire();
                            } catch (Exception ex) {
                                Logger.getLogger(ViewDetailsController.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            ObservableList<Details> list = FXCollections.observableArrayList();
                            warehouseDetailsJpaController.findWarehouseDetailsEntities().stream().filter((productDetails) -> (productDetails.getIsActive() == 1 && productDetails.getWarehouseId().equals(WarehouseController.choosedWarehouse))).forEach((productDetails) -> {
                                list.add(new Details(productDetails));
                            });
                            tableView.setItems(list);
                        }
                    });

                });
            }
        });
    }

    //adds an item
    private void addAction() {
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Timeline t = new Timeline();
                t.setCycleCount(1);
                t.getKeyFrames().add(new KeyFrame(Duration.millis(650), new KeyValue(addPane.layoutYProperty(), 0.0)));
                t.play();
                label.textProperty().set("Add Details");
            }
        });

        cancelSecondButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                Timeline t = new Timeline();
                t.setCycleCount(1);
                t.getKeyFrames().add(new KeyFrame(Duration.millis(650), new KeyValue(addPane.layoutYProperty(), -582)));
                t.play();
            }
        });

        //valueTextField listener
        addDetailsValue.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.isEmpty() && !addDetailsName.getText().trim().isEmpty()) {
                    addSecondButton.setDisable(false);
                } else {
                    addSecondButton.setDisable(true);
                }
            }
        });

    }

    private boolean checkforAdd() {
        if (addDetailsName.getText().trim().isEmpty()) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please enter the name");
            al.showAndWait();
            return false;
        } else if (addDetailsValue.getText().trim().isEmpty()) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please enter the name");
            al.showAndWait();
            return false;
        } else {
            return true;
        }

    }

    private void clearAdd() {
        addDetailsName.clear();
        addDetailsValue.clear();
    }

}
