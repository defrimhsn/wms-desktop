/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import gui.ControlledScreen;
import gui.ScreensController;
import gui.WarehouseMain;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.User;
import jpa.UserJpaController;

/**
 * FXML Controller class
 *
 * @author defri
 */
public class LoginController implements Initializable, ControlledScreen {

    /**
     * Initializes the controller class.
     */
    private ScreensController controller;

    @FXML
    private Pane mainPane;

    @FXML
    private Pane innerpane;

    @FXML
    private Button button;

    @FXML
    private TextField usernameField;

    @FXML
    private Label usernameLabel;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Label passwordLabel;

    public static User loggedUser;

    private UserJpaController userJpaController;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        begin();
        transaction();
        validateLogin();
        indefiniteAnimations();
        button.setDefaultButton(true);

    }

    @Override
    public void setScreenParent(ScreensController screenPage) {
        controller = screenPage;
    }

    private void indefiniteAnimations() {
        Timeline timeline = new Timeline();
        timeline.setAutoReverse(true);
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(600),
                new KeyValue(usernameLabel.opacityProperty(), 0.7),
                new KeyValue(passwordLabel.opacityProperty(), 0.7)));
        timeline.play();
    }

    private void transaction() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WarehouseV2PU");
        userJpaController = new UserJpaController(emf);
    }

    private void begin() {
        innerpane.setLayoutY(740);
        Timeline timeline = new Timeline();
        timeline.setCycleCount(1);
        timeline.setDelay(Duration.millis(3000));
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(1000),
                new KeyValue(innerpane.opacityProperty(), 1.0), new KeyValue(innerpane.layoutYProperty(), 164)));
        timeline.play();

    }

    private void validateLogin() {
        button.setOnAction((ActionEvent event) -> {
            boolean logged = false;
            for (User u : userJpaController.findUserEntities()) {
                if (usernameField.getText().equals(u.getUsername()) && u.getPassword().equals(Utility.MD5(passwordField.getText()))) {
                    logged = true;
                    loggedUser = u;
                    break;
                } else {
                    logged = false;
                }
            }
            if (logged) {
                exitTimeline();
                usernameField.clear();
                passwordField.clear();
                

            } else {
                Alert al = new Alert(Alert.AlertType.ERROR);
                al.setTitle("Sorry!");
                al.setContentText("Username or password incorrect!");
                al.showAndWait();
                //clear();
            }
        });
    }

    private void exitTimeline() {
        Timeline timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(200),
                new KeyValue(innerpane.opacityProperty(), 0.0),
                new KeyValue(innerpane.scaleXProperty(), 1.1),
                new KeyValue(innerpane.scaleYProperty(), 1.1)));
        timeline.play();
        timeline.setOnFinished((ActionEvent event) -> {
            controller.setScreen(WarehouseMain.MAINWINDOW);
            innerpane.setOpacity(1.0);
            innerpane.scaleXProperty().set(1.1);
            innerpane.scaleXProperty().set(1.1);
        });

    }

}
