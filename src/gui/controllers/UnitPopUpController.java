/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.Unit;
import jpa.UnitJpaController;

/**
 * FXML Controller class
 *
 * @author defri
 */
public class UnitPopUpController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private AnchorPane anch;

    @FXML
    private TextField nameField;

    @FXML
    private TextArea Description;

    @FXML
    private Button addButton;

    @FXML
    private Button cancelButton;

    private UnitJpaController unitJpaController;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        transaction();
        addUnit();
    }

    private void transaction() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WarehouseV2PU");
        unitJpaController = new UnitJpaController(emf);
    }

    private void addUnit() {
        addButton.setOnAction(e -> {
            if (check()) {
                Unit unit = new Unit();
                unit.setName(nameField.getText().trim());
                unit.setDescription(Description.getText().trim());
                unitJpaController.create(unit);
                if (unitJpaController.findUnitEntities().contains(unit)) {
                    Alert al = new Alert(Alert.AlertType.INFORMATION);
                    al.setTitle("Congratulations!");
                    al.setContentText("Unit Added successfully!");
                    al.showAndWait();
                    Stage s = (Stage) anch.getScene().getWindow();
                    s.close();
                }
            }
        });
    }

    private boolean check() {
        if (nameField.getText().trim().isEmpty()) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please enter the name");
            al.showAndWait();
            return false;
        }
        return true;
    }

}
