/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.Category;
import jpa.CategoryDetails;
import jpa.CategoryDetailsJpaController;
import jpa.CategoryJpaController;
import jpa.Sector;
import jpa.SectorJpaController;
import jpa.Warehouse;
import jpa.WarehouseJpaController;
import jpa.exceptions.NonexistentEntityException;

/**
 * FXML Controller class
 *
 * @author Gravity
 */
public class CategoryController implements Initializable {

    @FXML
    private Pane pane;

    @FXML
    private TabPane tabpane;

    @FXML
    private AnchorPane anch1;

    @FXML
    private Pane innerPane;

    @FXML
    private Button addButton;

    @FXML
    private Button cancelButton;

    @FXML
    private TextField nameField;

    @FXML
    private ComboBox<Sector> chooseSector;

    @FXML
    private ComboBox<Warehouse> chooseWarehouse;

    @FXML
    private Label categoryLabel;

    @FXML
    private TextArea descriptionField;

    @FXML
    private Label descriptionLabel;

    @FXML
    private ComboBox<Sector> searchBySector;

    @FXML
    private CheckBox addExtraDetails;

    @FXML
    private AnchorPane anch2;

    @FXML
    private Pane searchPane;

    @FXML
    private Label searchLabel;

    @FXML
    private ComboBox<String> searchcategoryCombo;

    @FXML
    private TextField searchbox;

    @FXML
    private Button searchButton;

    @FXML
    private Pane editPane;

    @FXML
    private TextField editNameField;

    @FXML
    private TextField editDescriptionField;

    @FXML
    private Button editButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Button refreshTable;

    @FXML
    private MenuItem viewDetails;

    @FXML
    private MenuItem deleteMenuItem;

    @FXML
    private TableView<Category> tableView;

    @FXML
    private TableColumn<Category, String> nameColumn;

    @FXML
    private TableColumn<Category, String> descriptionColumn;

    private WarehouseJpaController warehouseJpaController;
    private SectorJpaController sectorJpaController;
    private CategoryJpaController categoryJpaController;
    private CategoryDetailsJpaController categoryDetailsJpaController;

    public static boolean isCategory;
    public static Category choosedCategory;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        transaction();
        initTable();
        loadtable();
        addCategory();
        tableListener();
        loadWarehouses();
        combosListener();
        checkButton();
        comboListener();
        loadSearchCombo();
        loadSectors(searchBySector);

    }

    private void transaction() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WarehouseV2PU");
        sectorJpaController = new SectorJpaController(emf);
        warehouseJpaController = new WarehouseJpaController(emf);
        categoryJpaController = new CategoryJpaController(emf);
        categoryDetailsJpaController = new CategoryDetailsJpaController(emf);
    }

    private void initTable() {
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        descriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
    }

    private void addCategory() {
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (check()) {
                    Category category = new Category();
                    category.setName(nameField.getText().trim());
                    if (descriptionField.getText() == null || descriptionField.getText().trim().isEmpty()) {

                    } else {
                        category.setDescription(descriptionField.getText().trim());
                    }
                    category.setCreatedDate(Utility.nowDate());
                    category.setIsActive(1);
                    category.setSectorId(chooseSector.getSelectionModel().getSelectedItem());
                    category.getSectorId().setWarehouseId(chooseWarehouse.getSelectionModel().getSelectedItem());
                    categoryJpaController.create(category);
                    if (addExtraDetails.isSelected()) {
                        for (Map.Entry ma : AddExtraDetailsController.details.entrySet()) {
                            CategoryDetails categoryDetails = new CategoryDetails();
                            categoryDetails.setIsActive(1);
                            categoryDetails.setName((String) ma.getKey());
                            categoryDetails.setValue((String) ma.getValue());
                            categoryDetails.setCategoryId(category);
                            categoryDetails.setCreatedDate(Utility.nowDate());
                            categoryDetailsJpaController.create(categoryDetails);
                        }
                    } else {

                    }
                    boolean added = false;
                    for (Category cat : categoryJpaController.findCategoryEntities()) {
                        if (cat.getIsActive() == 1 && cat.equals(category)) {
                            added = true;
                            break;
                        } else {
                            added = false;
                        }
                    }
                    if (added) {
                        Alert al = new Alert(Alert.AlertType.INFORMATION);
                        al.setTitle("Category added successfully!");
                        al.setContentText(al.getTitle());
                        al.showAndWait();
                        clear();
                        loadtable();
                    }
                }
            }
        });
    }

    private boolean check() {
        if (chooseWarehouse.getSelectionModel().getSelectedItem() == null) {
            Alert al = new Alert(AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please choose a warehouse");
            al.showAndWait();
            return false;
        } else if (chooseSector.getSelectionModel().getSelectedItem() == null) {
            Alert al = new Alert(AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please choose a sector");
            al.showAndWait();
            return false;
        } else if (nameField.getText() == null || nameField.getText().trim().isEmpty()) {
            Alert al = new Alert(AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please enter the name");
            al.showAndWait();
            return false;
        } else {
            return true;
        }
    }

    private void loadWarehouses() {
        ObservableList<Warehouse> ware = FXCollections.observableArrayList();
        for (Warehouse w : warehouseJpaController.findWarehouseEntities()) {
            if (w.getIsActive() == 1) {
                ware.add(w);
            }
        }
        chooseWarehouse.setItems(ware);
    }

    private void loadSectors() {
        ObservableList<Sector> sec = FXCollections.observableArrayList();
        for (Sector w : sectorJpaController.findSectorEntities()) {
            if (w.getIsActive() == 1) {
                sec.add(w);
            }
        }
        chooseSector.setItems(sec);
    }

    private void combosListener() {
        chooseWarehouse.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ObservableList<Sector> l = FXCollections.observableArrayList();
                Warehouse temp = chooseWarehouse.getSelectionModel().getSelectedItem();
                for (Sector sector : sectorJpaController.findSectorEntities()) {
                    if (sector.getIsActive() == 1 && sector.getWarehouseId().equals(temp)) {
                        l.add(sector);
                    }
                }
                chooseSector.setItems(l);
            }
        });

    }

    private void tableListener() {
        tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Category c = tableView.getSelectionModel().getSelectedItem();
                if (c != null) {
                    editButton.setDisable(false);
                    deleteButton.setDisable(false);
                }

                Timeline timeline = new Timeline();
                timeline.setAutoReverse(true);
                timeline.setCycleCount(2);
                timeline.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(editNameField.scaleXProperty(), 1.1),
                        new KeyValue(editDescriptionField.scaleXProperty(), 1.1)));
                timeline.play();
                editNameField.setText(c.getName());
                editDescriptionField.setText(c.getDescription());

                deleteButton.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if (Utility.hasSomethingActive(c)) {
                            Alert al = new Alert(Alert.AlertType.ERROR);
                            al.setTitle("Error");
                            al.setContentText("To delete this category, make sure you have deleted all the content!");
                            al.showAndWait();
                        } else {
                            c.setIsActive(0);
                            c.setUpdatedDate(Utility.nowDate());
                            try {
                                categoryJpaController.edit(c);
                                loadtable();
                                clearEditFields();
                                tableView.refresh();
                            } catch (NonexistentEntityException ex) {
                                Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (Exception ex) {
                                Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                    }
                });

                deleteMenuItem.setOnAction(e -> {
                    deleteButton.fire();
                });

                viewDetails.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {
                            isCategory = true;
                            choosedCategory = c;
                            Stage s = new Stage(StageStyle.DECORATED);
                            s.setScene(new Scene((Pane) FXMLLoader.load(getClass().getResource("../fxml/ViewDetails.fxml"))));
                            s.initModality(Modality.APPLICATION_MODAL);
                            s.setResizable(false);
                            s.sizeToScene();
                            s.setTitle("Category Details");
                            s.show();
                        } catch (IOException ex) {
                            Logger.getLogger(WarehouseController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });

                editButton.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if (checkforEdit()) {
                            c.setName(editNameField.getText().trim());
                            c.setDescription(editDescriptionField.getText().trim());
                            c.setUpdatedDate(Utility.nowDate());
                            try {
                                categoryJpaController.edit(c);
                                loadtable();
                                tableView.refresh();
                                clearEditFields();
                            } catch (NonexistentEntityException ex) {
                                Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (Exception ex) {
                                Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                    }

                });

            }
        });

        anch2.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                editButton.setDisable(true);
                deleteButton.setDisable(true);
            }
        });

    }

    private void clearEditFields() {
        editNameField.clear();
        editDescriptionField.clear();
    }

    private boolean checkforEdit() {
        if (editNameField.getText() == null || editNameField.getText().trim().isEmpty()) {
            Alert al = new Alert(AlertType.WARNING);
            al.setContentText("Please enter the name");
            al.setTitle("Warning");
            al.showAndWait();
            return false;
        } else {
            return true;
        }
    }

    private void loadSectors(ComboBox<Sector> comb) {
        ObservableList<Sector> list = FXCollections.observableArrayList();
        for (Sector s : sectorJpaController.findSectorEntities()) {
            if (s.getIsActive() == 1) {
                list.add(s);
            }
        }
        comb.setItems(list);
    }

    private void comboListener() {

        searchcategoryCombo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if (searchcategoryCombo.getSelectionModel().getSelectedItem().equals("Sector")) {
                    searchBySector.opacityProperty().set(0);
                    Timeline timeline = new Timeline();
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(500),
                            new KeyValue(searchbox.scaleXProperty(), 0),
                            new KeyValue(searchBySector.layoutXProperty(), 300),
                            new KeyValue(searchBySector.opacityProperty(), 1.0)));
                    timeline.play();
                }
                if (searchcategoryCombo.getSelectionModel().getSelectedItem().equals("Name")) {
                    searchBySector.opacityProperty().set(1);
                    Timeline timeline = new Timeline();
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(500),
                            new KeyValue(searchbox.scaleXProperty(), 1.0),
                            new KeyValue(searchBySector.layoutXProperty(), 98),
                            new KeyValue(searchBySector.opacityProperty(), 0.0)));
                    timeline.play();
                }
            }
        });

        refreshTable.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                loadtable();
                searchbox.clear();
                searchcategoryCombo.setValue(null);
                searchBySector.setValue(null);
            }
        });
    }

    private void loadSearchCombo() {
        ObservableList<String> list = FXCollections.observableArrayList("Sector", "Name");
        searchcategoryCombo.setItems(list);

        searchButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ObservableList<Category> result = FXCollections.observableArrayList();
                String str = searchcategoryCombo.getSelectionModel().getSelectedItem();
                if (str == null) {
                    Timeline timeline = new Timeline();
                    timeline.setAutoReverse(true);
                    timeline.setCycleCount(4);
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(200), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            searchLabel.setText("required*");
                            searchLabel.textFillProperty().set(Color.RED);
                            searchcategoryCombo.setStyle("-fx-border-style: solid; -fx-border-width:1px; -fx-border-color:red; -fx-border-radius:3px;");
                        }
                    },
                            new KeyValue(searchcategoryCombo.layoutXProperty(), 101),
                            new KeyValue(searchcategoryCombo.layoutXProperty(), 96)));
                    timeline.setOnFinished(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            searchLabel.setText("Search by*");
                            searchLabel.textFillProperty().set(Color.BLACK);
                            searchcategoryCombo.setStyle("-fx-border-style: none; ");
                        }
                    });
                    timeline.play();
                } else {
                    if (str.equals("Sector")) {

                        if (searchBySector.getSelectionModel().getSelectedItem() == null) {
                            Timeline timeline = new Timeline();
                            timeline.setAutoReverse(true);
                            timeline.setCycleCount(4);
                            timeline.getKeyFrames().add(new KeyFrame(Duration.millis(200), new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    searchBySector.setStyle("-fx-border-style: solid; -fx-border-width:1px; -fx-border-color:red; -fx-border-radius:3px;");
                                }
                            },
                                    new KeyValue(searchBySector.layoutXProperty(), 297),
                                    new KeyValue(searchBySector.layoutXProperty(), 302)));
                            timeline.setOnFinished(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    searchBySector.setStyle("-fx-border-style: none; ");
                                }
                            });
                            timeline.play();
                        }
                        result.removeAll(result);
                        categoryJpaController.findCategoryEntities().stream().filter((c) -> (c.getIsActive() == 1 & c.getSectorId().equals(searchBySector.getSelectionModel().getSelectedItem()))).forEach((c) -> {
                            result.add(c);
                        });

                        tableView.setItems(result);

                    }
                    if (str.equals("Name")) {
                        if (searchbox.getText() == null || searchbox.getText().trim().isEmpty()) {
                            loadtable();

                        } else {
                            result.removeAll(result);
                            for (Category category : categoryJpaController.findCategoryEntities()) {
                                if (category.getIsActive() == 1 && category.getName().toLowerCase().contains(searchbox.getText().trim().toLowerCase())) {
                                    result.add(category);
                                }
                            }

                            tableView.setItems(result);
                        }

                    }
                }
            }
        });
    }

    private void checkButton() {
        addExtraDetails.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (addExtraDetails.isSelected()) {
                    Stage s = new Stage();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/AddExtraDetails.fxml"));
                    try {
                        Pane pane = (Pane) loader.load();
                        s.setScene(new Scene(pane));
                        s.show();
                    } catch (IOException ex) {
                        Logger.getLogger(SectorController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    private void clear() {
        chooseWarehouse.setValue(null);
        chooseWarehouse.setPromptText("Choose Warehouse");
        chooseSector.setValue(null);
        chooseSector.setPromptText("Choose Sector");
        nameField.clear();
        descriptionField.clear();
        addExtraDetails.setSelected(false);

    }

    private void loadtable() {
        ObservableList<Category> list = FXCollections.observableArrayList();
        for (Category c : categoryJpaController.findCategoryEntities()) {
            if (c.getIsActive() == 1) {
                list.add(c);
            }
        }
        tableView.setItems(list);
    }

}
