/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author defri
 */
public class ViewProfileController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private ImageView imgView;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        if(LoginController.loggedUser.getGender().equals("F")){
            imgView.setImage(new Image("../../res/femaleUser.png"));
        } else if(LoginController.loggedUser.getGender().equals("M")){
            imgView.setImage(new Image("../../res/maleUser.png"));
        }
    }

}
