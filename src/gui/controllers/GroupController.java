/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.Groups;
import jpa.GroupsJpaController;
import jpa.GroupsPermissions;
import jpa.GroupsPermissionsJpaController;
import jpa.Permissions;
import jpa.PermissionsJpaController;
import jpa.exceptions.NonexistentEntityException;

/**
 * FXML Controller class
 *
 * @author defri
 */
public class GroupController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Pane mainpane;

    @FXML
    private Tab groupTab;

    @FXML
    private TableView<Groups> tableview;

    @FXML
    private TableColumn<Groups, String> nameCol;

    @FXML
    private TableColumn<Groups, String> descriptioncol;

    @FXML
    private TextField nameField;

    @FXML
    private TextArea description;

    @FXML
    private ListView<CheckBox> permissions;

    @FXML
    private Button deleteButton;

    @FXML
    private AnchorPane anch;
    @FXML
    private Button addButton;

    @FXML
    private Button editButton;

    @FXML
    private Button cancelButton;

    @FXML
    private Tab permissionsTab;

    @FXML
    private TableView<Permissions> permissiontable;

    @FXML
    private TableColumn<Permissions, String> permissionNameCol;

    @FXML
    private TableColumn<Permissions, String> permissionDescriptioncol;

    @FXML
    private TextField permissionName;

    @FXML
    private AnchorPane anch2;

    @FXML
    private TextArea permissionDescription;

    @FXML
    private Button addPermission;

    @FXML
    private Button cancelPermission;

    @FXML
    private Button deletePermission;

    @FXML
    private Button editPermission;

    private ObservableList<CheckBox> list = FXCollections.observableArrayList();

    private GroupsJpaController groupsJpaController;
    private PermissionsJpaController permissionsJpaController;
    private GroupsPermissionsJpaController groupsPermissionsJpaController;

    private Groups selectedGroup;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        transaction();
        initPermissionsList();
        disableButtons();
        iniTable();
        loadTable();
        addGroup();
        tableListener();
        permissionsTableListener();
        addPermission();
        editGroup();
        cancel();
        initPermissionsTable();
        loadPermissionsTable();
        Utility.validateTextField(nameField, false);
        Utility.validateControl(nameField);
        Utility.validateTextField(permissionName, false);
        Utility.validateControl(permissionName);

    }

    private void transaction() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WarehouseV2PU");
        permissionsJpaController = new PermissionsJpaController(emf);
        groupsJpaController = new GroupsJpaController(emf);
        groupsPermissionsJpaController = new GroupsPermissionsJpaController(emf);
    }

    private void initPermissionsList() {

        for (Permissions p : permissionsJpaController.findPermissionsEntities()) {
            if (p.getIsActive() == 1) {
                list.add(new CheckBox(p.getName()));
            }
        }
        permissions.setItems(list);
    }

    private void disableButtons() {
        editButton.setDisable(true);
        deleteButton.setDisable(true);
    }

    private ObservableList<Permissions> getPermissions(ObservableList<CheckBox> checkBoxList) {
        ObservableList<Permissions> temp = FXCollections.observableArrayList();
        for (Permissions permissions : permissionsJpaController.findPermissionsEntities()) {
            for (CheckBox checkBox : checkBoxList) {
                if (checkBox.isSelected() && checkBox.getText().equals(permissions.getName())) {
                    temp.add(permissions);
                }
            }
        }
        return temp;

    }

    private void iniTable() {
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        descriptioncol.setCellValueFactory(new PropertyValueFactory<>("description"));
    }

    private void initPermissionsTable() {
        permissionNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        permissionDescriptioncol.setCellValueFactory(new PropertyValueFactory<>("description"));
    }

    private void loadTable() {
        ObservableList<Groups> list = FXCollections.observableArrayList();
        for (Groups gr : groupsJpaController.findGroupsEntities()) {
            if (gr.getIsActive() == 1) {
                list.add(gr);
            }
        }
        tableview.setItems(list);
        tableview.refresh();
    }

    private void loadPermissionsTable() {
        ObservableList<Permissions> permi = FXCollections.observableArrayList();
        for (Permissions p : permissionsJpaController.findPermissionsEntities()) {
            if (p.getIsActive() == 1) {
                permi.add(p);
            }
        }
        permissiontable.setItems(permi);

    }

    private void editGroup() {
        editButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (selectedGroup != null) {
                    if (checkforEdit()) {
                        selectedGroup.setName(nameField.getText());
                        selectedGroup.setDescription(description.getText());
                        selectedGroup.setUpdatedDate(Utility.nowDate());
                        for (GroupsPermissions groupsPermissions : groupsPermissionsJpaController.findGroupsPermissionsEntities()) {
                            if (groupsPermissions.getGroupsId().equals(selectedGroup)) {
                                try {
                                    groupsPermissionsJpaController.destroy(groupsPermissions.getId());
                                } catch (NonexistentEntityException ex) {
                                    Logger.getLogger(GroupController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }

                        for (Permissions p : getPermissions(list)) {
                            GroupsPermissions groupsPermissions = new GroupsPermissions();
                            groupsPermissions.setGroupsId(selectedGroup);
                            groupsPermissions.setPermissionsId(p);
                            groupsPermissionsJpaController.create(groupsPermissions);
                        }
                        clear();
                        loadTable();
                        tableview.refresh();
                        clear();
                    }
                }
            }
        });
    }

    private void addGroup() {
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (check()) {
                    Groups groups = new Groups();
                    groups.setName(nameField.getText());
                    groups.setDescription(description.getText());
                    groups.setIsActive(1);
                    groups.setCreatedDate(Utility.nowDate());
                    groupsJpaController.create(groups);
                    loadTable();

                    for (Permissions p : getPermissions(list)) {
                        GroupsPermissions groupsPermissions = new GroupsPermissions();
                        groupsPermissions.setGroupsId(groups);
                        groupsPermissions.setPermissionsId(p);
                        groupsPermissionsJpaController.create(groupsPermissions);
                    }
                    clear();

                }
            }
        });
    }

    private void tableListener() {
        tableview.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                selectedGroup = tableview.getSelectionModel().getSelectedItem();
                editButton.setDisable(false);
                deleteButton.setDisable(false);
                fillFields();
            }
        });

        anch.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                selectedGroup = null;
                editButton.setDisable(true);
                deleteButton.setDisable(true);
            }
        });

        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                for (GroupsPermissions groupsPermissions : groupsPermissionsJpaController.findGroupsPermissionsEntities()) {
                    if (groupsPermissions.getGroupsId().equals(selectedGroup)) {
                        try {
                            groupsPermissionsJpaController.destroy(groupsPermissions.getId());
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(GroupController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                selectedGroup.setIsActive(0);
                selectedGroup.setUpdatedDate(Utility.nowDate());
                try {
                    groupsJpaController.edit(selectedGroup);
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(GroupController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(GroupController.class.getName()).log(Level.SEVERE, null, ex);
                }

                loadTable();
                clear();

            }
        });
    }

    private void clear() {
        nameField.clear();
        description.clear();
        for (CheckBox checkBox : list) {
            checkBox.setSelected(false);
        }
        nameField.setStyle("-fx-border-style:none;");
    }

    private boolean check() {
        if (nameField.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning!");
            alert.setContentText("Please fill the name field!");
            alert.showAndWait();
            return false;
        } else if (nameField.getText().trim().length() < 3) {
            Alert al = new Alert(AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("The name should be longer than 3 characters!");
            al.showAndWait();
            return false;
        } else if (exists(nameField.getText())) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning!");
            alert.setContentText("This group exists already!");
            alert.showAndWait();
            return false;
        } else {
            return true;
        }
    }

    private boolean checkforEdit() {
        if (nameField.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning!");
            alert.setContentText("Please fill the name field!");
            alert.showAndWait();
            return false;
        } else {
            return true;
        }
    }

    private boolean exists(String name) {
        return groupsJpaController.findGroupsEntities().stream().anyMatch((g) -> (g.getIsActive() == 1 && g.getName().equals(name)));
    }

    private void cancel() {
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                clear();
            }
        });
    }

    private void fillFields() {
        if (selectedGroup != null) {
            nameField.setText(selectedGroup.getName());
            description.setText(selectedGroup.getDescription());
            for (CheckBox ch : list) {
                ch.setSelected(false);
            }
            fillCheckBoxes(selectedGroup);

        }
    }

    private void fillCheckBoxes(Groups g) {
        ArrayList<Permissions> list = new ArrayList<>();
        for (GroupsPermissions groupsPermissions : groupsPermissionsJpaController.findGroupsPermissionsEntities()) {
            if (groupsPermissions.getGroupsId().equals(g)) {
                list.add(groupsPermissions.getPermissionsId());
            }
        }

        for (CheckBox ch : this.list) {
            for (Permissions p : list) {
                if (p.getName().equals(ch.getText())) {
                    ch.setSelected(true);
                }
            }
        }
    }

    private void addPermission() {
        addPermission.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (checkPerm()) {
                    Permissions per = new Permissions();
                    per.setName(permissionName.getText());
                    per.setDescription(permissionDescription.getText());
                    per.setIsActive(1);
                    per.setCreatedDate(Utility.nowDate());
                    permissionsJpaController.create(per);
                    if (permissionsJpaController.findPermissionsEntities().contains(per)) {
                        Alert al = new Alert(AlertType.INFORMATION);
                        al.setContentText("Permission successfully added!");
                        al.showAndWait();
                        permissionName.clear();
                        permissionName.setStyle("-fx-border-style:none;");
                        permissionDescription.clear();

                        loadTable();
                        loadPermissionsTable();
                        list.removeAll(list);
                        initPermissionsList();
                    }
                }
            }
        });

    }

    private void permissionsTableListener() {
        permissiontable.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                editPermission.setDisable(false);
                deletePermission.setDisable(false);

                Permissions choosedPermission = permissiontable.getSelectionModel().getSelectedItem();
                if (choosedPermission != null) {
                    permissionName.setText(choosedPermission.getName());
                    permissionDescription.setText(choosedPermission.getDescription());
                }

                deletePermission.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        choosedPermission.setIsActive(0);
                        choosedPermission.setUpdatedDate(Utility.nowDate());
                        try {
                            permissionsJpaController.edit(choosedPermission);
                            if (permissionsJpaController.findPermissionsEntities().contains(choosedPermission)) {
                                if (choosedPermission.getIsActive() == 0) {
                                    Alert alert = new Alert(AlertType.INFORMATION);
                                    alert.setContentText("Permission deleted successfully!");
                                    alert.showAndWait();
                                }
                            }
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(GroupController.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (Exception ex) {
                            Logger.getLogger(GroupController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        permissionName.clear();
                        permissionName.setStyle("-fx-border-style:none;");
                        permissionDescription.clear();
                        loadPermissionsTable();
                        list.removeAll(list);
                        initPermissionsList();
                    }
                });

                editPermission.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if (checkPerm()) {
                            choosedPermission.setName(permissionName.getText());
                            choosedPermission.setDescription(description.getText());
                            choosedPermission.setUpdatedDate(Utility.nowDate());
                            try {
                                permissionsJpaController.edit(choosedPermission);
                            } catch (NonexistentEntityException ex) {
                                Logger.getLogger(GroupController.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (Exception ex) {
                                Logger.getLogger(GroupController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            permissionName.clear();
                            permissionName.setStyle("-fx-border-style:none;");
                            permissionDescription.clear();
                            list.removeAll(list);
                            loadPermissionsTable();
                        }
                    }
                });

            }
        });

        anch2.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                editPermission.setDisable(true);
                deletePermission.setDisable(true);
                permissionName.clear();
                permissionDescription.clear();
            }
        });
    }

    private boolean checkPerm() {
        if (permissionName.getText().trim().isEmpty()) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setContentText("Please type the permission name");
            alert.setTitle("Name field empty!");
            alert.showAndWait();
            return false;
        } else if (permissionName.getText().trim().length() < 3) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setContentText("The permission name should be longer than 3 characters");
            alert.setTitle("Name field empty!");
            alert.showAndWait();
            return false;
        } else {
            return true;
        }
    }

}
