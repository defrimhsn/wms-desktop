/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javax.swing.text.Utilities;
import jpa.Category;
import jpa.Product;
import jpa.ProductUnit;
import jpa.Stock;
import jpa.Unit;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 * FXML Controller class
 *
 * @author defri
 */
public class StockController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TableView<StockTableViewModel> tableView;

    @FXML
    private TableColumn<StockTableViewModel, String> productNameCol;

    @FXML
    private TableColumn<StockTableViewModel, Double> quantityCol;

    @FXML
    private TableColumn<StockTableViewModel, Unit> unitCol;

    @FXML
    private TableColumn<StockTableViewModel, Category> categoryCol;

    @FXML
    private TableColumn<StockTableViewModel, Double> sellPriceCol;

    @FXML
    private TableColumn<StockTableViewModel, Double> totalCostCol;

    @FXML
    private ComboBox<String> chooseProductCombo;

    @FXML
    private TextField quantityTextField;

    @FXML
    private TextField priceTextField;

    @FXML
    private TextField sellPriceTextField;

    @FXML
    private ComboBox<Unit> chooseUnitCombo;

    @FXML
    private TextField totalCostTextField;

    @FXML
    private MenuItem editMenuItem;

    @FXML
    private MenuItem deleteMenuItem;

    @FXML
    private Button addButton;

    @FXML
    private ImageView pdfIcon;

    @FXML
    private ImageView htmlIco;

    @FXML
    private ImageView xmlIcon;

    @FXML
    private ImageView xlsxIcon;

    @FXML
    private TableView<Unit> table;

    @FXML
    private TableColumn<Unit, String> tableName;

    @FXML
    private TableColumn<Unit, String> tableDescription;

    @FXML
    private TextField unitName;

    @FXML
    private TextArea unitDescription;

    @FXML
    private Button addUnit;

    @FXML
    private MenuItem deleteContextMenuItem;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        initTable();
        loadTable();
        loadProducts();
        calculateTotalCost();
        addtoStock();
        tableListener();
        initUnitTable();
        loadUnitTable();
        addUnit();
        unitTableListener();
        controlControls();

    }

    private void initTable() {
        productNameCol.setCellValueFactory(new PropertyValueFactory<>("productName"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        unitCol.setCellValueFactory(new PropertyValueFactory<>("unit"));
        categoryCol.setCellValueFactory(new PropertyValueFactory<>("category"));
        sellPriceCol.setCellValueFactory(new PropertyValueFactory<>("sellPrice"));
        totalCostCol.setCellValueFactory(new PropertyValueFactory<>("totalCost"));
    }

    private void loadTable() {
        ObservableList<StockTableViewModel> list = FXCollections.observableArrayList();
        Utility.STOCKJPACONTROLLER.findStockEntities().stream().forEach((stock) -> {

            list.add(new StockTableViewModel(stock));
        });
        tableView.setItems(list);
    }

    private void loadProducts() {
        ObservableList<String> list = FXCollections.observableArrayList();
        Utility.PRODUCTJPACONTROLLER.findProductEntities().stream().filter((product) -> (product.getIsActive() == 1)).filter((product) -> (!list.contains(product.getName()))).forEach((product) -> {
            list.add(product.getName());
        });
        chooseProductCombo.setItems(list);

        chooseProductCombo.setOnAction(e -> {
            ObservableList<Unit> unitsAvaliable = FXCollections.observableArrayList();
            Utility.PRODUCTUNITJPACONTROLLER.findProductUnitEntities().stream().filter((productUnit) -> (productUnit.getProductId().getName().equalsIgnoreCase(chooseProductCombo.getValue()))).forEach((productUnit) -> {
                unitsAvaliable.add(productUnit.getUnitId());
            });
            chooseUnitCombo.setItems(unitsAvaliable);
        });

        chooseUnitCombo.setOnAction(e -> {
            for (ProductUnit productUnit : Utility.PRODUCTUNITJPACONTROLLER.findProductUnitEntities()) {
                if (productUnit.getProductId().getName().equalsIgnoreCase(chooseProductCombo.getValue()) && productUnit.getUnitId().equals(chooseUnitCombo.getValue())) {
                    priceTextField.setText(productUnit.getProductId().getPrice() + "");
                    break;
                }
            }
        });

    }

    private void calculateTotalCost() {
        quantityTextField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!sellPriceTextField.getText().trim().isEmpty() && !quantityTextField.getText().trim().isEmpty()) {
                double totalCost = Integer.parseInt(quantityTextField.getText()) * Double.parseDouble(sellPriceTextField.getText());
                totalCostTextField.textProperty().set(totalCost + "");
            }
        });

        sellPriceTextField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!sellPriceTextField.getText().trim().isEmpty() && !quantityTextField.getText().trim().isEmpty()) {
                double totalCost = Integer.parseInt(quantityTextField.getText()) * Double.parseDouble(sellPriceTextField.getText());
                totalCostTextField.textProperty().set(totalCost + "");
            }
        });
    }

    private void addtoStock() {
        addButton.setOnAction(e -> {
            if (addButton.getText().equals("Add")) {
                if (check()) {
                    Stock stock = new Stock();
                    Product p = new Product();
                    for (ProductUnit productUnit : Utility.PRODUCTUNITJPACONTROLLER.findProductUnitEntities()) {
                        if (productUnit.getProductId().getName().equals(chooseProductCombo.getValue()) && productUnit.getUnitId().equals(chooseUnitCombo.getValue())) {
                            p = productUnit.getProductId();
                        }
                    }
                    if (p != null) {
                        stock.setProductId(p);
                        stock.setUnitId(chooseUnitCombo.getValue());
                        stock.setQuantity(Integer.parseInt(quantityTextField.getText()));
                        stock.setSellPrice(Double.parseDouble(sellPriceTextField.getText()));
                        Utility.STOCKJPACONTROLLER.create(stock);
                        if (Utility.STOCKJPACONTROLLER.findStockEntities().contains(stock)) {
                            Alert al = new Alert(Alert.AlertType.INFORMATION);
                            al.setTitle("Congratulations!");
                            al.setContentText("Item added successfully to Stock!");
                            System.out.println("Item  [" + stock + "] has been added to the Stock [" + new Date() + "]");
                            al.showAndWait();
                            loadTable();
                            clear();
                        }
                    }
                }
            }
        });

    }

    private boolean check() {
        if (chooseProductCombo.getValue() == null) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please choose a product!");
            al.showAndWait();
            return false;
        } else if (quantityTextField.getText().trim().isEmpty() || Double.parseDouble(quantityTextField.getText()) <= 0) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please enter a positive quantity");
            al.showAndWait();
            return false;
        } else if (sellPriceTextField.getText().trim().isEmpty() || Double.parseDouble(sellPriceTextField.getText()) <= 0) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please enter a positive sell price");
            al.showAndWait();
            return false;
        }
        return true;
    }

    private void clear() {
        chooseProductCombo.setValue(null);
        chooseUnitCombo.setValue(null);
        priceTextField.clear();
        quantityTextField.clear();
        sellPriceTextField.clear();
        totalCostTextField.clear();
    }

    private void tableListener() {
        tableView.setOnMousePressed(ev -> {
            StockTableViewModel stockTableViewModel = tableView.getSelectionModel().getSelectedItem();

            deleteMenuItem.setOnAction(event -> {
                try {
                    Utility.STOCKJPACONTROLLER.destroy(stockTableViewModel.getStock().getId());
                    loadTable();
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

            editMenuItem.setOnAction(e -> {

                chooseProductCombo.setValue(stockTableViewModel.getProduct().getName());
                loadProducts();
                quantityTextField.setText(stockTableViewModel.getQuantity() + "");
                priceTextField.setText(stockTableViewModel.getProduct().getPrice() + "");
                sellPriceTextField.setText(stockTableViewModel.getSellPrice() + "");
                chooseUnitCombo.setValue(stockTableViewModel.getUnit());
                addButton.setText("Save Changes");
                addButton.setOnAction(event -> {
                    if (addButton.getText().equals("Save Changes")) {

                        if (check()) {
                            Product p = new Product();
                            for (ProductUnit productUnit : Utility.PRODUCTUNITJPACONTROLLER.findProductUnitEntities()) {
                                if (productUnit.getProductId().getName().equals(chooseProductCombo.getSelectionModel().getSelectedItem()) && productUnit.getUnitId().equals(chooseUnitCombo.getValue())) {
                                    p = productUnit.getProductId();
                                }
                            }

                            if (p != null) {
                                stockTableViewModel.getStock().setProductId(p);
                                stockTableViewModel.getStock().setUnitId(chooseUnitCombo.getValue());
                                stockTableViewModel.getStock().setQuantity(Integer.parseInt(quantityTextField.getText()));
                                stockTableViewModel.getStock().setSellPrice(Double.parseDouble(sellPriceTextField.getText()));
                                try {
                                    Utility.STOCKJPACONTROLLER.edit(stockTableViewModel.getStock());
                                    Alert al = new Alert(Alert.AlertType.INFORMATION);
                                    al.setTitle("Congratulations!");
                                    al.setContentText("Item edited successfully to Stock!");
                                    al.showAndWait();
                                    loadTable();
                                    clear();

                                } catch (Exception ex) {
                                    Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }
                    }
                });

            });
        });
    }

    private void initUnitTable() {
        tableName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
    }

    private void loadUnitTable() {
        ObservableList<Unit> units = FXCollections.observableArrayList();
        Utility.UNITJPACONTROLLER.findUnitEntities().stream().forEach(unit -> {
            units.add(unit);
        });
        table.setItems(units);
    }

    private void addUnit() {

        addUnit.setOnAction(e -> {
            if (checkUnit()) {
                Unit u = new Unit();
                u.setName(unitName.getText().trim());
                u.setDescription(unitDescription.getText().trim());
                Utility.UNITJPACONTROLLER.create(u);
                Alert al = new Alert(Alert.AlertType.INFORMATION);
                al.setTitle("Congratulations");
                al.setContentText("Unit added successfully!");
                al.show();
                loadUnitTable();
                clearUnit();
            }
        });

    }

    private void clearUnit() {
        unitName.clear();
        unitDescription.clear();
    }

    private void unitTableListener() {
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        table.setOnKeyPressed((KeyEvent event) -> {
            if (event.getCode().equals(KeyCode.DELETE)) {
                deleteContextMenuItem.fire();
            }
        });
        deleteContextMenuItem.setOnAction(e -> {

            table.getSelectionModel().getSelectedItems().stream().forEach(unit -> {
                try {
                    Utility.UNITJPACONTROLLER.destroy(unit.getId());
                } catch (IllegalOrphanException | NonexistentEntityException ex) {
                    Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            loadUnitTable();
        });

    }

    private boolean checkUnit() {
        if (unitName.getText().trim().isEmpty()) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please enter the name!");
            al.showAndWait();
            return false;
        }
        return true;
    }

    private void controlControls() {
        UnaryOperator<Change> filter = change -> {
            String s = change.getText();
            if (s.matches("[0-9]*")) {
                return change;
            }
            return null;

        };
        TextFormatter<String> textFormatter = new TextFormatter<>(filter);
        quantityTextField.setTextFormatter(textFormatter);
        
        

    }

}
