/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import edu.princeton.cs.algs4.Queue;
import java.util.Date;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.Stock;
import jpa.StockJpaController;
import org.eclipse.persistence.internal.queries.EntityFetchGroup;

/**
 *
 * @author Deathrow
 */
public class PQ {

    @SuppressWarnings("FieldMayBeFinal")
    private ObservableList<StockTableViewModel> list;
    @SuppressWarnings("FieldMayBeFinal")
    private Queue<StockTableViewModel> queue;
    @SuppressWarnings("FieldMayBeFinal")
    private ObservableList<StockTableViewModel> orderedList;

    public PQ(ObservableList<StockTableViewModel> list) {
        this.list = list;
        queue = new Queue<>();
        orderedList = FXCollections.observableArrayList();
        enqueue();
    }

    private void enqueue() {
        list.stream().forEach((stockTableViewModel) -> {
            queue.enqueue(stockTableViewModel);
        });
    }

    public void printElements() {
        while (!queue.isEmpty()) {
            Date d = queue.dequeue().getProduct().getExpiredDate();
            System.out.println(d.getYear());
        }
    }

    public static void main(String[] args) {
        ObservableList<StockTableViewModel> givenlist = FXCollections.observableArrayList();
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WarehouseV2PU");
        StockJpaController stockJpaController = new StockJpaController(emf);
        stockJpaController.findStockEntities().forEach(stock -> {
            givenlist.add(new StockTableViewModel(stock));
        });
        PQ p = new PQ(givenlist);
        p.printElements();

    }

}
