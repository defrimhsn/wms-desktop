/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import static gui.controllers.AddExtraWarehouseDetailsController.name;
import static gui.controllers.AddExtraWarehouseDetailsController.value;
import java.net.URL;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author defri
 */
public class AddExtraDetailsController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Pane pane;

    @FXML
    private TextField nameField;

    @FXML
    private Button okButton;

    @FXML
    private Button cancelButton;

    @FXML
    private TextField valueField;

    public static HashMap<String, String> details = new HashMap<>();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        addDetail();
        cancelButton.setOnAction(e -> {
            Stage s = (Stage)pane.getScene().getWindow();
            s.close();
        });
      
    }

    private void addDetail() {
      okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                name = nameField.getText();
                value = valueField.getText();
                if (check()) {
                    details.put(name, value);
                    nameField.clear();
                    valueField.clear();
                    Alert al = new Alert(Alert.AlertType.NONE);
                    al.getButtonTypes().addAll(ButtonType.YES, ButtonType.NO);
                    al.setContentText("Do you want to add more details?");
                    al.setAlertType(Alert.AlertType.CONFIRMATION);
                    Optional<ButtonType> result = al.showAndWait();
                    if (result.get() == ButtonType.YES) {
                        addDetail();
                    } else {
                        Stage stage = (Stage) pane.getScene().getWindow();
                        stage.close();
                    }
                }
            }
        });
    }

    private boolean check() {
        if (nameField.getText().trim().isEmpty()) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please enter the name!");
            al.showAndWait();
            return false;
        } else if (valueField.getText().trim().isEmpty()) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please enter the value!");
            al.showAndWait();
            return false;
        }
        return true;
    }
}