/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.Location;
import jpa.LocationJpaController;
import jpa.Warehouse;
import jpa.WarehouseDetails;
import jpa.WarehouseDetailsJpaController;
import jpa.WarehouseJpaController;
import jpa.exceptions.NonexistentEntityException;

/**
 * FXML Controller class
 *
 * @author defri
 */
public class WarehouseController implements Initializable {

    @FXML
    private Pane mainPane;

    @FXML
    private ComboBox<String> countryCombobox;

    @FXML
    private ComboBox<String> cityCombobox;

    @FXML
    private ComboBox<String> streetsComboBox;

    @FXML
    private TextField nameField;

    @FXML
    private TextArea descriptionField;

    @FXML
    private Button addButton;

    @FXML
    private CheckBox extraDetailsCheckButton;

    @FXML
    private Label searchLABEL;

    @FXML
    private ComboBox<String> searchcategoryCombo;

    @FXML
    private TextField searchbox;

    @FXML
    private Button searchButton;

    @FXML
    private Pane editPane;

    @FXML
    private Button editButton;

    @FXML
    private Button deleteButton;

    @FXML
    private ComboBox<String> editcountryCombobox;

    @FXML
    private ComboBox<String> editcityCombobox;

    @FXML
    private ComboBox<String> editstreetsComboBox;

    @FXML
    private TextField editnameField;

    @FXML
    private TextArea editdescriptionField;

    @FXML
    private Button refreshTable;

    @FXML
    private TableView<WarehouseLocationTableView> tableview;

    @FXML
    private TableColumn<WarehouseLocationTableView, String> countryCol;

    @FXML
    private TableColumn<WarehouseLocationTableView, String> cityCol;

    @FXML
    private TableColumn<WarehouseLocationTableView, String> streetCol;

    @FXML
    private TableColumn<WarehouseLocationTableView, String> nameCol;

    @FXML
    private MenuItem editMenu;

    @FXML
    private MenuItem deleteMenu;

    @FXML
    private MenuItem viewDetails;

    private static LocationJpaController locationJpaController;
    public static WarehouseJpaController warehouseJpaController;
    public static WarehouseDetailsJpaController warehouseDetailsJpaController;

    public static HashMap<String, String> details = new HashMap<>();

    public static WarehouseLocationTableView warehouseLocationTableView;

    public static String choosedCountry;
    public static String choosedCity;
    public static String choosedStreet;

    public static boolean isWarehouse = false;
    public static Warehouse choosedWarehouse;

    public static ObservableList<WarehouseLocationTableView> list = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        transaction();
        initCountries();
        initCities();
        checkButton();
        addWarehouse();
        initTable();
        loadTable();
        contextMenu();
        loadSearchCategories();
    }

    private void transaction() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WarehouseV2PU");
        locationJpaController = new LocationJpaController(emf);
        warehouseJpaController = new WarehouseJpaController(emf);
        warehouseDetailsJpaController = new WarehouseDetailsJpaController(emf);

    }

    private void initCountries() {
        countryCombobox.setItems(getCountries());
    }

    private void initCities() {
        cityCombobox.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                choosedCountry = countryCombobox.getSelectionModel().getSelectedItem();
                cityCombobox.setItems(getCities());
            }
        });

        streetsComboBox.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                choosedCity = cityCombobox.getSelectionModel().getSelectedItem();
                streetsComboBox.setItems(getStreets());
            }
        });
    }

    public static ObservableList<String> getCountries() {
        ObservableList<String> items = FXCollections.observableArrayList();
        for (Location l : locationJpaController.findLocationEntities()) {
            if (l.getIsActive() == 1) {
                if (!items.contains(l.getCountry())) {
                    items.add(l.getCountry());
                }
            }
        }
        return items;
    }

    public static ObservableList<String> getCities() {
        ObservableList<String> items = FXCollections.observableArrayList();
        locationJpaController.findLocationEntities().stream().forEach((l) -> {
            if (l.getCountry().equals(choosedCountry) && choosedCountry != null) {
                if (l.getIsActive() == 1) {
                    if (!items.contains(l.getCity())) {
                        items.add(l.getCity());
                    }
                }
            }

        });
        return items;
    }

    public static ObservableList<String> getStreets() {
        ObservableList<String> items = FXCollections.observableArrayList();
        locationJpaController.findLocationEntities().stream().forEach((l) -> {
            if (l.getCity().equals(choosedCity) && choosedCity != null) {
                if (l.getIsActive() == 1) {
                    if (!items.contains(l.getStreet())) {
                        items.add(l.getStreet());
                    }
                }
            }

        });
        return items;
    }

    private void addWarehouse() {
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int id = 0;
                choosedStreet = streetsComboBox.getSelectionModel().getSelectedItem();
                for (Location location : locationJpaController.findLocationEntities()) {
                    if (location.getCountry().equals(choosedCountry) && location.getCity().equals(choosedCity) && location.getStreet().equals(choosedStreet)) {
                        id = location.getId();
                    }
                }
                if (id != 0) {
                    if (check()) {
                        String name = nameField.getText();
                        String description = descriptionField.getText();
                        Warehouse w = new Warehouse();
                        w.setName(name);
                        if (description != null && !description.trim().isEmpty()) {
                            w.setDescription(description);
                        }
                        w.setLocationId(locationJpaController.findLocation(id));
                        w.setIsActive(1);
                        w.setCreatedDate(Utility.nowDate());
                        warehouseJpaController.create(w);
                        if (!details.isEmpty()) {
                            for (Map.Entry pair : details.entrySet()) {
                                WarehouseDetails warehouseDetails = new WarehouseDetails();
                                warehouseDetails.setName(pair.getKey().toString());
                                warehouseDetails.setValue(pair.getValue().toString());
                                warehouseDetails.setWarehouseId(w);
                                warehouseDetails.setIsActive(1);
                                warehouseDetails.setCreatedDate(Utility.nowDate());
                                warehouseDetailsJpaController.create(warehouseDetails);
                            }

                        }
                        clear();

                    }

                }
            }
        });
    }

    private void clear() {
        countryCombobox.setValue("Choose your country!");
        cityCombobox.setValue("Choose your city!");
        streetsComboBox.setValue("Choose your street!");
        nameField.clear();
        descriptionField.clear();

    }

    private boolean check() {

        if (countryCombobox.getSelectionModel().getSelectedItem() == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Country field empty!");
            alert.show();
            return false;
        } else if (cityCombobox.getSelectionModel().getSelectedItem() == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("City field empty!");
            alert.show();
            return false;
        } else if (streetsComboBox.getSelectionModel().getSelectedItem() == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Street field empty!");
            alert.show();
            return false;
        } else if (nameField == null || nameField.getText().trim().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Name field empty!");
            alert.show();
            return false;
        } else if (existName(nameField.getText().trim())) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Name exists already!");
            alert.show();
            return false;
        } else {
            return true;
        }
    }

    private boolean existName(String name) {
        return warehouseJpaController.findWarehouseEntities().stream().anyMatch((warehouse) -> (name.equals(warehouse.getName())));
    }

    private void checkButton() {
        extraDetailsCheckButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (extraDetailsCheckButton.isSelected()) {
                    try {
                        Stage stage = new Stage();
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/AddExtraWarehouseDetails.fxml"));
                        Pane p = (Pane) loader.load();
                        Scene sc = new Scene(p);
                        stage.setScene(sc);
                        stage.show();
                    } catch (IOException ex) {
                        Logger.getLogger(WarehouseController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    private void initTable() {
        countryCol.setCellValueFactory(new PropertyValueFactory<>("country"));
        cityCol.setCellValueFactory(new PropertyValueFactory<>("city"));
        streetCol.setCellValueFactory(new PropertyValueFactory<>("street"));
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));

    }

    private void loadTable() {
        list.remove(0, list.size());
        warehouseJpaController.findWarehouseEntities().stream().forEach((warehouse) -> {
            if (warehouse.getIsActive() == 1) {
                list.add(new WarehouseLocationTableView(warehouse, warehouse.getLocationId()));
            }
        });
        tableview.setItems(list);
    }

    private void contextMenu() {
        tableview.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                warehouseLocationTableView = tableview.getSelectionModel().getSelectedItem();
                editcountryCombobox.setValue(warehouseLocationTableView.getCountry());
                editcityCombobox.setValue(warehouseLocationTableView.getCity());
                editstreetsComboBox.setValue(warehouseLocationTableView.getStreet());
                editnameField.setText(warehouseLocationTableView.getName());
                Timeline timeline = new Timeline();
                timeline.setCycleCount(2);
                timeline.setAutoReverse(true);
                timeline.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(editcountryCombobox.scaleXProperty(), 1.1),
                        new KeyValue(editcityCombobox.scaleXProperty(), 1.1),
                        new KeyValue(editstreetsComboBox.scaleXProperty(), 1.1),
                        new KeyValue(editnameField.scaleXProperty(), 1.1),
                        new KeyValue(editdescriptionField.scaleXProperty(), 1.1)));
                timeline.play();
                editdescriptionField.setText(warehouseLocationTableView.getWarehouse().getDescription());
                deleteMenu.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if (Utility.hasSomethingActive(warehouseLocationTableView.getWarehouse())) {
                            Alert al = new Alert(Alert.AlertType.ERROR);
                            al.setTitle("Error");
                            al.setContentText("To delete this warehouse, make sure you have deleted all the content!");
                            al.showAndWait();
                        } else {
                            warehouseLocationTableView.getWarehouse().setIsActive(0);
                            warehouseLocationTableView.getWarehouse().setUpdatedDate(Utility.nowDate());
                            try {
                                warehouseJpaController.edit(warehouseLocationTableView.getWarehouse());
                            } catch (NonexistentEntityException ex) {
                                Logger.getLogger(WarehouseController.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (Exception ex) {
                                Logger.getLogger(WarehouseController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            loadTable();
                        }

                    }
                }
                );

                viewDetails.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {
                            isWarehouse = true;
                            choosedWarehouse = warehouseLocationTableView.getWarehouse();
                            Stage s = new Stage(StageStyle.DECORATED);
                            s.setScene(new Scene((Pane) FXMLLoader.load(getClass().getResource("../fxml/ViewDetails.fxml"))));
                            s.initModality(Modality.APPLICATION_MODAL);
                            s.setResizable(false);
                            s.sizeToScene();
                            s.setTitle("Warehouse Details");
                            s.show();
                        } catch (IOException ex) {
                            Logger.getLogger(WarehouseController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });

                editButton.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if (checkforEdit()) {
                            warehouseLocationTableView.getWarehouse().getLocationId().setCountry(editcountryCombobox.getSelectionModel().getSelectedItem());
                            warehouseLocationTableView.getWarehouse().getLocationId().setCity(editcityCombobox.getSelectionModel().getSelectedItem());
                            warehouseLocationTableView.getWarehouse().getLocationId().setStreet(editstreetsComboBox.getSelectionModel().getSelectedItem());
                            warehouseLocationTableView.getWarehouse().setName(editnameField.getText());
                            warehouseLocationTableView.getWarehouse().setDescription(descriptionField.getText());
                            warehouseLocationTableView.getWarehouse().setUpdatedDate(Utility.nowDate());
                            try {
                                warehouseJpaController.edit(warehouseLocationTableView.getWarehouse());
                            } catch (NonexistentEntityException ex) {
                                Logger.getLogger(WarehouseController.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (Exception ex) {
                                Logger.getLogger(WarehouseController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            clearEditFields();
                            loadTable();
                            tableview.refresh();
                        }
                    }
                });

                deleteButton.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        warehouseLocationTableView.getWarehouse().setIsActive(0);
                        warehouseLocationTableView.getWarehouse().setUpdatedDate(Utility.nowDate());
                        try {
                            warehouseJpaController.edit(warehouseLocationTableView.getWarehouse());
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(WarehouseController.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (Exception ex) {
                            Logger.getLogger(WarehouseController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        loadTable();
                    }
                });

            }
        });
    }

    private boolean checkforEdit() {
        if (editcountryCombobox.getSelectionModel().getSelectedItem() == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Please choose a country!");
            alert.showAndWait();
            return false;
        } else if (editcityCombobox.getSelectionModel().getSelectedItem() == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Please choose a city!");
            alert.showAndWait();
            return false;
        } else if (editstreetsComboBox.getSelectionModel().getSelectedItem() == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Please choose a country!");
            alert.showAndWait();
            return false;
        } else if (editnameField.getText().trim().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Please type a name!");
            alert.showAndWait();
        }
        return true;
    }

    private void clearEditFields() {
        editcountryCombobox.setValue("Choose Country!");
        editcityCombobox.setValue("Choose City!");
        editstreetsComboBox.setValue("Choose Street");
        editnameField.clear();
        editdescriptionField.clear();
    }

    private void loadSearchCategories() {
        searchcategoryCombo.setItems(FXCollections.observableArrayList("Country", "City", "Street", "Name"));

        searchButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String str = searchcategoryCombo.getSelectionModel().getSelectedItem();
                if (str == null) {
                    Timeline timeline = new Timeline();
                    timeline.setAutoReverse(true);
                    timeline.setCycleCount(4);
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(200), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            searchLABEL.setText("required*");
                            searchLABEL.textFillProperty().set(Color.RED);
                            searchcategoryCombo.setStyle("-fx-border-style: solid; -fx-border-width:1px; -fx-border-color:red; -fx-border-radius:3px;");
                        }
                    },
                            new KeyValue(searchcategoryCombo.layoutXProperty(), 101),
                            new KeyValue(searchcategoryCombo.layoutXProperty(), 96)));
                    timeline.setOnFinished(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            searchLABEL.setText("Search by*");
                            searchLABEL.textFillProperty().set(Color.BLACK);
                            searchcategoryCombo.setStyle("-fx-border-style: none; ");
                        }
                    });
                    timeline.play();

                } else {
                    ObservableList<WarehouseLocationTableView> result = FXCollections.observableArrayList();

                    if (str.equals("Country")) {
                        for (Warehouse w : warehouseJpaController.findWarehouseEntities()) {
                            if (w.getLocationId().getCountry().toLowerCase().contains(searchbox.getText().toLowerCase())) {
                                if (w.getIsActive() == 1) {
                                    result.add(new WarehouseLocationTableView(w, w.getLocationId()));
                                }
                            }

                        }
                    }

                    if (str.equals("City")) {
                        for (Warehouse w : warehouseJpaController.findWarehouseEntities()) {
                            if (w.getLocationId().getCity().toLowerCase().contains(searchbox.getText().toLowerCase())) {
                                if (w.getIsActive() == 1) {
                                    result.add(new WarehouseLocationTableView(w, w.getLocationId()));
                                }
                            }
                        }
                    }

                    if (str.equals("Street")) {
                        for (Warehouse w : warehouseJpaController.findWarehouseEntities()) {
                            if (w.getLocationId().getStreet().toLowerCase().contains(searchbox.getText().toLowerCase())) {
                                if (w.getIsActive() == 1) {
                                    result.add(new WarehouseLocationTableView(w, w.getLocationId()));
                                }
                            }
                        }
                    }

                    if (str.equals("Name")) {
                        for (Warehouse w : warehouseJpaController.findWarehouseEntities()) {
                            if (w.getName().contains(searchbox.getText().toLowerCase())) {
                                if (w.getIsActive() == 1) {
                                    result.add(new WarehouseLocationTableView(w, w.getLocationId()));
                                }
                            }
                        }
                    }

                    tableview.setItems(result);

                    refreshTable.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            loadTable();
                        }
                    });

                }
            }
        });

    }

}
