/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.Category;
import jpa.CategoryJpaController;
import jpa.Product;
import jpa.ProductDetails;
import jpa.ProductDetailsJpaController;
import jpa.ProductJpaController;
import jpa.ProductUnit;
import jpa.ProductUnitJpaController;
import jpa.Sector;
import jpa.SectorJpaController;
import jpa.Shelf;
import jpa.ShelfJpaController;
import jpa.Unit;
import jpa.UnitJpaController;
import jpa.Warehouse;
import jpa.WarehouseJpaController;
import jpa.exceptions.NonexistentEntityException;

/**
 * FXML Controller class
 *
 * @author defri
 */
public class ProductController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TextField barcodeTextField;

    @FXML
    private TextField nameTextField;

    @FXML
    private ComboBox<Warehouse> chooseWarehouseComboBox;

    @FXML
    private ComboBox<Sector> chooseSectorComboBox;

    @FXML
    private ComboBox<Category> chooseCategoryComboBox;

    @FXML
    private ComboBox<Shelf> chooseShelfComboBox;

    @FXML
    private DatePicker expirationDatePicker;

    @FXML
    private TextField priceTextField;

    @FXML
    private TextArea descriptionTextArea;

    @FXML
    private CheckBox addExtraDetails;

    @FXML
    private Button addButton;

    @FXML
    private Button cancelButton;

    @FXML
    private TableView<ProductTableViewModel> tableView;

    @FXML
    private TableColumn<ProductTableViewModel, String> barcodeCol;

    @FXML
    private TableColumn<ProductTableViewModel, String> nameCol;

    @FXML
    private TableColumn<ProductTableViewModel, Category> categoryCol;

    @FXML
    private TableColumn<ProductTableViewModel, Double> priceCol;

    @FXML
    private TableColumn<ProductTableViewModel, Date> expirationDateCol;

    @FXML
    private TableColumn<ProductTableViewModel, Warehouse> warehouseCol;

    @FXML
    private TableColumn<ProductTableViewModel, Sector> sectorCol;

    @FXML
    private TableColumn<ProductTableViewModel, Shelf> shelfCol;

    @FXML
    private TableColumn<ProductTableViewModel, Unit> unitCol;

    @FXML
    private TableColumn<ProductTableViewModel, String> descriptionCol;

    @FXML
    private MenuItem editMenuItem;

    @FXML
    private ImageView addUnit;

    @FXML
    private MenuItem deleteMenuItem;

    @FXML
    private ComboBox<Unit> chooseUnit;

    @FXML
    private MenuItem viewDetailsMenuItem;

    @FXML
    private MenuItem refreshTable;

    private WarehouseJpaController warehouseJpaController;
    private SectorJpaController sectorJpaController;
    private ShelfJpaController shelfJpaController;
    private CategoryJpaController categoryJpaController;
    private ProductJpaController productJpaController;
    private ProductDetailsJpaController productDetailsJpaController;
    private ProductUnitJpaController productUnitJpaController;
    private UnitJpaController unitJpaController;

    public static boolean isProduct = false;
    public static Product choosedProduct;
    public static Unit unit;
    public static Unit editUnit;

    public static Product choosedProducttoEdit;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        transaction();
        addProduct();
        combosListener();
        initProductTable();
        loadProductTable();
        checkButton();
        tableListener();
        loadCombos();
        contextMenu();
        loadUnits();
        addUnit();

        chooseUnit.setOnMousePressed(e -> {
            loadUnits();
        });
    }

    private void transaction() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WarehouseV2PU");
        warehouseJpaController = new WarehouseJpaController(emf);
        sectorJpaController = new SectorJpaController(emf);
        shelfJpaController = new ShelfJpaController(emf);
        categoryJpaController = new CategoryJpaController(emf);
        productJpaController = new ProductJpaController(emf);
        productDetailsJpaController = new ProductDetailsJpaController(emf);
        productUnitJpaController = new ProductUnitJpaController(emf);
        unitJpaController = new UnitJpaController(emf);
    }

    private void addProduct() {
        addButton.setOnAction((ActionEvent event) -> {
            if (check()) {
                Product product = new Product();
                product.setName(nameTextField.getText());
                product.setBarcode(barcodeTextField.getText());
                product.setIsActive(1);
                product.setCreatedDate(Utility.nowDate());
                product.setPrice(Double.parseDouble(priceTextField.getText()));
                product.setShelfId(chooseShelfComboBox.getSelectionModel().getSelectedItem());
                product.getShelfId().setSectorId(chooseSectorComboBox.getValue());
                product.getShelfId().getSectorId().setWarehouseId(chooseWarehouseComboBox.getSelectionModel().getSelectedItem());
                product.setExpiredDate(java.sql.Date.valueOf(expirationDatePicker.getValue()));
                product.setCategoryId(chooseCategoryComboBox.getSelectionModel().getSelectedItem());
                product.setDescription(descriptionTextArea.getText());
                productJpaController.create(product);
                if (exist(product)) {
                    Alert al = new Alert(Alert.AlertType.INFORMATION);
                    al.setTitle("Item added Successfully!");
                    al.setContentText(al.getTitle());
                    al.showAndWait();
                }
                this.unit = chooseUnit.getSelectionModel().getSelectedItem();
                ProductUnit productUnit = new ProductUnit();
                productUnit.setProductId(product);
                productUnit.setUnitId(unit);
                productUnitJpaController.create(productUnit);
                if (addExtraDetails.isSelected()) {
                    for (Map.Entry pair : AddExtraDetailsController.details.entrySet()) {
                        ProductDetails productDetails = new ProductDetails();
                        productDetails.setName(pair.getKey().toString());
                        productDetails.setValue(pair.getValue().toString());
                        productDetails.setIsActive(1);
                        productDetails.setCreatedDate(Utility.nowDate());
                        productDetails.setProductId(product);
                        productDetailsJpaController.create(productDetails);
                    }
                }

                clearAddFields();
                loadProductTable();

            }
        });
    }

    private void checkButton() {
        AddExtraDetailsController.details.clear();
        addExtraDetails.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (addExtraDetails.isSelected()) {
                    Stage s = new Stage();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/AddExtraDetails.fxml"));
                    try {
                        Pane pane = (Pane) loader.load();
                        s.setScene(new Scene(pane));
                        s.show();
                    } catch (IOException ex) {
                        Logger.getLogger(SectorController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    private boolean exist(Product pr) {
        return productJpaController.findProductEntities().stream().anyMatch((product) -> (product.getIsActive() == 1 && product.equals(pr)));

    }

    private void clearAddFields() {
        chooseWarehouseComboBox.setValue(null);
        chooseSectorComboBox.setValue(null);
        chooseShelfComboBox.setValue(null);
        chooseCategoryComboBox.setValue(null);
        nameTextField.clear();
        barcodeTextField.clear();
        priceTextField.clear();
        expirationDatePicker.setValue(null);
        addExtraDetails.setSelected(false);
        descriptionTextArea.clear();
    }

    private void initProductTable() {
        barcodeCol.setCellValueFactory(new PropertyValueFactory<>("barcode"));
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        categoryCol.setCellValueFactory(new PropertyValueFactory<>("category"));
        warehouseCol.setCellValueFactory(new PropertyValueFactory<>("warehouse"));
        sectorCol.setCellValueFactory(new PropertyValueFactory<>("sector"));
        shelfCol.setCellValueFactory(new PropertyValueFactory<>("shelf"));
        unitCol.setCellValueFactory(new PropertyValueFactory<>("unit"));
        descriptionCol.setCellValueFactory(new PropertyValueFactory<>("description"));
        priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        expirationDateCol.setCellValueFactory(new PropertyValueFactory<>("expirationDate"));

    }

    private void loadProductTable() {
        ObservableList<ProductTableViewModel> productList = FXCollections.observableArrayList();
        productJpaController.findProductEntities().stream().filter((product) -> (product.getIsActive() == 1)).forEach((product) -> {
            productList.add(new ProductTableViewModel(product));
        });
        tableView.setItems(productList);

    }

    private void tableListener() {

        tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadProductTable();

                ProductTableViewModel productTableViewModel = tableView.getSelectionModel().getSelectedItem();

                deleteMenuItem.setOnAction((ActionEvent event1) -> {
                    ProductTableViewModel productTableViewModel1 = tableView.getSelectionModel().getSelectedItem();
                    productTableViewModel1.getProduct().setIsActive(0);
                    productTableViewModel1.getProduct().setUpdatedDate(Utility.nowDate());
                    for (ProductDetails productDetails : productDetailsJpaController.findProductDetailsEntities()) {
                        if (productDetails.getProductId().equals(productTableViewModel1.getProduct())) {
                            try {
                                productDetailsJpaController.destroy(productDetails.getId());
                            } catch (NonexistentEntityException ex) {
                                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                    for (ProductUnit productUnit : productUnitJpaController.findProductUnitEntities()) {
                        if (productUnit.getProductId().equals(productTableViewModel1.getProduct())) {
                            try {
                                productUnitJpaController.destroy(productUnit.getId());
                            } catch (NonexistentEntityException ex) {
                                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                    try {
                        productJpaController.edit(productTableViewModel1.getProduct());
                    } catch (NonexistentEntityException ex) {
                        Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    loadProductTable();
                    productTableViewModel1 = null;
                });

                editMenuItem.setOnAction((ActionEvent event1) -> {
                    choosedProducttoEdit = productTableViewModel.getProduct();
                    editUnit = productTableViewModel.getUnit();
                    System.out.println(editUnit);
                    Stage s = new Stage(StageStyle.DECORATED);
                    try {
                        s.setScene(new Scene((Pane) FXMLLoader.load(getClass().getResource("../fxml/EditProduct.fxml"))));
                    } catch (IOException ex) {
                        Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    s.initModality(Modality.APPLICATION_MODAL);
                    s.setResizable(false);
                    s.sizeToScene();
                    s.setTitle("Product Details");
                    s.show();
                });

                viewDetailsMenuItem.setOnAction((ActionEvent event1) -> {
                    try {
                        isProduct = true;
                        choosedProduct = productTableViewModel.getProduct();
                        Stage s = new Stage(StageStyle.DECORATED);
                        s.setScene(new Scene((Pane) FXMLLoader.load(getClass().getResource("../fxml/ViewDetails.fxml"))));
                        s.initModality(Modality.APPLICATION_MODAL);
                        s.setResizable(false);
                        s.sizeToScene();
                        s.setTitle("Product Details");
                        s.show();
                    } catch (IOException ex) {
                        Logger.getLogger(WarehouseController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
            }
        });

    }

    private boolean check() {
        if (chooseWarehouseComboBox.getSelectionModel().getSelectedItem() == null) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please choose a warehouse!");
            al.showAndWait();
            return false;
        } else if (chooseSectorComboBox.getSelectionModel().getSelectedItem() == null) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please choose a sector!");
            al.showAndWait();
            return false;
        } else if (chooseShelfComboBox.getSelectionModel().getSelectedItem() == null) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please choose a shelf!");
            al.showAndWait();
            return false;
        } else if (chooseCategoryComboBox.getSelectionModel().getSelectedItem() == null) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please choose a category!");
            al.showAndWait();
            return false;
        } else if (barcodeTextField.getText() == null || barcodeTextField.getText().trim().isEmpty()) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please enter a barcode!");
            al.showAndWait();
            return false;
        } else if (nameTextField.getText() == null || nameTextField.getText().trim().isEmpty()) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please enter a name!");
            al.showAndWait();
            return false;
        } else if (expirationDatePicker.getValue() == null) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please select the expiration date of this product!");
            al.showAndWait();
            return false;
        } else if (priceTextField.getText() == null || priceTextField.getText().trim().isEmpty()) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please enter the price for this product!");
            al.showAndWait();
            return false;
        } else if (chooseUnit.getSelectionModel().getSelectedItem() == null) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning!");
            al.setContentText("Please choose a unit!");
            al.showAndWait();
            return false;

        }
        return true;
    }

    private void loadCombos() {
        ObservableList<Warehouse> list = FXCollections.observableArrayList();
        warehouseJpaController.findWarehouseEntities().stream().filter((warehouse) -> (warehouse.getIsActive() == 1)).forEach((warehouse) -> {
            list.add(warehouse);
        });
        chooseWarehouseComboBox.setItems(list);
    }

    private void combosListener() {
        chooseWarehouseComboBox.setOnAction((ActionEvent event) -> {
            Warehouse choosedWarehouse = chooseWarehouseComboBox.getSelectionModel().getSelectedItem();

            ObservableList<Sector> sec = FXCollections.observableArrayList();
            sectorJpaController.findSectorEntities().stream().filter((sector) -> (sector.getIsActive() == 1 && sector.getWarehouseId().equals(choosedWarehouse))).forEach((sector) -> {
                sec.add(sector);
            });
            chooseSectorComboBox.setItems(sec);

            chooseSectorComboBox.setOnAction((ActionEvent event1) -> {
                Sector choosedSector = chooseSectorComboBox.getSelectionModel().getSelectedItem();
                ObservableList<Shelf> shelves = FXCollections.observableArrayList();
                ObservableList<Category> categories = FXCollections.observableArrayList();
                shelfJpaController.findShelfEntities().stream().filter((sh) -> (sh.getIsActive() == 1 && sh.getSectorId().equals(choosedSector))).forEach((sh) -> {
                    shelves.add(sh);
                });
                chooseShelfComboBox.setItems(shelves);
                categoryJpaController.findCategoryEntities().stream().filter((c) -> (c.getIsActive() == 1 && c.getSectorId().equals(choosedSector))).forEach((c) -> {
                    categories.add(c);
                });
                chooseCategoryComboBox.setItems(categories);
            });
        });
    }

    private void contextMenu() {
        viewDetailsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    Stage s = new Stage(StageStyle.DECORATED);
                    s.setScene(new Scene((Pane) FXMLLoader.load(getClass().getResource("../fxml/ViewDetails.fxml"))));
                    s.initModality(Modality.APPLICATION_MODAL);
                    s.setResizable(false);
                    s.sizeToScene();
                    s.setTitle("Product Details");
                    s.show();
                } catch (IOException ex) {
                    Logger.getLogger(WarehouseController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        refreshTable.setOnAction(ev -> {
            ObservableList<ProductTableViewModel> productList = FXCollections.observableArrayList();
            productJpaController.findProductEntities().stream().filter((product) -> (product.getIsActive() == 1)).forEach((product) -> {
                productList.add(new ProductTableViewModel(product));
            });
            tableView.setItems(productList);
        });
    }

    private void loadUnits() {
        chooseUnit.setItems(FXCollections.observableArrayList(unitJpaController.findUnitEntities()));
    }

    private void addUnit() {
        addUnit.setOnMousePressed(e -> {
            try {
                Stage s = new Stage();
                s.setScene(new Scene((Pane) FXMLLoader.load(getClass().getResource("../fxml/UnitPopUp.fxml"))));
                s.show();
            } catch (IOException ex) {
                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

}
