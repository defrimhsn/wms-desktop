/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import jpa.Location;
import jpa.Warehouse;

/**
 *
 * @author Deathrow
 */
public class WarehouseLocationTableView {

    private Warehouse warehouse;
    private Location location;
    
    private String country;
    private String city;
    private String street;
    private String name;

    public WarehouseLocationTableView(Warehouse warehouse, Location location) {
        this.warehouse = warehouse;
        this.location = location;
        country = this.location.getCountry();
        city = this.location.getCity();
        street = this.location.getStreet();
        name = this.warehouse.getName();
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public Location getLocation() {
        return location;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public String getName() {
        return name;
    }
    
    @Override
    public String toString(){
        return "---\n"+"Country : "+country+"\n"+"City : "+city+"\n"+"Street : "+street+"\n"+"Name : "+name;
    }

}

