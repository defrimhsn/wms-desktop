/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import java.util.Date;
import java.util.Objects;
import javax.persistence.Persistence;
import jpa.Category;
import jpa.Product;
import jpa.ProductUnit;
import jpa.ProductUnitJpaController;
import jpa.Sector;
import jpa.Shelf;
import jpa.Unit;
import jpa.Warehouse;

/**
 *
 * @author Deathrow
 */
public class ProductTableViewModel {

    private Product product;
    private String barcode;
    private String name;
    private Category category;
    private double price;
    private Date expirationDate;
    private Warehouse warehouse;
    private Sector sector;
    private Shelf shelf;
    private String description;
    private Unit unit;

    public ProductTableViewModel(Product product) {
        this.product = product;
        this.barcode = product.getBarcode();
        this.name = product.getName();
        this.category = product.getCategoryId();
        this.price = product.getPrice();
        getUnitfromProduct();
        this.expirationDate = product.getExpiredDate();
        this.warehouse = product.getShelfId().getSectorId().getWarehouseId();
        this.sector = product.getShelfId().getSectorId();
        this.shelf = product.getShelfId();
        this.description = product.getDescription();

    }

    private void getUnitfromProduct() {
        Unit u = new Unit();
        ProductUnitJpaController productUnitJpaController = new ProductUnitJpaController(Persistence.createEntityManagerFactory("WarehouseV2PU"));
        for (ProductUnit productUnit : productUnitJpaController.findProductUnitEntities()) {
            if (productUnit.getProductId().equals(this.product)) {
                u = productUnit.getUnitId();
            }
        }
        this.unit = u;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public Shelf getShelf() {
        return shelf;
    }

    public void setShelf(Shelf shelf) {
        this.shelf = shelf;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ProductTableViewModel) {
            ProductTableViewModel productTableViewModel = (ProductTableViewModel) obj;
            if (Objects.equals(productTableViewModel.getProduct().getId(), product.getId())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.product);
        hash = 19 * hash + Objects.hashCode(this.barcode);
        hash = 19 * hash + Objects.hashCode(this.name);
        hash = 19 * hash + Objects.hashCode(this.category);
        hash = 19 * hash + (int) (Double.doubleToLongBits(this.price) ^ (Double.doubleToLongBits(this.price) >>> 32));
        hash = 19 * hash + Objects.hashCode(this.expirationDate);
        hash = 19 * hash + Objects.hashCode(this.warehouse);
        hash = 19 * hash + Objects.hashCode(this.sector);
        hash = 19 * hash + Objects.hashCode(this.shelf);
        hash = 19 * hash + Objects.hashCode(this.description);
        return hash;
    }

    @Override
    public String toString() {
        return product.getName();
    }

}
