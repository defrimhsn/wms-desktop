/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author defri
 */
public class AddExtraWarehouseDetailsController implements Initializable {
    
    @FXML
    private Pane pane;
    
    @FXML
    private TextField nameField;
    
    @FXML
    private Button okButton;
    
    @FXML
    private Button cancelButton;
    
    @FXML
    private TextField valueField;
    
    public static String name;
    public static String value;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        buttonsAction();
    }
    
    private void buttonsAction() {
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Stage stage = (Stage) pane.getScene().getWindow();
                stage.close();
            }
        });
        
        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                name = nameField.getText();
                value = valueField.getText();
                if (check()) {
                    WarehouseController.details.put(name, value);
                    nameField.clear();
                    valueField.clear();
                    Alert al = new Alert(Alert.AlertType.NONE);
                    al.getButtonTypes().addAll(ButtonType.YES, ButtonType.NO);
                    al.setContentText("Do you want to add more details?");
                    al.setAlertType(Alert.AlertType.CONFIRMATION);
                    Optional<ButtonType> result = al.showAndWait();
                    if (result.get() == ButtonType.YES) {
                        buttonsAction();
                    } else {
                        Stage stage = (Stage) pane.getScene().getWindow();
                        stage.close();
                    }
                }
            }
        });
        
    }
    
    private boolean check() {
        if (name == null || name.trim().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Name field empty!");
            alert.show();
            return false;
        } else if (value == null || value.trim().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Value field empty!");
            alert.show();
            return false;
        } else {
            return true;
        }
        
    }
    
}
