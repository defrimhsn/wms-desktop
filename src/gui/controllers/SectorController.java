/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.Sector;
import jpa.SectorDetails;
import jpa.SectorDetailsJpaController;
import jpa.SectorJpaController;
import jpa.Warehouse;
import jpa.WarehouseJpaController;
import jpa.exceptions.NonexistentEntityException;

/**
 * FXML Controller class
 *
 * @author Deathrow
 */
public class SectorController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private ComboBox<Warehouse> chooseWarehouse;

    @FXML
    private TextField sectorName;

    @FXML
    private TextArea descriptionField;

    @FXML
    private Button addButton;

    @FXML
    private Button cancelButton;

    @FXML
    private Pane searchPane;

    @FXML
    private Label searchLABEL;

    @FXML
    private ComboBox<String> searchcategoryCombo;

    @FXML
    private TextField searchbox;

    @FXML
    private Button searchButton;

    @FXML
    private Pane editPane;

    @FXML
    private Button editButton;

    @FXML
    private MenuItem deleteMenuItem;

    @FXML
    private Button deleteButton;

    @FXML
    private ComboBox<Warehouse> editWarehouse;

    @FXML
    private TableView<Sector> tableView;

    @FXML
    private TableColumn<Sector, String> nameCol;

    @FXML
    private TableColumn<Sector, String> descriptonCol;

    @FXML
    private CheckBox addExtraDetails;

    @FXML
    private ComboBox<Warehouse> warehouseComboBox;

    @FXML
    private TextField editNameField;

    @FXML
    private Button refreshTable;

    @FXML
    private TextArea editDescripton;

    @FXML
    private MenuItem viewDetails;

    public static boolean isSector = false;
    public static Sector choosedSector;

    private WarehouseJpaController warehouseJpaController;
    private SectorJpaController sectorJpaController;
    private SectorDetailsJpaController sectorDetailsJpaController;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        transaction();
        loadWarehouseComboBox(chooseWarehouse);
        editButton.setDisable(true);
        deleteButton.setDisable(true);
        loadWarehouseComboBox(editWarehouse);
        addSector();
        initTable();
        loadTable();
        tableListenter();
        checkButton();
        loadSearchCombo();
        refreshTable();
        comboBoxListener();
        loadWarehouseComboBox(warehouseComboBox);
        Utility.validateControl(sectorName);
        Utility.validateControl(descriptionField);
        Utility.validateControl(chooseWarehouse);

    }

    private void transaction() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WarehouseV2PU");
        warehouseJpaController = new WarehouseJpaController(emf);
        sectorJpaController = new SectorJpaController(emf);
        sectorDetailsJpaController = new SectorDetailsJpaController(emf);

    }

    private void loadWarehouseComboBox(ComboBox<Warehouse> comb) {
        ObservableList<Warehouse> list = FXCollections.observableArrayList();
        warehouseJpaController.findWarehouseEntities().stream().filter((w) -> (w.getIsActive() == 1)).forEach((w) -> {
            list.add(w);
        });
        comb.setItems(list);
    }

    private void addSector() {
        addButton.setOnAction((ActionEvent event) -> {
            if (check()) {
                
                Sector s = new Sector();
                s.setName(sectorName.getText().trim());
                s.setIsActive(1);
                s.setWarehouseId(chooseWarehouse.getSelectionModel().getSelectedItem());
                s.setCreatedDate(Utility.nowDate());
                s.setDescription(descriptionField.getText());
                sectorJpaController.create(s);
                
                if (addExtraDetails.isSelected()) {
                    for (Map.Entry m : AddExtraDetailsController.details.entrySet()) {
                        SectorDetails sectorDetails = new SectorDetails();
                        sectorDetails.setIsActive(1);
                        sectorDetails.setName(m.getKey().toString());
                        sectorDetails.setValue(m.getValue().toString());
                        sectorDetails.setCreatedDate(Utility.nowDate());
                        sectorDetails.setSectorId(s);
                        sectorDetailsJpaController.create(sectorDetails);
                    }
                } else {
                    
                }
                if (exist(s.getName())) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("Sector added successfully!");
                    alert.showAndWait();
                }
                
                clear();
                AddExtraDetailsController.details.entrySet().removeAll(AddExtraDetailsController.details.entrySet());
                addExtraDetails.setSelected(false);
            }
        });
    }

    private boolean check() {

        if (chooseWarehouse.getSelectionModel().getSelectedItem() == null) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please choose a Warehouse!");
            al.showAndWait();
            return false;
        } else if (sectorName.getText().trim().isEmpty()) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please enter a name!");
            al.showAndWait();
            return false;
        } else if (exist(sectorName.getText().trim())) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("This sector exists already");
            al.showAndWait();
            return false;
        }
        return true;
    }

    private boolean exist(String trim) {
        return sectorJpaController.findSectorEntities().stream().anyMatch((s) -> (s.getIsActive() == 1 && s.getName().equals(trim)));
    }

    private void clear() {
        chooseWarehouse.setValue(null);
        chooseWarehouse.setPromptText("Choose Warehouse");
        sectorName.clear();
        descriptionField.clear();
    }

    private void initTable() {
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        descriptonCol.setCellValueFactory(new PropertyValueFactory<>("description"));
    }

    private void loadTable() {
        ObservableList<Sector> list = FXCollections.observableArrayList();
        sectorJpaController.findSectorEntities().stream().filter((s) -> (s.getIsActive() == 1)).forEach((s) -> {
            list.add(s);
        });
        tableView.setItems(list);
    }

    private void tableListenter() {

        tableView.setOnMousePressed((MouseEvent event) -> {
            Sector s = tableView.getSelectionModel().getSelectedItem();
            if (s != null) {
                editButton.setDisable(false);
                deleteButton.setDisable(false);
                
                Timeline timeline = new Timeline();
                timeline.setAutoReverse(true);
                timeline.setCycleCount(2);
                timeline.getKeyFrames().add(new KeyFrame(Duration.millis(100), new KeyValue(editNameField.scaleXProperty(), 1.1),
                        new KeyValue(editDescripton.scaleXProperty(), 1.1), new KeyValue(editWarehouse.scaleXProperty(), 1.1)));
                timeline.play();
                editNameField.setText(s.getName());
                editDescripton.setText(s.getDescription());
                editWarehouse.setValue(s.getWarehouseId());
                
                deleteButton.setOnAction((ActionEvent event1) -> {
                    if (Utility.hasSomethingActive(s)) {
                        Alert al = new Alert(Alert.AlertType.ERROR);
                        al.setTitle("Error");
                        al.setContentText("To delete this Sector, make sure you have deleted all the content!");
                        al.showAndWait();
                    } else {
                        s.setIsActive(0);
                        s.setUpdatedDate(Utility.nowDate());
                        try {
                            sectorJpaController.edit(s);
                            loadTable();
                            editWarehouse.setValue(null);
                            editWarehouse.setPromptText("Choose Warehouse");
                            editNameField.clear();
                            editDescripton.clear();
                            tableView.refresh();
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(SectorController.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (Exception ex) {
                            Logger.getLogger(SectorController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
                
                deleteMenuItem.setOnAction((ActionEvent event1) -> {
                    deleteButton.fire();
                });
                
                viewDetails.setOnAction((ActionEvent event1) -> {
                    try {
                        isSector = true;
                        choosedSector = s;
                        Stage s1 = new Stage(StageStyle.DECORATED);
                        s1.setScene(new Scene((Pane) FXMLLoader.load(getClass().getResource("../fxml/ViewDetails.fxml"))));
                        s1.initModality(Modality.APPLICATION_MODAL);
                        s1.setResizable(false);
                        s1.sizeToScene();
                        s1.setTitle("Sector Details");
                        s1.show();
                    } catch (IOException ex) {
                        Logger.getLogger(WarehouseController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                
                editButton.setOnAction((ActionEvent event1) -> {
                    if (checkForEdit()) {
                        s.setUpdatedDate(Utility.nowDate());
                        s.setName(editNameField.getText().trim());
                        s.setDescription(descriptionField.getText().trim());
                        s.setWarehouseId(editWarehouse.getSelectionModel().getSelectedItem());
                        try {
                            sectorJpaController.edit(s);
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(SectorController.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (Exception ex) {
                            Logger.getLogger(SectorController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        loadTable();
                        tableView.refresh();
                        editWarehouse.setValue(null);
                        editWarehouse.setPromptText("Choose Warehouse");
                        editNameField.clear();
                        editDescripton.clear();
                        
                    }
                });
            }
        });
    }

    private void comboBoxListener() {
        searchcategoryCombo.setOnAction((ActionEvent event) -> {
            if (searchcategoryCombo.getSelectionModel().getSelectedItem().equals("Warehouse")) {
                warehouseComboBox.opacityProperty().set(0);
                Timeline timeline = new Timeline();
                timeline.getKeyFrames().add(new KeyFrame(Duration.millis(500),
                        new KeyValue(searchbox.scaleXProperty(), 0),
                        new KeyValue(warehouseComboBox.layoutXProperty(), 300),
                        new KeyValue(warehouseComboBox.opacityProperty(), 1.0)));
                timeline.play();
            }
            if (searchcategoryCombo.getSelectionModel().getSelectedItem().equals("Name")) {
                warehouseComboBox.opacityProperty().set(1);
                Timeline timeline = new Timeline();
                timeline.getKeyFrames().add(new KeyFrame(Duration.millis(500),
                        new KeyValue(searchbox.scaleXProperty(), 1.0),
                        new KeyValue(warehouseComboBox.layoutXProperty(), 98),
                        new KeyValue(warehouseComboBox.opacityProperty(), 0.0)));
                timeline.play();
            }
        });
    }

    private boolean checkForEdit() {
        if (editWarehouse.getSelectionModel().getSelectedItem() == null) {
            Alert al = new Alert(AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please choose a warehouse!");
            al.showAndWait();
            return false;
        } else if (editNameField.getText().trim().isEmpty()) {
            Alert al = new Alert(AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please enter the name!");
            al.showAndWait();
            return false;
        }
        if (descriptonCol.getText().trim().isEmpty()) {
            Alert al = new Alert(AlertType.WARNING);
            al.setTitle("Warning");
            al.setContentText("Please enter the description!");
            al.showAndWait();
            return false;
        }
        return true;
    }

    private void checkButton() {
        addExtraDetails.setOnAction((ActionEvent event) -> {
            if (addExtraDetails.isSelected()) {
                Stage s = new Stage();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/AddExtraDetails.fxml"));
                try {
                    Pane pane = (Pane) loader.load();
                    s.setScene(new Scene(pane));
                    s.show();
                } catch (IOException ex) {
                    Logger.getLogger(SectorController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    private void loadSearchCombo() {
        searchcategoryCombo.setItems(FXCollections.observableArrayList("Warehouse", "Name"));

        searchButton.setOnAction((ActionEvent event) -> {
            String str = searchcategoryCombo.getSelectionModel().getSelectedItem();
            ObservableList<Sector> result = FXCollections.observableArrayList();
            if (str == null) {
                Timeline timeline = new Timeline();
                timeline.setAutoReverse(true);
                timeline.setCycleCount(4);
                timeline.getKeyFrames().add(new KeyFrame(Duration.millis(200), (ActionEvent event1) -> {
                    searchLABEL.setText("required*");
                    searchLABEL.textFillProperty().set(Color.RED);
                    searchcategoryCombo.setStyle("-fx-border-style: solid; -fx-border-width:1px; -fx-border-color:red; -fx-border-radius:3px;");
                },
                        new KeyValue(searchcategoryCombo.layoutXProperty(), 101),
                        new KeyValue(searchcategoryCombo.layoutXProperty(), 96)));
                timeline.setOnFinished((ActionEvent event1) -> {
                    searchLABEL.setText("Search by*");
                    searchLABEL.textFillProperty().set(Color.BLACK);
                    searchcategoryCombo.setStyle("-fx-border-style: none; ");
                });
                timeline.play();
            } else {
                if (str.equals("Warehouse")) {
                    if (warehouseComboBox.getSelectionModel().getSelectedItem() == null) {
                        Timeline timeline = new Timeline();
                        timeline.setAutoReverse(true);
                        timeline.setCycleCount(4);
                        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(200), (ActionEvent event1) -> {
                            warehouseComboBox.setStyle("-fx-border-style: solid; -fx-border-width:1px; -fx-border-color:red; -fx-border-radius:3px;");
                        },
                                new KeyValue(warehouseComboBox.layoutXProperty(), 297),
                                new KeyValue(warehouseComboBox.layoutXProperty(), 302)));
                        timeline.setOnFinished((ActionEvent event1) -> {
                            warehouseComboBox.setStyle("-fx-border-style: none; ");
                        });
                        timeline.play();
                    }
                    result.removeAll(result);
                    sectorJpaController.findSectorEntities().stream().filter((sec) -> (sec.getIsActive() == 1 && sec.getWarehouseId().equals(warehouseComboBox.getSelectionModel().getSelectedItem()))).forEach((sec) -> {
                        result.add(sec);
                    });

                    tableView.setItems(result);

                }
                if (str.equals("Name")) {
                    if (searchbox.getText() == null || searchbox.getText().trim().isEmpty()) {
                        loadTable();

                    } else {
                        result.removeAll(result);
                        sectorJpaController.findSectorEntities().stream().filter((s) -> (s.getIsActive() == 1 && s.getName().toLowerCase().contains(searchbox.getText().toLowerCase().trim()))).forEach((s) -> {
                            result.add(s);
                        });

                        tableView.setItems(result);
                    }
                }
            }
        });
    }

    private void refreshTable() {
        refreshTable.setOnAction((ActionEvent event) -> {
            loadTable();
            searchcategoryCombo.setValue(null);
            warehouseComboBox.setValue(null);
            warehouseComboBox.setPromptText("Choose Warehouse");
        });
    }

}
