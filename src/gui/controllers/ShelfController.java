/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.Observable;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.value.ObservableListValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.Category;
import jpa.Sector;
import jpa.SectorJpaController;
import jpa.Shelf;
import jpa.ShelfDetails;
import jpa.ShelfDetailsJpaController;
import jpa.ShelfJpaController;
import jpa.Warehouse;
import jpa.WarehouseJpaController;

/**
 * FXML Controller class
 *
 * @author Gravity
 */
public class ShelfController implements Initializable {

    @FXML
    private Pane pane;
    @FXML
    private TabPane tabpane;
    @FXML
    private AnchorPane anch1;
    @FXML
    private Pane innerPane;
    @FXML
    private Button addButton;
    @FXML
    private Button cancelButton;
    @FXML
    private TextField nameField;
    @FXML
    private ComboBox<Sector> chooseSector;
    @FXML
    private ComboBox<Warehouse> chooseWarehouse;
    @FXML
    private Label categoryLabel;
    @FXML
    private TextArea descriptionField;
    @FXML
    private Label descriptionLabel;
    @FXML
    private CheckBox addExtraDetails;
    @FXML
    private AnchorPane anch2;
    @FXML
    private Pane searchPane;
    @FXML
    private ComboBox<Sector> searchBySector;
    @FXML
    private ComboBox<Warehouse> searchByWarehouse;
    @FXML
    private Label searchLabel;
    @FXML
    private ComboBox<String> searchShelfCombo;
    @FXML
    private TextField searchBox;
    @FXML
    private Button searchButton;
    @FXML
    private Pane editPane;
    @FXML
    private TextField editNameField;
    @FXML
    private TextField editDescriptionField;
    @FXML
    private Button editButton;
    @FXML
    private Button deleteButton;
    @FXML
    private Button refreshTable;
    @FXML
    private TableView<Shelf> tableView;
    @FXML
    private ContextMenu contectMenu;
    @FXML
    private MenuItem editContextMenu;
    @FXML
    private MenuItem deleteContextMenu;
    @FXML
    private MenuItem detailsContextMenu;
    @FXML
    private TableColumn<Shelf, String> nameColumn;
    @FXML
    private TableColumn<Shelf, String> descriptionColumn;
    private ShelfJpaController shelfJpaController;
    private WarehouseJpaController warehouseJpaController;
    private SectorJpaController sectorJpaController;
    private ShelfDetailsJpaController shelfDetailsJpaController;

    public static boolean isShelf;
    public static Shelf choosedShelf;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        transaction();
        initTable();
        loadTable();
        addEvent();
        addExtraDetails();
        loadWarehouses();
        loadSectors();
        tableListener();
        comboListeners();
        loadSearchCombo();
    }

    private void initTable() {
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        descriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
    }

    private void loadTable() {
        ObservableList<Shelf> shelfs = FXCollections.observableArrayList();
        for (Shelf shelf : shelfJpaController.findShelfEntities()) {
            if (shelf.getIsActive() == 1) {
                shelfs.add(shelf);
            }
        }
        tableView.setItems(shelfs);
    }

    private void transaction() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WarehouseV2PU");
        shelfJpaController = new ShelfJpaController(emf);
        warehouseJpaController = new WarehouseJpaController(emf);
        sectorJpaController = new SectorJpaController(emf);
        shelfDetailsJpaController = new ShelfDetailsJpaController(emf);
    }

    private boolean checkField() {
        Alert a = new Alert(Alert.AlertType.WARNING);
        if (chooseWarehouse.getValue() == null) {
            a.setTitle("Please Choose a Warehouse!");
            a.setContentText("Please choose a warehouse!");
            a.show();
            return false;
        }
        if (chooseSector.getValue() == null) {
            a.setTitle("Please Choose a Sectore");
            a.setContentText("Please Choose a Sectore");
            a.showAndWait();
        }
        if (nameField.getText().trim().isEmpty()) {
            a.setTitle("Please enter the name!");
            a.setContentText("Please enter the name!");
            a.showAndWait();
            return false;
        }
        return true;
    }

    public void addEvent() {
        addButton.setOnAction((ActionEvent e) -> {
            if (checkField()) {
                Shelf shelf = new Shelf();
                shelf.setName(nameField.getText().trim());
                if (!(descriptionField == null || descriptionField.getText().trim().isEmpty())) {
                    shelf.setDescription(descriptionField.getText().trim());
                }
                shelf.setIsActive(1);
                shelf.setSectorId(chooseSector.getValue());
                shelf.getSectorId().setWarehouseId(chooseWarehouse.getValue());
                shelf.setCreatedDate(Utility.nowDate());

                shelfJpaController.create(shelf);

                if (addExtraDetails.isSelected()) {
                    for (Map.Entry m : AddExtraDetailsController.details.entrySet()) {
                        ShelfDetails shelfDetails = new ShelfDetails();
                        shelfDetails.setName(m.getKey().toString());
                        shelfDetails.setValue(m.getValue().toString());
                        shelfDetails.setIsActive(1);
                        shelfDetails.setCreatedDate(Utility.nowDate());
                        shelfDetails.setShelfId(shelf);
                        shelfDetailsJpaController.create(shelfDetails);
                    }
                }

                boolean added = false;
                for (Shelf s : shelfJpaController.findShelfEntities()) {
                    if (s.getIsActive() == 1 && s.equals(shelf)) {
                        added = true;
                        break;
                    }
                }
                if (added) {
                    Alert a = new Alert(Alert.AlertType.INFORMATION);
                    a.setTitle("Added");
                    a.setContentText("Item added successfully");
                    a.showAndWait();
                    clearFields();
                    loadTable();
                }
            }
        });
    }

    private void addExtraDetails() {

        addExtraDetails.setOnAction((ActionEvent e) -> {
            try {
                Parent root = FXMLLoader.load(getClass().getResource("../fxml/AddExtraDetails.fxml"));
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.show();
            } catch (IOException ex) {
                Logger.getLogger(ShelfController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    private void loadWarehouses() {
        ObservableList<Warehouse> list = FXCollections.observableArrayList();
        warehouseJpaController.findWarehouseEntities().stream().filter((warehouse) -> (warehouse.getIsActive() == 1)).forEach((warehouse) -> {
            list.add(warehouse);
        });
        chooseWarehouse.setItems(list);
    }

    private void loadSectors() {
        chooseWarehouse.setOnAction(e -> {
            ObservableList<Sector> sectors = FXCollections.observableArrayList();
            Warehouse temp = chooseWarehouse.getValue();
            for (Sector sector : sectorJpaController.findSectorEntities()) {
                if (sector.getIsActive() == 1 && sector.getWarehouseId().equals(temp)) {
                    sectors.add(sector);
                }
            }
            chooseSector.setItems(sectors);
        });
    }

    private void tableListener() {
        tableView.setOnMousePressed(e -> {
            Shelf shelf = tableView.getSelectionModel().getSelectedItem();
            if (shelf == null) {
                editButton.setDisable(true);
                deleteButton.setDisable(true);
            }

            Timeline timeline = new Timeline();
            timeline.setAutoReverse(true);
            timeline.setCycleCount(2);
            timeline.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                    new KeyValue(editNameField.scaleXProperty(), 1.1),
                    new KeyValue(editDescriptionField.scaleXProperty(), 1.1)));
            timeline.play();
            editNameField.setText(shelf.getName());
            editDescriptionField.setText(shelf.getDescription());

            deleteButton.setOnAction(event -> {
                if (Utility.hasSomethingActive(shelf)) {
                    Alert al = new Alert(Alert.AlertType.ERROR);
                    al.setTitle("Error");
                    al.setContentText("To delete this shelf, make sure you have deleted all the content!");
                    al.showAndWait();
                } else {
                    shelf.setIsActive(0);
                    shelf.setUpdatedDate(Utility.nowDate());
                    try {
                        shelfJpaController.edit(shelf);
                        loadTable();
                        clearEditFields();
                        tableView.refresh();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });

            deleteContextMenu.setOnAction(ee -> {
                deleteButton.fire();
            });

            editButton.setOnAction(eve -> {
                shelf.setName(editNameField.getText().trim());
                shelf.setDescription(editDescriptionField.getText().trim());
                shelf.setUpdatedDate(Utility.nowDate());
                try {
                    shelfJpaController.edit(shelf);
                    loadTable();
                    clearEditFields();
                    tableView.refresh();
                } catch (Exception exe) {
                    exe.printStackTrace();
                }
            });

            detailsContextMenu.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    try {
                        isShelf = true;
                        choosedShelf = shelf;
                        Stage s = new Stage(StageStyle.DECORATED);
                        s.setScene(new Scene((Pane) FXMLLoader.load(getClass().getResource("../fxml/ViewDetails.fxml"))));
                        s.initModality(Modality.APPLICATION_MODAL);
                        s.setResizable(false);
                        s.sizeToScene();
                        s.setTitle("Warehouse Details");
                        s.show();
                    } catch (IOException ex) {
                        Logger.getLogger(WarehouseController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
        });

        anch2.setOnMousePressed(ev -> {
            editButton.setDisable(true);
            deleteButton.setDisable(true);
        });
    }

    private void comboListeners() {

        searchShelfCombo.setOnAction((ActionEvent clickEvent) -> {
            String selected = searchShelfCombo.getValue();

            if (selected.equals("Warehouse")) {
                searchByWarehouse.opacityProperty().set(0);
                Timeline timeline = new Timeline();
                timeline.getKeyFrames().add(new KeyFrame(Duration.millis(500),
                        new KeyValue(searchBox.scaleXProperty(), 0),
                        new KeyValue(searchByWarehouse.layoutXProperty(), 300),
                        new KeyValue(searchByWarehouse.opacityProperty(), 1.0)));
                timeline.play();
            }
            if (selected.equals("Sector")) {
                searchBySector.opacityProperty().set(0);
                Timeline timeline = new Timeline();
                timeline.getKeyFrames().add(new KeyFrame(Duration.millis(500),
                        new KeyValue(searchBox.scaleXProperty(), 0),
                        new KeyValue(searchBySector.layoutXProperty(), 300),
                        new KeyValue(searchBySector.opacityProperty(), 1.0)));
                timeline.play();
            }
            if (selected.equals("Name")) {
                searchBySector.opacityProperty().set(1);
                Timeline timeline = new Timeline();
                timeline.getKeyFrames().add(new KeyFrame(Duration.millis(500),
                        new KeyValue(searchBox.scaleXProperty(), 1.0),
                        new KeyValue(searchBySector.layoutXProperty(), 98),
                        new KeyValue(searchBySector.opacityProperty(), 0.0),
                        new KeyValue(searchByWarehouse.layoutXProperty(), 98),
                        new KeyValue(searchByWarehouse.opacityProperty(), 0.0)));
                timeline.play();
            }
        });

        refreshTable.setOnAction((ActionEvent event) -> {
            loadTable();
            searchBox.clear();
            searchShelfCombo.setValue(null);
            searchByWarehouse.setValue(null);
            searchBySector.setValue(null);
        });
    }

    private void loadSearchCombo() {
        searchShelfCombo.setItems(FXCollections.observableArrayList("Warehouse", "Sector", "Name"));

        searchButton.setOnAction((ActionEvent event) -> {
            ObservableList<Shelf> list = FXCollections.observableArrayList();
            String selcted = searchShelfCombo.getValue();

            if (selcted == null) {
                Timeline timeline = new Timeline();
                timeline.setAutoReverse(true);
                timeline.setCycleCount(4);
                timeline.getKeyFrames().add(new KeyFrame(Duration.millis(200), (ActionEvent event1) -> {
                    searchLabel.setText("required*");
                    searchLabel.textFillProperty().set(Color.RED);
                    searchShelfCombo.setStyle("-fx-border-style: solid; -fx-border-width: 1px; -fx-border-color: red; -fx-border-radius: 3px;");
                },
                        new KeyValue(searchShelfCombo.layoutXProperty(), 101),
                        new KeyValue(searchShelfCombo.layoutXProperty(), 96)));
                timeline.setOnFinished((ActionEvent event1) -> {
                    searchLabel.setText("Search by*");
                    searchLabel.textFillProperty().set(Color.BLACK);
                    searchShelfCombo.setStyle("-fx-border-style: none; ");
                });
                timeline.play();
            } else {
                if (selcted.equals("Warehouse")) {

                    if (searchByWarehouse.getValue() == null) {
                        Timeline timeline = new Timeline();
                        timeline.setAutoReverse(true);
                        timeline.setCycleCount(4);
                        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(200), (ActionEvent event1) -> {
                            searchByWarehouse.setStyle("-fx-border-style: solid; -fx-border-width:1px; -fx-border-color:red; -fx-border-radius:3px;");
                        },
                                new KeyValue(searchByWarehouse.layoutXProperty(), 297),
                                new KeyValue(searchByWarehouse.layoutXProperty(), 302)));
                        timeline.setOnFinished((ActionEvent event1) -> {
                            searchByWarehouse.setStyle("-fx-border-style: none; ");
                        });
                        timeline.play();
                    }
                    list.removeAll(list);
                    shelfJpaController.findShelfEntities().stream().filter((sh) -> (sh.getIsActive() == 1 & sh.getSectorId().getWarehouseId().equals(searchByWarehouse.getSelectionModel().getSelectedItem()))).forEach((sh) -> {
                        list.add(sh);
                    });
                    tableView.setItems(list);
                }
                if (selcted.equals("Sector")) {

                    if (searchBySector.getSelectionModel().getSelectedItem() == null) {
                        Timeline timeline = new Timeline();
                        timeline.setAutoReverse(true);
                        timeline.setCycleCount(4);
                        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(200), (ActionEvent event1) -> {
                            searchBySector.setStyle("-fx-border-style: solid; -fx-border-width:1px; -fx-border-color:red; -fx-border-radius:3px;");
                        },
                                new KeyValue(searchBySector.layoutXProperty(), 297),
                                new KeyValue(searchBySector.layoutXProperty(), 302)));
                        timeline.setOnFinished((ActionEvent event1) -> {
                            searchBySector.setStyle("-fx-border-style: none; ");
                        });
                        timeline.play();
                    }
                    list.removeAll(list);
                    shelfJpaController.findShelfEntities().stream().filter((sh) -> (sh.getIsActive() == 1 & sh.getSectorId().equals(searchBySector.getSelectionModel().getSelectedItem()))).forEach((sh) -> {
                        list.add(sh);
                    });
                    tableView.setItems(list);
                }
                if (selcted.equals("Name")) {
                    if (searchBox.getText() == null || searchBox.getText().trim().isEmpty()) {
                        loadTable();
                    } else {
                        list.removeAll(list);
                        shelfJpaController.findShelfEntities().stream().filter((shelf) -> (shelf.getIsActive() == 1 && shelf.getName().toLowerCase().contains(searchBox.getText().trim().toLowerCase()))).forEach((shelf) -> {
                            list.add(shelf);
                        });
                        tableView.setItems(list);
                    }
                }
            }
        });
    }

    private boolean checkforEdit() {
        if (editNameField.getText() == null || editNameField.getText().trim().isEmpty()) {
            Alert al = new Alert(Alert.AlertType.WARNING);
            al.setContentText("Please enter the name");
            al.setTitle("Warning");
            al.showAndWait();
            return false;
        } else {
            return true;
        }
    }

    private void clearFields() {
        chooseSector.setValue(null);
        chooseWarehouse.setValue(null);
        nameField.clear();
        descriptionField.clear();
        addExtraDetails.setSelected(false);
    }

    private void clearEditFields() {
        editNameField.clear();
        editDescriptionField.clear();
    }
}
