/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import jpa.Category;
import jpa.Product;
import jpa.Stock;
import jpa.Unit;

/**
 *
 * @author Deathrow
 */
public class StockTableViewModel {

    private Stock stock;
    private Product product;

    private String productName;
    private int quantity;
    private Category category;
    private Unit unit;
    private double sellPrice;
    private double totalCost;

    public StockTableViewModel(Stock stock) {
        this.stock = stock;
        this.product = stock.getProductId();
        this.productName = this.product.getName();
        this.unit = stock.getUnitId();
        this.category = this.product.getCategoryId();
        this.sellPrice = this.stock.getSellPrice();
        this.quantity = this.stock.getQuantity();
        this.totalCost = this.quantity * this.sellPrice;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public double getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(double sellPrice) {
        this.sellPrice = sellPrice;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

}
