/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.Location;
import jpa.LocationJpaController;
import jpa.exceptions.NonexistentEntityException;

/**
 * FXML Controller class
 *
 * @author defri
 */
public class LocationController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Pane outterPane;

    @FXML
    private Tab addLocationTab;

    @FXML
    private TabPane tabpane;

    @FXML
    private AnchorPane anch1;

    @FXML
    private Pane innerPane;

    @FXML
    private TextField countryField;

    @FXML
    private Button addButton;

    @FXML
    private Button cancelButton;

    @FXML
    private TextField cityField;

    @FXML
    private TextField streetField;

    @FXML
    private Tab manageLocationsTab;

    @FXML
    private AnchorPane anch2;

    @FXML
    private TableView<Location> tableview;

    @FXML
    private TableColumn<Location, String> countryCol;

    @FXML
    private TableColumn<Location, String> cityCol;

    @FXML
    private TableColumn<Location, String> streetCol;

    @FXML
    private ContextMenu contextMenu;

    @FXML
    private MenuItem deleteMenuItem;

    @FXML
    private Pane searchPane;

    @FXML
    private ComboBox<String> searchcategoryCombo;

    @FXML
    private TextField searchbox;

    @FXML
    private Button searchButton;

    @FXML
    private Pane editPane;

    @FXML
    private TextField editCountryField;

    @FXML
    private TextField editCityField;

    @FXML
    private TextField editStreetField;

    @FXML
    private Button editButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Label searchLABEL;

    @FXML
    private ComboBox<String> chooseCity;

    @FXML
    private Label streetInfo;

    @FXML
    private ComboBox<String> chooseCountry;

    @FXML
    private Label cityLabelInfo;

    @FXML
    private Label countryLabelInfo;

    @FXML
    private Button refreshTable;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

        addAction();
        initTable();
        loadTable();
        tableListener();
        loadCombos();
        initCities();

        loadSearchCombo();
        Utility.validateControl(countryField);
        Utility.validateControl(cityField);
        Utility.validateControl(streetField);
        Utility.validateTextField(countryField, false);
        Utility.validateTextField(cityField, false);
        Utility.validateTextField(streetField, false);

    }

    private void loadCombos() {
        ObservableList<String> countries = FXCollections.observableArrayList();

        for (Location location : Utility.LOCATIONJPACONTROLLER.findLocationEntities()) {
            if (location.getIsActive() == 1) {
                if (!countries.contains(location.getCountry())) {
                    countries.add(location.getCountry());
                }
            }
        }

        chooseCountry.setItems(countries);

    }

    private void initCities() {
        chooseCity.setOnMousePressed((MouseEvent event) -> {
            chooseCity.setItems(getCities(chooseCountry.getSelectionModel().getSelectedItem()));
        });

        chooseCountry.setOnAction((ActionEvent event) -> {
            countryField.setText(chooseCountry.getSelectionModel().getSelectedItem());
        });

        chooseCity.setOnAction((ActionEvent event) -> {
            cityField.setText(chooseCity.getSelectionModel().getSelectedItem());
        });
    }

    public ObservableList<String> getCities(String country) {
        ObservableList<String> items = FXCollections.observableArrayList();
        Utility.LOCATIONJPACONTROLLER.findLocationEntities().stream().forEach((l) -> {
            if (l.getCountry().equals(country) && country != null) {
                if (l.getIsActive() == 1) {
                    if (!items.contains(l.getCity())) {
                        items.add(l.getCity());
                    }
                }
            }

        });
        return items;
    }

    public boolean check() {
        if (countryField.getText().trim().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Contry field empty!");
            alert.showAndWait();
            return false;
        } else if (countryField.getText().trim().length() < 3) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Country should be longer than 3 characters!");
            alert.showAndWait();
            return false;
        } else if (cityField.getText().trim().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("City field empty!");
            alert.showAndWait();
            return false;
        } else if (cityField.getText().trim().length() < 3) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("City should be longer than 3 characters!");
            alert.showAndWait();
            return false;
        } else {
            return true;
        }
    }

    private void addAction() {
        addButton.setOnAction((ActionEvent event) -> {
            if (check()) {
                try {
                    Location li = new Location();
                    li.setCountry(countryField.getText());
                    li.setCity(cityField.getText());
                    li.setCreatedDate(Utility.nowDate());
                    li.setIsActive(1);
                    if (streetField.getText() != null || !streetField.getText().trim().isEmpty()) {
                        li.setStreet(streetField.getText());
                    }
                    Utility.LOCATIONJPACONTROLLER.create(li);
                    clear();
                    loadTable();
                    loadCombos();

                    cityField.setStyle("-fx-border-style:none;");
                    streetField.setStyle("-fx-border-style:none;");
                    countryField.clear();
                    countryField.setStyle("-fx-border-style:none;");
                } catch (Exception ex) {
                    Logger.getLogger(LocationController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    private void clear() {
        countryField.clear();
        chooseCountry.setValue("Choose country");
        chooseCity.setValue("Choose city");

        cityField.clear();
        streetField.clear();
    }

    private void clearEditFields() {
        editCountryField.clear();
        editCityField.clear();
        editStreetField.clear();
    }

    private void initTable() {
        countryCol.setCellValueFactory(new PropertyValueFactory<>("country"));
        cityCol.setCellValueFactory(new PropertyValueFactory<>("city"));
        streetCol.setCellValueFactory(new PropertyValueFactory<>("street"));
    }

    private void loadTable() {
        ObservableList<Location> list = FXCollections.observableArrayList();
        for (Location location : Utility.LOCATIONJPACONTROLLER.findLocationEntities()) {
            if (location.getIsActive() == 1) {
                list.add(location);
            }
        }
        tableview.setItems(list);
    }

    private void tableListener() {

        anch2.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                clearEditFields();
                deleteButton.setDisable(true);
                editButton.setDisable(true);

            }
        });

        tableview.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                Location location = tableview.getSelectionModel().getSelectedItem();

                if (location != null) {
                    deleteButton.setDisable(false);
                    editButton.setDisable(false);
                }

                //fill Fields
                editCountryField.setText(location.getCountry());
                editCityField.setText(location.getCity());
                editStreetField.setText(location.getStreet());

                Timeline timeline = new Timeline();
                timeline.setAutoReverse(true);
                timeline.setCycleCount(2);
                timeline.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(editCountryField.scaleXProperty(), 1.1),
                        new KeyValue(editCityField.scaleXProperty(), 1.1),
                        new KeyValue(editStreetField.scaleXProperty(), 1.1)));
                timeline.play();

                Timeline zoomout = new Timeline();
                zoomout.setAutoReverse(true);
                zoomout.setCycleCount(2);
                zoomout.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(editCountryField.scaleXProperty(), 0.2),
                        new KeyValue(editCountryField.scaleYProperty(), 0.2),
                        new KeyValue(editCityField.scaleXProperty(), 0.2),
                        new KeyValue(editCityField.scaleYProperty(), 0.2),
                        new KeyValue(editStreetField.scaleXProperty(), 0.2),
                        new KeyValue(editStreetField.scaleYProperty(), 0.2)));

                editButton.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        location.setCountry(editCountryField.getText());
                        location.setCity(editCityField.getText());
                        location.setStreet(editStreetField.getText());
                        location.setUpdatedDate(Utility.nowDate().toString());
                        try {
                            Utility.LOCATIONJPACONTROLLER.edit(location);
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(LocationController.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (Exception ex) {
                            Logger.getLogger(LocationController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        zoomout.play();

                        loadTable();
                        tableview.refresh();

                    }
                });

                deleteButton.setOnAction((ActionEvent event1) -> {
                    if (Utility.hasSomethingActive(location)) {
                        Alert al = new Alert(Alert.AlertType.ERROR);
                        al.setTitle("Error");
                        al.setContentText("To delete this locations, make sure you have deleted all the content!");
                        al.showAndWait();
                    } else {
                        location.setIsActive(0);
                        location.setUpdatedDate(Utility.nowDate().toString());
                        try {
                            Utility.LOCATIONJPACONTROLLER.edit(location);
                            zoomout.play();
                            loadTable();
                            tableview.refresh();
                            clearEditFields();
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(LocationController.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (Exception ex) {
                            Logger.getLogger(LocationController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });

                deleteMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if (Utility.hasSomethingActive(location)) {
                            Alert al = new Alert(Alert.AlertType.ERROR);
                            al.setTitle("Error");
                            al.setContentText("To delete this locations, make sure you have deleted all the content!");
                            al.showAndWait();
                        } else {
                            location.setIsActive(0);
                            location.setUpdatedDate(Utility.nowDate().toString());
                            try {
                                Utility.LOCATIONJPACONTROLLER.edit(location);
                                zoomout.play();
                                loadTable();
                                tableview.refresh();
                                clearEditFields();
                            } catch (NonexistentEntityException ex) {
                                Logger.getLogger(LocationController.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (Exception ex) {
                                Logger.getLogger(LocationController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                    }
                });
            }
        }
        );

    }

    private void loadSearchCombo() {
        ObservableList<String> list = FXCollections.observableArrayList("Country", "City", "Street");
        searchcategoryCombo.setItems(list);

        searchButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String str = searchcategoryCombo.getSelectionModel().getSelectedItem();
                if (str == null) {
                    Timeline timeline = new Timeline();
                    timeline.setAutoReverse(true);
                    timeline.setCycleCount(4);
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(200), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            searchLABEL.setText("required*");
                            searchLABEL.textFillProperty().set(Color.RED);
                            searchcategoryCombo.setStyle("-fx-border-style: solid; -fx-border-width:1px; -fx-border-color:red; -fx-border-radius:3px;");
                        }
                    },
                            new KeyValue(searchcategoryCombo.layoutXProperty(), 101),
                            new KeyValue(searchcategoryCombo.layoutXProperty(), 96)));
                    timeline.setOnFinished(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            searchLABEL.setText("Search by*");
                            searchLABEL.textFillProperty().set(Color.BLACK);
                            searchcategoryCombo.setStyle("-fx-border-style: none; ");
                        }
                    });
                    timeline.play();

                } else {

                    ObservableList<Location> result = FXCollections.observableArrayList();

                    if (str.trim().equals("Country")) {
                        for (Location loc : Utility.LOCATIONJPACONTROLLER.findLocationEntities()) {
                            if ((loc.getCountry().toLowerCase()).contains(searchbox.getText().toLowerCase())) {
                                if (loc.getIsActive() == 1) {
                                    result.add(loc);
                                }

                            }
                        }

                    }
                    if (str.trim().equals("City")) {
                        for (Location loc : Utility.LOCATIONJPACONTROLLER.findLocationEntities()) {
                            if (loc.getCity().toLowerCase().contains(searchbox.getText().toLowerCase())) {
                                if (loc.getIsActive() == 1) {
                                    result.add(loc);
                                }
                            }
                        }

                    }
                    if (str.trim().equals("Street")) {
                        for (Location loc : Utility.LOCATIONJPACONTROLLER.findLocationEntities()) {
                            if (loc.getStreet().toLowerCase().contains(searchbox.getText().toLowerCase())) {
                                if (loc.getIsActive() == 1) {
                                    result.add(loc);
                                }
                            }
                        }

                    }

                    tableview.setItems(result);

                }

            }
        });

    }
}
