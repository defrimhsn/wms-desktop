/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.Groups;
import jpa.GroupsJpaController;
import jpa.User;
import jpa.UserJpaController;
import jpa.exceptions.NonexistentEntityException;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

/**
 * FXML Controller class
 *
 * @author defri
 */
public class UserController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    public static Pane pane;

    @FXML
    private TabPane tabpane;

    @FXML
    private TextField addNameField;

    @FXML
    private TextField addSurnameField;

    @FXML
    private RadioButton addradioMButton;

    @FXML
    private PasswordField passwordField;

    @FXML
    private ImageView imgview;

    @FXML
    private PasswordField passwordField2;

    @FXML
    private ToggleGroup gender1;

    @FXML
    private RadioButton addradioFButton;

    @FXML
    private TextField addUsernameField;

    @FXML
    private ComboBox<Groups> addgroupComboBox;

    @FXML
    private Button cancelbutton;

    @FXML
    private Button addbutton;

    @FXML
    private Tab newRegistrationTab;

    @FXML
    private Tab manageTab;

    @FXML
    private Label infoText;

    @FXML
    private TableView<User> tableview;

    @FXML
    private TableColumn<User, String> nameCol;

    @FXML
    private TableColumn<User, String> surnameCol;

    @FXML
    private TableColumn<User, String> genderCol;

    @FXML
    private TableColumn<User, String> usernameCol;

    @FXML
    private TableColumn<User, Groups> groupCol;

    @FXML
    private MenuItem deleteMenuItem;

    @FXML
    private Button deleteButton;

    @FXML
    private Pane searchPane;

    @FXML
    private ComboBox<String> searchcategoryCombo;

    @FXML
    private Button searchButton;

    @FXML
    private Pane editPane;

    @FXML
    private TextField editNameField;

    @FXML
    private TextField editSurnameField;

    @FXML
    private RadioButton radioMButton;

    @FXML
    private ToggleGroup gender;

    @FXML
    private Button editButton;

    @FXML
    private RadioButton radioFButton;

    @FXML
    private TextField editUsernameField;

    @FXML
    private Button refreshTable;

    @FXML
    private TextField searchbox;

    @FXML
    private Label searchLABEL;

    @FXML
    private ImageView createPdf;

    @FXML
    private ImageView createExcel;

    @FXML
    private ImageView createHTML;

    @FXML
    private ComboBox<Groups> groupComboBox;

    private UserJpaController userJpaController;
    private GroupsJpaController groupsJpaController;

    private User selectedUser;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // TODO
        transaction();
        initTable();
        loadTable();
        initSearchCategoryCombo();
        chooseGroup();
        createTransitions();
        addChooseGroup();
        tableListener();
        addUser();
        editUser();
        deleteUser();
        refreshTable();
        search();
        Utility.validateTextField(addNameField, false);
        Utility.validateTextField(addSurnameField, false);
        Utility.validateTextField(editNameField, false);
        Utility.validateTextField(editSurnameField, false);

    }

    private void transaction() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WarehouseV2PU");
        userJpaController = new UserJpaController(emf);
        groupsJpaController = new GroupsJpaController(emf);
    }

    private void initTable() {
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        surnameCol.setCellValueFactory(new PropertyValueFactory<>("surname"));
        genderCol.setCellValueFactory(new PropertyValueFactory<>("gender"));
        usernameCol.setCellValueFactory(new PropertyValueFactory<>("username"));
        groupCol.setCellValueFactory(new PropertyValueFactory<>("groupId"));

        Utility.validateControl(addNameField);
        Utility.validateControl(addSurnameField);
        Utility.validateControl(addUsernameField);
        Utility.validateControl(editNameField);
        Utility.validateControl(editSurnameField);
        Utility.validateControl(editUsernameField);

    }

    private void loadTable() {
        ObservableList<User> list = FXCollections.observableArrayList();
        for (User us : userJpaController.findUserEntities()) {
            if (us.getIsActive() == 1) {
                list.add(us);
            }
        }
        tableview.setItems(list);
    }

    private void initSearchCategoryCombo() {
        ObservableList<String> list = FXCollections.observableArrayList();
        list.addAll("Name", "Surname", "Gender", "Username", "Group");
        searchcategoryCombo.setItems(list);
    }

    private void chooseGroup() {
        ObservableList<Groups> list = FXCollections.observableArrayList();
        groupsJpaController.findGroupsEntities().stream().filter((groups) -> (groups.getIsActive() == 1)).forEach((groups) -> {
            list.add(groups);
        });
        groupComboBox.setItems(list);
    }

    private void addChooseGroup() {
        ObservableList<Groups> list = FXCollections.observableArrayList();
        groupsJpaController.findGroupsEntities().stream().filter((groups) -> (groups.getIsActive() == 1)).forEach((groups) -> {
            list.add(groups);
        });
        addgroupComboBox.setItems(list);
    }

    private void createTransitions() {
        Timeline t = new Timeline();
        t.setCycleCount(Timeline.INDEFINITE);
        t.setAutoReverse(true);
        t.getKeyFrames().add(new KeyFrame(Duration.seconds(10), new KeyValue(imgview.opacityProperty(), 0.5), new KeyValue(imgview.scaleXProperty(), 1.1), new KeyValue(imgview.scaleYProperty(), 1.1)));
        t.play();
    }

    private void tableListener() {
        tableview.setOnMousePressed((MouseEvent event) -> {
            selectedUser = tableview.getSelectionModel().getSelectedItem();
            Timeline timeline = new Timeline();
            timeline.setAutoReverse(true);
            timeline.setCycleCount(2);
            timeline.getKeyFrames().add(new KeyFrame(Duration.millis(200),
                    new KeyValue(editNameField.scaleXProperty(), 1.1),
                    new KeyValue(editSurnameField.scaleXProperty(), 1.1),
                    new KeyValue(editUsernameField.scaleXProperty(), 1.1),
                    new KeyValue(groupComboBox.scaleXProperty(), 1.1)));
            timeline.play();
            fillFields(selectedUser);
        });
    }

    private void fillFields(User user) {
        if (user != null) {
            editNameField.setText(selectedUser.getName());
            editSurnameField.setText(selectedUser.getSurname());
            editUsernameField.setText(selectedUser.getUsername());
            groupComboBox.setValue(selectedUser.getGroupId());
            if (selectedUser.getGender().charAt(0) == 'M') {
                gender.selectToggle(radioMButton);
            } else {
                gender.selectToggle(radioFButton);
            }
        }
    }

    private void addUser() {
        addbutton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (check()) {
                    User user = new User();
                    user.setName(addNameField.getText());
                    user.setSurname(addSurnameField.getText());
                    user.setUsername(addUsernameField.getText());
                    RadioButton r = (RadioButton) addradioMButton.getToggleGroup().getSelectedToggle();
                    user.setGender(r.getText());
                    user.setCreatedDate(Utility.nowDate());
                    user.setIsActive(1);
                    user.setGroupId(addgroupComboBox.getSelectionModel().getSelectedItem());
                    user.setPassword(Utility.MD5(passwordField.getText()));
                    userJpaController.create(user);
                    Alert al = new Alert(Alert.AlertType.INFORMATION);
                    al.setTitle("Congratulations!");
                    al.setContentText("User added successfully!");
                    al.show();
                    clearFields();
                    loadTable();

                }
            }
        });
    }

    private void editUser() {
        editButton.setOnAction((ActionEvent event) -> {
            if (checkEditFields()) {
                if (selectedUser == null) {

                } else {
                    selectedUser.setName(editNameField.getText());
                    selectedUser.setSurname(editSurnameField.getText());
                    selectedUser.setUsername(editUsernameField.getText());
                    selectedUser.setGroupId(groupComboBox.getSelectionModel().getSelectedItem());
                    selectedUser.setUpdatedDate(Utility.nowDate());
                    RadioButton r = (RadioButton) radioMButton.getToggleGroup().getSelectedToggle();
                    selectedUser.setGender(r.getText());
                    try {
                        userJpaController.edit(selectedUser);
                    } catch (NonexistentEntityException ex) {
                        Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    clearFields();
                    loadTable();
                    tableview.refresh();
                }
            }
        });
    }

    private void deleteUser() {
        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (selectedUser == null) {

                } else {
                    selectedUser.setIsActive(0);
                    selectedUser.setUpdatedDate(Utility.nowDate());
                    try {
                        userJpaController.edit(selectedUser);
                    } catch (NonexistentEntityException ex) {
                        Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    clearFields();
                    loadTable();
                }

            }
        });

        deleteMenuItem.setOnAction((ActionEvent event) -> {
            if (selectedUser == null) {

            } else {
                selectedUser.setIsActive(0);
                selectedUser.setUpdatedDate(Utility.nowDate());
                try {
                    userJpaController.edit(selectedUser);
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                }
                clearFields();
                loadTable();
            }
        });
    }

    private boolean validatePassword() {
        return passwordField.getText().equals(passwordField2.getText());
    }

    private void refreshTable() {
        refreshTable.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                loadTable();
                tableview.refresh();
            }
        }
        );
    }

    private void search() {
        searchbox.setOnMousePressed(e -> {
            searchButton.setDefaultButton(true);
            searchButton.setFocusTraversable(true);
        });
        searchButton.setOnAction((ActionEvent event) -> {
            String str = searchcategoryCombo.getSelectionModel().getSelectedItem();
            ObservableList<User> namelist = FXCollections.observableArrayList();
            if (str == null) {
                Timeline timeline = new Timeline();
                timeline.setAutoReverse(true);
                timeline.setCycleCount(4);
                timeline.getKeyFrames().add(new KeyFrame(Duration.millis(200), (ActionEvent event1) -> {
                    searchLABEL.setText("required*");
                    searchLABEL.textFillProperty().set(Color.RED);
                    searchcategoryCombo.setStyle("-fx-border-style: solid; -fx-border-width:1px; -fx-border-color:red; -fx-border-radius:3px;");
                },
                        new KeyValue(searchcategoryCombo.layoutXProperty(), 101),
                        new KeyValue(searchcategoryCombo.layoutXProperty(), 96)));
                timeline.setOnFinished((ActionEvent event1) -> {
                    searchLABEL.setText("Search by*");
                    searchLABEL.textFillProperty().set(Color.BLACK);
                    searchcategoryCombo.setStyle("-fx-border-style: none; ");
                });
                timeline.play();
            } else {
                if (str.equals("Name")) {
                    userJpaController.findUserEntities().stream().filter((user) -> (user.getIsActive() == 1)).filter((user) -> (user.getName().toLowerCase().contains(searchbox.getText().toLowerCase()))).forEach((user) -> {
                        namelist.add(user);
                    });
                    tableview.setItems(namelist);
                }
                if (str.equals("Surname")) {
                    namelist.removeAll(namelist);
                    userJpaController.findUserEntities().stream().filter((user) -> (user.getIsActive() == 1)).filter((user) -> (user.getSurname().toLowerCase().contains(searchbox.getText().toLowerCase()))).forEach((user) -> {
                        namelist.add(user);
                    });
                    tableview.setItems(namelist);
                }
                if (str.equals("Gender")) {
                    namelist.removeAll(namelist);
                    userJpaController.findUserEntities().stream().filter((user) -> (user.getIsActive() == 1)).filter((user) -> (user.getGender().trim().toLowerCase().equals(searchbox.getText().trim().toLowerCase()))).forEach((user) -> {
                        namelist.add(user);
                    });
                    tableview.setItems(namelist);
                }

                if (str.equals("Username")) {
                    namelist.removeAll(namelist);
                    userJpaController.findUserEntities().stream().filter((user) -> (user.getIsActive() == 1)).filter((user) -> (user.getUsername().toLowerCase().contains(searchbox.getText().trim().toLowerCase()))).forEach((user) -> {
                        namelist.add(user);
                    });
                    tableview.setItems(namelist);
                }

                if (str.equals("Group")) {
                    namelist.removeAll(namelist);
                    userJpaController.findUserEntities().stream().filter((user) -> (user.getIsActive() == 1)).filter((user) -> (user.getGroupId().getName().toLowerCase().contains(searchbox.getText().trim().toLowerCase()))).forEach((user) -> {
                        namelist.add(user);
                    });
                    tableview.setItems(namelist);
                }
            }

        });

    }

    private void clearFields() {
        editNameField.clear();
        editNameField.setStyle("-fx-border-style:none;");
        editSurnameField.clear();
        editSurnameField.setStyle("-fx-border-style:none;");
        editUsernameField.clear();
        editUsernameField.setStyle("-fx-border-style:none;");
        groupComboBox.setPromptText("Choose Group!");
        radioFButton.setSelected(false);
        radioMButton.setSelected(false);

        addNameField.clear();
        addNameField.setStyle("-fx-border-style:none;");
        addSurnameField.clear();
        addSurnameField.setStyle("-fx-border-style:none;");
        addUsernameField.clear();
        addUsernameField.setStyle("-fx-border-style:none;");
        addradioFButton.setSelected(false);
        addradioMButton.setSelected(false);
        addgroupComboBox.setPromptText("Choose Group!");
        passwordField.setText("");
        passwordField2.setText("");
    }

    private boolean check() {
        if (addNameField.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Please fill the name field!");
            alert.showAndWait();
            return false;
        } else if (addNameField.getText().trim().length() < 3) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("name should be longer than 3 characters!");
            alert.showAndWait();
            return false;
        } else if (addSurnameField.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Please fill the surname field!");
            alert.showAndWait();
            return false;
        } else if (addSurnameField.getText().trim().length() < 3) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("surname should be longer than 3 characters!");
            alert.showAndWait();
            return false;
        } else if ((RadioButton) addradioMButton.getToggleGroup().getSelectedToggle() == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Please choose the gender");
            alert.showAndWait();
            return false;
        } else if (addUsernameField.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Please fill the username field!");
            alert.showAndWait();
            return false;
        } else if (addUsernameField.getText().trim().length() < 3) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Username should be longer than 3 characters!");
            alert.showAndWait();
            return false;
        } else if (addgroupComboBox.getSelectionModel().getSelectedItem() == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Please choose a group!");
            alert.showAndWait();
            return false;
        } else if (passwordField.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Please type a password!");
            alert.showAndWait();
            return false;
        } else if (!validatePassword()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("The passwords do not match!");
            alert.showAndWait();
            return false;
        } else if (existUsername(addUsernameField.getText())) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("This username exists already!");
            alert.showAndWait();
            return false;
        } else {
            return true;
        }
    }

    private boolean existUsername(String username) {
        boolean exists = false;
        for (User user : userJpaController.findUserEntities()) {
            exists = user.getUsername().equals(username);
        }
        return exists;
    }

    private boolean checkEditFields() {
        if (editNameField.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Please fill the name field!");
            alert.showAndWait();
            return false;
        } else if (editNameField.getText().trim().length() < 3) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Name should be longer than 3 characters!");
            alert.showAndWait();
            return false;
        } else if (editSurnameField.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Please fill the surname field!");
            alert.showAndWait();
            return false;
        } else if (editSurnameField.getText().trim().length() < 3) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("surname should be longer than 3 characters!");
            alert.showAndWait();
            return false;
        } else if ((RadioButton) radioMButton.getToggleGroup().getSelectedToggle() == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Please choose the gender");
            alert.showAndWait();
            return false;
        } else if (editUsernameField.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Please fill the username field!");
            alert.showAndWait();
            return false;
        } else if (editUsernameField.getText().trim().length() < 3) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Username should be longer than 3 characters!");
            alert.showAndWait();
            return false;
        } else if (groupComboBox.getSelectionModel().getSelectedItem() == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("Please choose a group!");
            alert.showAndWait();
            return false;
        } else if (existUsername(editUsernameField.getText())) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setContentText("This username exists already!");
            alert.showAndWait();
            return false;
        } else {
            return true;
        }
    }

}
