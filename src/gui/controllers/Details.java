/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import jpa.CategoryDetails;
import jpa.ProductDetails;
import jpa.SectorDetails;
import jpa.ShelfDetails;
import jpa.WarehouseDetails;

/**
 *
 * @author Deathrow
 */
public class Details {

    private WarehouseDetails warehouseDetails;
    private String warehouseDetailsName;
    private String warehouseDetailsValue;

    private SectorDetails sectorDetails;
    private String sectorDetailsName;
    private String sectorDetailsValue;

    private ShelfDetails shelfDetails;
    private String shelfDetailsName;
    private String shelfDetailsValue;

    private CategoryDetails categoryDetails;
    private String categoryDetailsName;
    private String categoryDetailsValue;

    private ProductDetails productDetails;
    private String productDetailsName;
    private String productDetailsValue;

    private StringProperty name;
    private StringProperty value;

    public Details(WarehouseDetails warehouseDetails) {
        this.warehouseDetails = warehouseDetails;
        this.warehouseDetailsName = warehouseDetails.getName();
        this.warehouseDetailsValue = warehouseDetails.getValue();
        name = new SimpleStringProperty();
        name.set(warehouseDetails.getName());
        value = new SimpleStringProperty();
        value.set(warehouseDetails.getValue());
    }

    public Details(SectorDetails sectorDetails) {
        this.sectorDetails = sectorDetails;
        this.sectorDetailsName = sectorDetails.getName();
        this.sectorDetailsValue = sectorDetails.getValue();
        name = new SimpleStringProperty();
        name.set(sectorDetails.getName());
        value = new SimpleStringProperty();
        value.set(sectorDetails.getValue());
    }
//

    public Details(ShelfDetails shelfDetails) {
        this.shelfDetails = shelfDetails;
        this.shelfDetailsName = shelfDetails.getName();
        this.shelfDetailsValue = shelfDetails.getValue();
        name = new SimpleStringProperty();
        name.set(shelfDetails.getName());
        value = new SimpleStringProperty();
        value.set(shelfDetails.getValue());

    }
//

    public Details(CategoryDetails categoryDetails) {
        this.categoryDetails = categoryDetails;
        this.categoryDetailsName = categoryDetails.getName();
        this.categoryDetailsValue = categoryDetails.getValue();
        name = new SimpleStringProperty();
        name.set(categoryDetails.getName());
        value = new SimpleStringProperty();
        value.set(categoryDetails.getValue());

    }
//

    public Details(ProductDetails productDetails) {
        this.productDetails = productDetails;
        this.productDetailsName = productDetails.getName();
        this.productDetailsValue = productDetails.getValue();
        name = new SimpleStringProperty();
        name.set(productDetails.getName());
        value = new SimpleStringProperty();
        value.set(productDetails.getValue());

    }

    public WarehouseDetails getWarehouseDetails() {
        return warehouseDetails;
    }

    public void setWarehouseDetails(WarehouseDetails warehouseDetails) {
        this.warehouseDetails = warehouseDetails;
    }

    public String getWarehouseDetailsName() {
        return warehouseDetailsName;
    }

    public void setWarehouseDetailsName(String warehouseDetailsName) {
        this.warehouseDetailsName = warehouseDetailsName;
    }

    public String getWarehouseDetailsValue() {
        return warehouseDetailsValue;
    }

    public void setWarehouseDetailsValue(String warehouseDetailsValue) {
        this.warehouseDetailsValue = warehouseDetailsValue;
    }

    public SectorDetails getSectorDetails() {
        return sectorDetails;
    }

    public void setSectorDetails(SectorDetails sectorDetails) {
        this.sectorDetails = sectorDetails;
    }

    public String getSectorDetailsName() {
        return sectorDetailsName;
    }

    public void setSectorDetailsName(String sectorDetailsName) {
        this.sectorDetailsName = sectorDetailsName;
    }

    public String getSectorDetailsValue() {
        return sectorDetailsValue;
    }

    public void setSectorDetailsValue(String sectorDetailsValue) {
        this.sectorDetailsValue = sectorDetailsValue;
    }

    public ShelfDetails getShelfDetails() {
        return shelfDetails;
    }

    public void setShelfDetails(ShelfDetails shelfDetails) {
        this.shelfDetails = shelfDetails;
    }

    public String getShelfDetailsName() {
        return shelfDetailsName;
    }

    public void setShelfDetailsName(String shelfDetailsName) {
        this.shelfDetailsName = shelfDetailsName;
    }

    public String getShelfDetailsValue() {
        return shelfDetailsValue;
    }

    public void setShelfDetailsValue(String shelfDetailsValue) {
        this.shelfDetailsValue = shelfDetailsValue;
    }

    public CategoryDetails getCategoryDetails() {
        return categoryDetails;
    }

    public void setCategoryDetails(CategoryDetails categoryDetails) {
        this.categoryDetails = categoryDetails;
    }

    public String getCategoryDetailsName() {
        return categoryDetailsName;
    }

    public void setCategoryDetailsName(String categoryDetailsName) {
        this.categoryDetailsName = categoryDetailsName;
    }

    public String getCategoryDetailsValue() {
        return categoryDetailsValue;
    }

    public void setCategoryDetailsValue(String categoryDetailsValue) {
        this.categoryDetailsValue = categoryDetailsValue;
    }

    public ProductDetails getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(ProductDetails productDetails) {
        this.productDetails = productDetails;
    }

    public String getProductDetailsName() {
        return productDetailsName;
    }

    public void setProductDetailsName(String productDetailsName) {
        this.productDetailsName = productDetailsName;
    }

    public String getProductDetailsValue() {
        return productDetailsValue;
    }

    public void setProductDetailsValue(String productDetailsValue) {
        this.productDetailsValue = productDetailsValue;
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getValue() {
        return value.get();
    }

    public void setValue(String value) {
        this.value.set(value);
    }

    public Object get() {
        if (warehouseDetails != null) {
            return warehouseDetails;
        }
        else if (sectorDetails != null) {
            return sectorDetails;
        }
        else if (shelfDetails != null) {
            return shelfDetails;
        }
        else if (categoryDetails != null) {
            return categoryDetails;
        }
        else if (productDetails != null) {
            return productDetails;
        }
        return null;
    }

}
