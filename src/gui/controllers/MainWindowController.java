/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.controllers;

import com.sun.glass.ui.Window;
import gui.ControlledScreen;
import gui.ScreensController;
import gui.WarehouseMain;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import jpa.Location;
import jpa.LocationJpaController;
import jpa.ProductJpaController;
import jpa.Sector;
import jpa.SectorJpaController;
import jpa.ShelfJpaController;
import jpa.Warehouse;
import jpa.WarehouseJpaController;

/**
 * FXML Controller class
 *
 * @author defri
 */
public class MainWindowController implements Initializable, ControlledScreen {

    /**
     * Initializes the controller class.
     */
    private ScreensController controller;

    @FXML
    private Pane pane;

    @FXML
    private Pane innerpane;

    @FXML
    private Button reportsButton;

    @FXML
    private Button stockButton;

    @FXML
    private Button productButton;

    @FXML
    private Button categoryButton;

    @FXML
    private Button shelfButton;

    @FXML
    private Button sectorButton;

    @FXML
    private Button warehouseButton;

    @FXML
    private Button locationButton;

    @FXML
    private Button userButton;

    @FXML
    private Button groupsButton;

    @FXML
    private Button loadUsers;

    @FXML
    private Button loadGroups;

    @FXML
    private Button loadLocation;

    @FXML
    private Button loadWarehouse;

    @FXML
    private Button loadSector;

    @FXML
    private Button loadShelf;

    @FXML
    private Button loadCategory;

    @FXML
    private Button loadProduct;

    @FXML
    private Button loadStock;

    @FXML
    private Button loadReport;

    @FXML
    private MenuButton profileMenuButton;

    @FXML
    private MenuItem menuitemInfo;

    @FXML
    private MenuItem menuitemHelp;

    @FXML
    private MenuItem logOut;

    @FXML
    private MenuItem viewProfile;

    @FXML
    private Label locationsNumber;

    @FXML
    private Label warehousesNumber;

    @FXML
    private Label sectorsNumber;

    @FXML
    private Label shelvesNumber;

    @FXML
    private Label productsNumber;

    @FXML
    private Label dateTimeLabel;

    private LocationJpaController locationJpaController;
    private WarehouseJpaController warehouseJpaController;
    private SectorJpaController sectorJpaController;
    private ShelfJpaController shelfJpaController;
    private ProductJpaController productJpaController;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        transaction();
        showStatistics();
        animations();

        viewProfile.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Stage stage = new Stage();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/ViewProfile.fxml"));
                try {
                    Pane pane = (Pane) loader.load();
                    stage.setScene(new Scene(pane));
                    stage.show();
                } catch (IOException ex) {
                    Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        userButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/User.fxml"));
                try {
                    Pane pane = (Pane) loader.load();
                    innerpane.getChildren().removeAll(innerpane.getChildren());
                    innerpane.getChildren().addAll(pane.getChildren());

                } catch (IOException ex) {
                    Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        groupsButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/Group.fxml"));
                try {
                    Pane pane = (Pane) loader.load();
                    innerpane.getChildren().removeAll(innerpane.getChildren());
                    innerpane.getChildren().addAll(pane.getChildren());
                } catch (IOException ex) {
                    Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        locationButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/Location.fxml"));
                try {
                    Pane pane = (Pane) loader.load();
                    innerpane.getChildren().removeAll(innerpane.getChildren());
                    innerpane.getChildren().addAll(pane.getChildren());
                } catch (IOException ex) {
                    Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        warehouseButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/Warehouse.fxml"));
                try {
                    Pane pane = (Pane) loader.load();
                    innerpane.getChildren().removeAll(innerpane.getChildren());
                    innerpane.getChildren().addAll(pane.getChildren());
                } catch (IOException ex) {
                    Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        sectorButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/Sector.fxml"));
                try {
                    Pane pane = (Pane) loader.load();
                    innerpane.getChildren().removeAll(innerpane.getChildren());
                    innerpane.getChildren().addAll(pane.getChildren());
                } catch (IOException ex) {
                    Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        shelfButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/Shelf.fxml"));
                try {
                    Pane pane = (Pane) loader.load();
                    innerpane.getChildren().removeAll(innerpane.getChildren());
                    innerpane.getChildren().addAll(pane.getChildren());
                } catch (IOException ex) {
                    Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        categoryButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/Category.fxml"));
                try {
                    Pane pane = (Pane) loader.load();
                    innerpane.getChildren().removeAll(innerpane.getChildren());
                    innerpane.getChildren().addAll(pane.getChildren());
                } catch (IOException ex) {
                    Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        productButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/Product.fxml"));
                try {
                    Pane pane = (Pane) loader.load();
                    innerpane.getChildren().removeAll(innerpane.getChildren());
                    innerpane.getChildren().addAll(pane.getChildren());
                } catch (IOException ex) {
                    Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        stockButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/Stock.fxml"));
                try {
                    Pane pane = (Pane) loader.load();
                    innerpane.getChildren().removeAll(innerpane.getChildren());
                    innerpane.getChildren().addAll(pane.getChildren());
                } catch (IOException ex) {
                    Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        logOut.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                controller.setScreen(WarehouseMain.LOGIN);
            }
        });

        Timeline showDate = new Timeline();
        showDate.setCycleCount(-1);
        final DateFormat dateFormat = DateFormat.getInstance();
        showDate.getKeyFrames().add(new KeyFrame(Duration.seconds(1), (ActionEvent event) -> {
            final Calendar c = Calendar.getInstance();
            dateTimeLabel.textProperty().set(c.get(Calendar.YEAR) + "/" + c.get((Calendar.MONTH)) + "/" + c.get((Calendar.DATE)) + "   " + c.getTime().getHours() + ":" + c.getTime().getMinutes() + ":" + c.getTime().getSeconds());
        }));
        showDate.play();

    }

    private void transaction() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WarehouseV2PU");
        locationJpaController = new LocationJpaController(emf);
        warehouseJpaController = new WarehouseJpaController(emf);
        sectorJpaController = new SectorJpaController(emf);
        shelfJpaController = new ShelfJpaController(emf);
        productJpaController = new ProductJpaController(emf);
    }

    private void showStatistics() {
        locationsNumber.textProperty().set(locationJpaController.findLocationEntities().stream().filter((location) -> (location.getIsActive() == 1)).map((_item) -> 1).reduce(0, Integer::sum) + "");
        warehousesNumber.textProperty().set(warehouseJpaController.findWarehouseEntities().stream().filter((warehouse) -> (warehouse.getIsActive() == 1)).map((_item) -> 1).reduce(0, Integer::sum) + "");
        sectorsNumber.textProperty().set(sectorJpaController.findSectorEntities().stream().filter((sector) -> (sector.getIsActive() == 1)).map((_item) -> 1).reduce(0, Integer::sum) + "");
        shelvesNumber.textProperty().set(shelfJpaController.findShelfEntities().stream().filter((shelf) -> (shelf.getIsActive() == 1)).map((_item) -> 1).reduce(0, Integer::sum) + "");
        productsNumber.textProperty().set(productJpaController.findProductEntities().stream().filter((product) -> (product.getIsActive() == 1)).map((_item) -> 1).reduce(0, Integer::sum) + "");
    }

    private void animations() {

        Timeline time = new Timeline();
        loadUsers.setOnMouseEntered((MouseEvent event) -> {
            Timeline timen = new Timeline();
            timen.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                    new KeyValue(userButton.layoutXProperty(), 58)));
            timen.play();
        });

        loadUsers.setOnMouseExited((MouseEvent event) -> {
            time.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                    new KeyValue(userButton.layoutXProperty(), -195)));
            time.play();
        });

        userButton.setOnMouseEntered((MouseEvent event) -> {
            time.stop();
        });

        userButton.setOnMouseExited((MouseEvent event) -> {
            time.play();
        });

        ///
        Timeline timee = new Timeline();
        loadGroups.setOnMouseEntered((MouseEvent event) -> {
            Timeline timen = new Timeline();
            timen.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                    new KeyValue(groupsButton.layoutXProperty(), 57)));
            timen.play();
        });

        loadGroups.setOnMouseExited((MouseEvent event) -> {
            timee.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                    new KeyValue(groupsButton.layoutXProperty(), -195)));
            timee.play();
        });

        groupsButton.setOnMouseEntered((MouseEvent event) -> {
            timee.stop();
        });

        groupsButton.setOnMouseExited((MouseEvent event) -> {
            timee.play();
        });

        ///
        Timeline timeee = new Timeline();
        loadLocation.setOnMouseEntered((MouseEvent event) -> {
            Timeline timen = new Timeline();
            timen.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                    new KeyValue(locationButton.layoutXProperty(), 57)));
            timen.play();
        });

        loadLocation.setOnMouseExited((MouseEvent event) -> {
            timeee.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                    new KeyValue(locationButton.layoutXProperty(), -195)));
            timeee.play();
        });

        locationButton.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                timeee.stop();
            }
        });

        locationButton.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                timeee.play();
            }
        });

        ///
        Timeline loadW = new Timeline();
        this.loadWarehouse.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Timeline timen = new Timeline();
                timen.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(warehouseButton.layoutXProperty(), 57)));
                timen.play();
            }
        });

        this.loadWarehouse.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadW.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(warehouseButton.layoutXProperty(), -195)));
                loadW.play();
            }
        });

        warehouseButton.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadW.stop();
            }
        });

        warehouseButton.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadW.play();
            }
        });

        ///
        Timeline loadS = new Timeline();
        loadSector.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Timeline timen = new Timeline();
                timen.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(sectorButton.layoutXProperty(), 57)));
                timen.play();
            }
        });

        this.loadSector.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadS.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(sectorButton.layoutXProperty(), -195)));
                loadS.play();
            }
        });

        sectorButton.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadS.stop();
            }
        });

        sectorButton.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadS.play();
            }
        });

        ///
        Timeline loadSh = new Timeline();
        loadShelf.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Timeline timen = new Timeline();
                timen.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(shelfButton.layoutXProperty(), 57)));
                timen.play();
            }
        });

        this.loadShelf.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadSh.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(shelfButton.layoutXProperty(), -195)));
                loadSh.play();
            }
        });

        shelfButton.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadSh.stop();
            }
        });

        shelfButton.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadSh.play();
            }
        });

        ///
        Timeline loadC = new Timeline();
        loadCategory.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Timeline timen = new Timeline();
                timen.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(categoryButton.layoutXProperty(), 57)));
                timen.play();
            }
        });

        this.loadCategory.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadC.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(categoryButton.layoutXProperty(), -195)));
                loadC.play();
            }
        });

        categoryButton.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadC.stop();
            }
        });

        categoryButton.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadC.play();
            }
        });

        ///
        Timeline loadP = new Timeline();
        loadProduct.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Timeline timen = new Timeline();
                timen.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(productButton.layoutXProperty(), 57)));
                timen.play();
            }
        });

        this.loadProduct.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadP.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(productButton.layoutXProperty(), -195)));
                loadP.play();
            }
        });

        productButton.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadP.stop();
            }
        });

        productButton.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadP.play();
            }
        });

        ///
        Timeline loadStc = new Timeline();
        loadStock.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Timeline timen = new Timeline();
                timen.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(stockButton.layoutXProperty(), 57)));
                timen.play();
            }
        });

        this.loadStock.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadStc.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(stockButton.layoutXProperty(), -195)));
                loadStc.play();
            }
        });

        stockButton.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadStc.stop();
            }
        });

        stockButton.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadStc.play();
            }
        });

        ///
        Timeline loadR = new Timeline();
        loadReport.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Timeline timen = new Timeline();
                timen.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(reportsButton.layoutXProperty(), 57)));
                timen.play();
            }
        });

        this.loadReport.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadR.getKeyFrames().add(new KeyFrame(Duration.millis(100),
                        new KeyValue(reportsButton.layoutXProperty(), -195)));
                loadR.play();
            }
        });

        reportsButton.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadR.stop();
            }
        });

        reportsButton.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadR.play();
            }
        });

    }

    @Override
    public void setScreenParent(ScreensController screenPage) {
        controller = screenPage;
    }

}
