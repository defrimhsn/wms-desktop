/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class WarehouseJpaController implements Serializable {

    public WarehouseJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Warehouse warehouse) {
        if (warehouse.getWarehouseDetailsCollection() == null) {
            warehouse.setWarehouseDetailsCollection(new ArrayList<WarehouseDetails>());
        }
        if (warehouse.getSectorCollection() == null) {
            warehouse.setSectorCollection(new ArrayList<Sector>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Location locationId = warehouse.getLocationId();
            if (locationId != null) {
                locationId = em.getReference(locationId.getClass(), locationId.getId());
                warehouse.setLocationId(locationId);
            }
            Collection<WarehouseDetails> attachedWarehouseDetailsCollection = new ArrayList<WarehouseDetails>();
            for (WarehouseDetails warehouseDetailsCollectionWarehouseDetailsToAttach : warehouse.getWarehouseDetailsCollection()) {
                warehouseDetailsCollectionWarehouseDetailsToAttach = em.getReference(warehouseDetailsCollectionWarehouseDetailsToAttach.getClass(), warehouseDetailsCollectionWarehouseDetailsToAttach.getId());
                attachedWarehouseDetailsCollection.add(warehouseDetailsCollectionWarehouseDetailsToAttach);
            }
            warehouse.setWarehouseDetailsCollection(attachedWarehouseDetailsCollection);
            Collection<Sector> attachedSectorCollection = new ArrayList<Sector>();
            for (Sector sectorCollectionSectorToAttach : warehouse.getSectorCollection()) {
                sectorCollectionSectorToAttach = em.getReference(sectorCollectionSectorToAttach.getClass(), sectorCollectionSectorToAttach.getId());
                attachedSectorCollection.add(sectorCollectionSectorToAttach);
            }
            warehouse.setSectorCollection(attachedSectorCollection);
            em.persist(warehouse);
            if (locationId != null) {
                locationId.getWarehouseCollection().add(warehouse);
                locationId = em.merge(locationId);
            }
            for (WarehouseDetails warehouseDetailsCollectionWarehouseDetails : warehouse.getWarehouseDetailsCollection()) {
                Warehouse oldWarehouseIdOfWarehouseDetailsCollectionWarehouseDetails = warehouseDetailsCollectionWarehouseDetails.getWarehouseId();
                warehouseDetailsCollectionWarehouseDetails.setWarehouseId(warehouse);
                warehouseDetailsCollectionWarehouseDetails = em.merge(warehouseDetailsCollectionWarehouseDetails);
                if (oldWarehouseIdOfWarehouseDetailsCollectionWarehouseDetails != null) {
                    oldWarehouseIdOfWarehouseDetailsCollectionWarehouseDetails.getWarehouseDetailsCollection().remove(warehouseDetailsCollectionWarehouseDetails);
                    oldWarehouseIdOfWarehouseDetailsCollectionWarehouseDetails = em.merge(oldWarehouseIdOfWarehouseDetailsCollectionWarehouseDetails);
                }
            }
            for (Sector sectorCollectionSector : warehouse.getSectorCollection()) {
                Warehouse oldWarehouseIdOfSectorCollectionSector = sectorCollectionSector.getWarehouseId();
                sectorCollectionSector.setWarehouseId(warehouse);
                sectorCollectionSector = em.merge(sectorCollectionSector);
                if (oldWarehouseIdOfSectorCollectionSector != null) {
                    oldWarehouseIdOfSectorCollectionSector.getSectorCollection().remove(sectorCollectionSector);
                    oldWarehouseIdOfSectorCollectionSector = em.merge(oldWarehouseIdOfSectorCollectionSector);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Warehouse warehouse) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Warehouse persistentWarehouse = em.find(Warehouse.class, warehouse.getId());
            Location locationIdOld = persistentWarehouse.getLocationId();
            Location locationIdNew = warehouse.getLocationId();
            Collection<WarehouseDetails> warehouseDetailsCollectionOld = persistentWarehouse.getWarehouseDetailsCollection();
            Collection<WarehouseDetails> warehouseDetailsCollectionNew = warehouse.getWarehouseDetailsCollection();
            Collection<Sector> sectorCollectionOld = persistentWarehouse.getSectorCollection();
            Collection<Sector> sectorCollectionNew = warehouse.getSectorCollection();
            List<String> illegalOrphanMessages = null;
            for (WarehouseDetails warehouseDetailsCollectionOldWarehouseDetails : warehouseDetailsCollectionOld) {
                if (!warehouseDetailsCollectionNew.contains(warehouseDetailsCollectionOldWarehouseDetails)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain WarehouseDetails " + warehouseDetailsCollectionOldWarehouseDetails + " since its warehouseId field is not nullable.");
                }
            }
            for (Sector sectorCollectionOldSector : sectorCollectionOld) {
                if (!sectorCollectionNew.contains(sectorCollectionOldSector)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Sector " + sectorCollectionOldSector + " since its warehouseId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (locationIdNew != null) {
                locationIdNew = em.getReference(locationIdNew.getClass(), locationIdNew.getId());
                warehouse.setLocationId(locationIdNew);
            }
            Collection<WarehouseDetails> attachedWarehouseDetailsCollectionNew = new ArrayList<WarehouseDetails>();
            for (WarehouseDetails warehouseDetailsCollectionNewWarehouseDetailsToAttach : warehouseDetailsCollectionNew) {
                warehouseDetailsCollectionNewWarehouseDetailsToAttach = em.getReference(warehouseDetailsCollectionNewWarehouseDetailsToAttach.getClass(), warehouseDetailsCollectionNewWarehouseDetailsToAttach.getId());
                attachedWarehouseDetailsCollectionNew.add(warehouseDetailsCollectionNewWarehouseDetailsToAttach);
            }
            warehouseDetailsCollectionNew = attachedWarehouseDetailsCollectionNew;
            warehouse.setWarehouseDetailsCollection(warehouseDetailsCollectionNew);
            Collection<Sector> attachedSectorCollectionNew = new ArrayList<Sector>();
            for (Sector sectorCollectionNewSectorToAttach : sectorCollectionNew) {
                sectorCollectionNewSectorToAttach = em.getReference(sectorCollectionNewSectorToAttach.getClass(), sectorCollectionNewSectorToAttach.getId());
                attachedSectorCollectionNew.add(sectorCollectionNewSectorToAttach);
            }
            sectorCollectionNew = attachedSectorCollectionNew;
            warehouse.setSectorCollection(sectorCollectionNew);
            warehouse = em.merge(warehouse);
            if (locationIdOld != null && !locationIdOld.equals(locationIdNew)) {
                locationIdOld.getWarehouseCollection().remove(warehouse);
                locationIdOld = em.merge(locationIdOld);
            }
            if (locationIdNew != null && !locationIdNew.equals(locationIdOld)) {
                locationIdNew.getWarehouseCollection().add(warehouse);
                locationIdNew = em.merge(locationIdNew);
            }
            for (WarehouseDetails warehouseDetailsCollectionNewWarehouseDetails : warehouseDetailsCollectionNew) {
                if (!warehouseDetailsCollectionOld.contains(warehouseDetailsCollectionNewWarehouseDetails)) {
                    Warehouse oldWarehouseIdOfWarehouseDetailsCollectionNewWarehouseDetails = warehouseDetailsCollectionNewWarehouseDetails.getWarehouseId();
                    warehouseDetailsCollectionNewWarehouseDetails.setWarehouseId(warehouse);
                    warehouseDetailsCollectionNewWarehouseDetails = em.merge(warehouseDetailsCollectionNewWarehouseDetails);
                    if (oldWarehouseIdOfWarehouseDetailsCollectionNewWarehouseDetails != null && !oldWarehouseIdOfWarehouseDetailsCollectionNewWarehouseDetails.equals(warehouse)) {
                        oldWarehouseIdOfWarehouseDetailsCollectionNewWarehouseDetails.getWarehouseDetailsCollection().remove(warehouseDetailsCollectionNewWarehouseDetails);
                        oldWarehouseIdOfWarehouseDetailsCollectionNewWarehouseDetails = em.merge(oldWarehouseIdOfWarehouseDetailsCollectionNewWarehouseDetails);
                    }
                }
            }
            for (Sector sectorCollectionNewSector : sectorCollectionNew) {
                if (!sectorCollectionOld.contains(sectorCollectionNewSector)) {
                    Warehouse oldWarehouseIdOfSectorCollectionNewSector = sectorCollectionNewSector.getWarehouseId();
                    sectorCollectionNewSector.setWarehouseId(warehouse);
                    sectorCollectionNewSector = em.merge(sectorCollectionNewSector);
                    if (oldWarehouseIdOfSectorCollectionNewSector != null && !oldWarehouseIdOfSectorCollectionNewSector.equals(warehouse)) {
                        oldWarehouseIdOfSectorCollectionNewSector.getSectorCollection().remove(sectorCollectionNewSector);
                        oldWarehouseIdOfSectorCollectionNewSector = em.merge(oldWarehouseIdOfSectorCollectionNewSector);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = warehouse.getId();
                if (findWarehouse(id) == null) {
                    throw new NonexistentEntityException("The warehouse with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Warehouse warehouse;
            try {
                warehouse = em.getReference(Warehouse.class, id);
                warehouse.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The warehouse with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<WarehouseDetails> warehouseDetailsCollectionOrphanCheck = warehouse.getWarehouseDetailsCollection();
            for (WarehouseDetails warehouseDetailsCollectionOrphanCheckWarehouseDetails : warehouseDetailsCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Warehouse (" + warehouse + ") cannot be destroyed since the WarehouseDetails " + warehouseDetailsCollectionOrphanCheckWarehouseDetails + " in its warehouseDetailsCollection field has a non-nullable warehouseId field.");
            }
            Collection<Sector> sectorCollectionOrphanCheck = warehouse.getSectorCollection();
            for (Sector sectorCollectionOrphanCheckSector : sectorCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Warehouse (" + warehouse + ") cannot be destroyed since the Sector " + sectorCollectionOrphanCheckSector + " in its sectorCollection field has a non-nullable warehouseId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Location locationId = warehouse.getLocationId();
            if (locationId != null) {
                locationId.getWarehouseCollection().remove(warehouse);
                locationId = em.merge(locationId);
            }
            em.remove(warehouse);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Warehouse> findWarehouseEntities() {
        return findWarehouseEntities(true, -1, -1);
    }

    public List<Warehouse> findWarehouseEntities(int maxResults, int firstResult) {
        return findWarehouseEntities(false, maxResults, firstResult);
    }

    private List<Warehouse> findWarehouseEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Warehouse.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Warehouse findWarehouse(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Warehouse.class, id);
        } finally {
            em.close();
        }
    }

    public int getWarehouseCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Warehouse> rt = cq.from(Warehouse.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
