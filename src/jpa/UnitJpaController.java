/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class UnitJpaController implements Serializable {

    public UnitJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Unit unit) {
        if (unit.getProductUnitCollection() == null) {
            unit.setProductUnitCollection(new ArrayList<ProductUnit>());
        }
        if (unit.getStockCollection() == null) {
            unit.setStockCollection(new ArrayList<Stock>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<ProductUnit> attachedProductUnitCollection = new ArrayList<ProductUnit>();
            for (ProductUnit productUnitCollectionProductUnitToAttach : unit.getProductUnitCollection()) {
                productUnitCollectionProductUnitToAttach = em.getReference(productUnitCollectionProductUnitToAttach.getClass(), productUnitCollectionProductUnitToAttach.getId());
                attachedProductUnitCollection.add(productUnitCollectionProductUnitToAttach);
            }
            unit.setProductUnitCollection(attachedProductUnitCollection);
            Collection<Stock> attachedStockCollection = new ArrayList<Stock>();
            for (Stock stockCollectionStockToAttach : unit.getStockCollection()) {
                stockCollectionStockToAttach = em.getReference(stockCollectionStockToAttach.getClass(), stockCollectionStockToAttach.getId());
                attachedStockCollection.add(stockCollectionStockToAttach);
            }
            unit.setStockCollection(attachedStockCollection);
            em.persist(unit);
            for (ProductUnit productUnitCollectionProductUnit : unit.getProductUnitCollection()) {
                Unit oldUnitIdOfProductUnitCollectionProductUnit = productUnitCollectionProductUnit.getUnitId();
                productUnitCollectionProductUnit.setUnitId(unit);
                productUnitCollectionProductUnit = em.merge(productUnitCollectionProductUnit);
                if (oldUnitIdOfProductUnitCollectionProductUnit != null) {
                    oldUnitIdOfProductUnitCollectionProductUnit.getProductUnitCollection().remove(productUnitCollectionProductUnit);
                    oldUnitIdOfProductUnitCollectionProductUnit = em.merge(oldUnitIdOfProductUnitCollectionProductUnit);
                }
            }
            for (Stock stockCollectionStock : unit.getStockCollection()) {
                Unit oldUnitIdOfStockCollectionStock = stockCollectionStock.getUnitId();
                stockCollectionStock.setUnitId(unit);
                stockCollectionStock = em.merge(stockCollectionStock);
                if (oldUnitIdOfStockCollectionStock != null) {
                    oldUnitIdOfStockCollectionStock.getStockCollection().remove(stockCollectionStock);
                    oldUnitIdOfStockCollectionStock = em.merge(oldUnitIdOfStockCollectionStock);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Unit unit) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Unit persistentUnit = em.find(Unit.class, unit.getId());
            Collection<ProductUnit> productUnitCollectionOld = persistentUnit.getProductUnitCollection();
            Collection<ProductUnit> productUnitCollectionNew = unit.getProductUnitCollection();
            Collection<Stock> stockCollectionOld = persistentUnit.getStockCollection();
            Collection<Stock> stockCollectionNew = unit.getStockCollection();
            List<String> illegalOrphanMessages = null;
            for (ProductUnit productUnitCollectionOldProductUnit : productUnitCollectionOld) {
                if (!productUnitCollectionNew.contains(productUnitCollectionOldProductUnit)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ProductUnit " + productUnitCollectionOldProductUnit + " since its unitId field is not nullable.");
                }
            }
            for (Stock stockCollectionOldStock : stockCollectionOld) {
                if (!stockCollectionNew.contains(stockCollectionOldStock)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Stock " + stockCollectionOldStock + " since its unitId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<ProductUnit> attachedProductUnitCollectionNew = new ArrayList<ProductUnit>();
            for (ProductUnit productUnitCollectionNewProductUnitToAttach : productUnitCollectionNew) {
                productUnitCollectionNewProductUnitToAttach = em.getReference(productUnitCollectionNewProductUnitToAttach.getClass(), productUnitCollectionNewProductUnitToAttach.getId());
                attachedProductUnitCollectionNew.add(productUnitCollectionNewProductUnitToAttach);
            }
            productUnitCollectionNew = attachedProductUnitCollectionNew;
            unit.setProductUnitCollection(productUnitCollectionNew);
            Collection<Stock> attachedStockCollectionNew = new ArrayList<Stock>();
            for (Stock stockCollectionNewStockToAttach : stockCollectionNew) {
                stockCollectionNewStockToAttach = em.getReference(stockCollectionNewStockToAttach.getClass(), stockCollectionNewStockToAttach.getId());
                attachedStockCollectionNew.add(stockCollectionNewStockToAttach);
            }
            stockCollectionNew = attachedStockCollectionNew;
            unit.setStockCollection(stockCollectionNew);
            unit = em.merge(unit);
            for (ProductUnit productUnitCollectionNewProductUnit : productUnitCollectionNew) {
                if (!productUnitCollectionOld.contains(productUnitCollectionNewProductUnit)) {
                    Unit oldUnitIdOfProductUnitCollectionNewProductUnit = productUnitCollectionNewProductUnit.getUnitId();
                    productUnitCollectionNewProductUnit.setUnitId(unit);
                    productUnitCollectionNewProductUnit = em.merge(productUnitCollectionNewProductUnit);
                    if (oldUnitIdOfProductUnitCollectionNewProductUnit != null && !oldUnitIdOfProductUnitCollectionNewProductUnit.equals(unit)) {
                        oldUnitIdOfProductUnitCollectionNewProductUnit.getProductUnitCollection().remove(productUnitCollectionNewProductUnit);
                        oldUnitIdOfProductUnitCollectionNewProductUnit = em.merge(oldUnitIdOfProductUnitCollectionNewProductUnit);
                    }
                }
            }
            for (Stock stockCollectionNewStock : stockCollectionNew) {
                if (!stockCollectionOld.contains(stockCollectionNewStock)) {
                    Unit oldUnitIdOfStockCollectionNewStock = stockCollectionNewStock.getUnitId();
                    stockCollectionNewStock.setUnitId(unit);
                    stockCollectionNewStock = em.merge(stockCollectionNewStock);
                    if (oldUnitIdOfStockCollectionNewStock != null && !oldUnitIdOfStockCollectionNewStock.equals(unit)) {
                        oldUnitIdOfStockCollectionNewStock.getStockCollection().remove(stockCollectionNewStock);
                        oldUnitIdOfStockCollectionNewStock = em.merge(oldUnitIdOfStockCollectionNewStock);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = unit.getId();
                if (findUnit(id) == null) {
                    throw new NonexistentEntityException("The unit with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Unit unit;
            try {
                unit = em.getReference(Unit.class, id);
                unit.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The unit with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<ProductUnit> productUnitCollectionOrphanCheck = unit.getProductUnitCollection();
            for (ProductUnit productUnitCollectionOrphanCheckProductUnit : productUnitCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Unit (" + unit + ") cannot be destroyed since the ProductUnit " + productUnitCollectionOrphanCheckProductUnit + " in its productUnitCollection field has a non-nullable unitId field.");
            }
            Collection<Stock> stockCollectionOrphanCheck = unit.getStockCollection();
            for (Stock stockCollectionOrphanCheckStock : stockCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Unit (" + unit + ") cannot be destroyed since the Stock " + stockCollectionOrphanCheckStock + " in its stockCollection field has a non-nullable unitId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(unit);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Unit> findUnitEntities() {
        return findUnitEntities(true, -1, -1);
    }

    public List<Unit> findUnitEntities(int maxResults, int firstResult) {
        return findUnitEntities(false, maxResults, firstResult);
    }

    private List<Unit> findUnitEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Unit.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Unit findUnit(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Unit.class, id);
        } finally {
            em.close();
        }
    }

    public int getUnitCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Unit> rt = cq.from(Unit.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
