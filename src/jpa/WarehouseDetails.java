/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Deathrow
 */
@Entity
@Table(name = "warehouse_details")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WarehouseDetails.findAll", query = "SELECT w FROM WarehouseDetails w"),
    @NamedQuery(name = "WarehouseDetails.findById", query = "SELECT w FROM WarehouseDetails w WHERE w.id = :id"),
    @NamedQuery(name = "WarehouseDetails.findByName", query = "SELECT w FROM WarehouseDetails w WHERE w.name = :name"),
    @NamedQuery(name = "WarehouseDetails.findByValue", query = "SELECT w FROM WarehouseDetails w WHERE w.value = :value"),
    @NamedQuery(name = "WarehouseDetails.findByCreatedDate", query = "SELECT w FROM WarehouseDetails w WHERE w.createdDate = :createdDate"),
    @NamedQuery(name = "WarehouseDetails.findByUpdatedDate", query = "SELECT w FROM WarehouseDetails w WHERE w.updatedDate = :updatedDate"),
    @NamedQuery(name = "WarehouseDetails.findByIsActive", query = "SELECT w FROM WarehouseDetails w WHERE w.isActive = :isActive")})
public class WarehouseDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "value")
    private String value;
    @Basic(optional = false)
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "updated_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Basic(optional = false)
    @Column(name = "is_active")
    private int isActive;
    @JoinColumn(name = "warehouse_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Warehouse warehouseId;

    public WarehouseDetails() {
    }

    public WarehouseDetails(Integer id) {
        this.id = id;
    }

    public WarehouseDetails(Integer id, String name, String value, Date createdDate, int isActive) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.createdDate = createdDate;
        this.isActive = isActive;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public Warehouse getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Warehouse warehouseId) {
        this.warehouseId = warehouseId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WarehouseDetails)) {
            return false;
        }
        WarehouseDetails other = (WarehouseDetails) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.WarehouseDetails[ id=" + id + " ]";
    }

}
