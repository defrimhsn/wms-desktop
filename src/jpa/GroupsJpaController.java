/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class GroupsJpaController implements Serializable {

    public GroupsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Groups groups) {
        if (groups.getGroupsPermissionsCollection() == null) {
            groups.setGroupsPermissionsCollection(new ArrayList<GroupsPermissions>());
        }
        if (groups.getUserCollection() == null) {
            groups.setUserCollection(new ArrayList<User>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<GroupsPermissions> attachedGroupsPermissionsCollection = new ArrayList<GroupsPermissions>();
            for (GroupsPermissions groupsPermissionsCollectionGroupsPermissionsToAttach : groups.getGroupsPermissionsCollection()) {
                groupsPermissionsCollectionGroupsPermissionsToAttach = em.getReference(groupsPermissionsCollectionGroupsPermissionsToAttach.getClass(), groupsPermissionsCollectionGroupsPermissionsToAttach.getId());
                attachedGroupsPermissionsCollection.add(groupsPermissionsCollectionGroupsPermissionsToAttach);
            }
            groups.setGroupsPermissionsCollection(attachedGroupsPermissionsCollection);
            Collection<User> attachedUserCollection = new ArrayList<User>();
            for (User userCollectionUserToAttach : groups.getUserCollection()) {
                userCollectionUserToAttach = em.getReference(userCollectionUserToAttach.getClass(), userCollectionUserToAttach.getId());
                attachedUserCollection.add(userCollectionUserToAttach);
            }
            groups.setUserCollection(attachedUserCollection);
            em.persist(groups);
            for (GroupsPermissions groupsPermissionsCollectionGroupsPermissions : groups.getGroupsPermissionsCollection()) {
                Groups oldGroupsIdOfGroupsPermissionsCollectionGroupsPermissions = groupsPermissionsCollectionGroupsPermissions.getGroupsId();
                groupsPermissionsCollectionGroupsPermissions.setGroupsId(groups);
                groupsPermissionsCollectionGroupsPermissions = em.merge(groupsPermissionsCollectionGroupsPermissions);
                if (oldGroupsIdOfGroupsPermissionsCollectionGroupsPermissions != null) {
                    oldGroupsIdOfGroupsPermissionsCollectionGroupsPermissions.getGroupsPermissionsCollection().remove(groupsPermissionsCollectionGroupsPermissions);
                    oldGroupsIdOfGroupsPermissionsCollectionGroupsPermissions = em.merge(oldGroupsIdOfGroupsPermissionsCollectionGroupsPermissions);
                }
            }
            for (User userCollectionUser : groups.getUserCollection()) {
                Groups oldGroupIdOfUserCollectionUser = userCollectionUser.getGroupId();
                userCollectionUser.setGroupId(groups);
                userCollectionUser = em.merge(userCollectionUser);
                if (oldGroupIdOfUserCollectionUser != null) {
                    oldGroupIdOfUserCollectionUser.getUserCollection().remove(userCollectionUser);
                    oldGroupIdOfUserCollectionUser = em.merge(oldGroupIdOfUserCollectionUser);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Groups groups) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Groups persistentGroups = em.find(Groups.class, groups.getId());
            Collection<GroupsPermissions> groupsPermissionsCollectionOld = persistentGroups.getGroupsPermissionsCollection();
            Collection<GroupsPermissions> groupsPermissionsCollectionNew = groups.getGroupsPermissionsCollection();
            Collection<User> userCollectionOld = persistentGroups.getUserCollection();
            Collection<User> userCollectionNew = groups.getUserCollection();
            List<String> illegalOrphanMessages = null;
            for (GroupsPermissions groupsPermissionsCollectionOldGroupsPermissions : groupsPermissionsCollectionOld) {
                if (!groupsPermissionsCollectionNew.contains(groupsPermissionsCollectionOldGroupsPermissions)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain GroupsPermissions " + groupsPermissionsCollectionOldGroupsPermissions + " since its groupsId field is not nullable.");
                }
            }
            for (User userCollectionOldUser : userCollectionOld) {
                if (!userCollectionNew.contains(userCollectionOldUser)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain User " + userCollectionOldUser + " since its groupId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<GroupsPermissions> attachedGroupsPermissionsCollectionNew = new ArrayList<GroupsPermissions>();
            for (GroupsPermissions groupsPermissionsCollectionNewGroupsPermissionsToAttach : groupsPermissionsCollectionNew) {
                groupsPermissionsCollectionNewGroupsPermissionsToAttach = em.getReference(groupsPermissionsCollectionNewGroupsPermissionsToAttach.getClass(), groupsPermissionsCollectionNewGroupsPermissionsToAttach.getId());
                attachedGroupsPermissionsCollectionNew.add(groupsPermissionsCollectionNewGroupsPermissionsToAttach);
            }
            groupsPermissionsCollectionNew = attachedGroupsPermissionsCollectionNew;
            groups.setGroupsPermissionsCollection(groupsPermissionsCollectionNew);
            Collection<User> attachedUserCollectionNew = new ArrayList<User>();
            for (User userCollectionNewUserToAttach : userCollectionNew) {
                userCollectionNewUserToAttach = em.getReference(userCollectionNewUserToAttach.getClass(), userCollectionNewUserToAttach.getId());
                attachedUserCollectionNew.add(userCollectionNewUserToAttach);
            }
            userCollectionNew = attachedUserCollectionNew;
            groups.setUserCollection(userCollectionNew);
            groups = em.merge(groups);
            for (GroupsPermissions groupsPermissionsCollectionNewGroupsPermissions : groupsPermissionsCollectionNew) {
                if (!groupsPermissionsCollectionOld.contains(groupsPermissionsCollectionNewGroupsPermissions)) {
                    Groups oldGroupsIdOfGroupsPermissionsCollectionNewGroupsPermissions = groupsPermissionsCollectionNewGroupsPermissions.getGroupsId();
                    groupsPermissionsCollectionNewGroupsPermissions.setGroupsId(groups);
                    groupsPermissionsCollectionNewGroupsPermissions = em.merge(groupsPermissionsCollectionNewGroupsPermissions);
                    if (oldGroupsIdOfGroupsPermissionsCollectionNewGroupsPermissions != null && !oldGroupsIdOfGroupsPermissionsCollectionNewGroupsPermissions.equals(groups)) {
                        oldGroupsIdOfGroupsPermissionsCollectionNewGroupsPermissions.getGroupsPermissionsCollection().remove(groupsPermissionsCollectionNewGroupsPermissions);
                        oldGroupsIdOfGroupsPermissionsCollectionNewGroupsPermissions = em.merge(oldGroupsIdOfGroupsPermissionsCollectionNewGroupsPermissions);
                    }
                }
            }
            for (User userCollectionNewUser : userCollectionNew) {
                if (!userCollectionOld.contains(userCollectionNewUser)) {
                    Groups oldGroupIdOfUserCollectionNewUser = userCollectionNewUser.getGroupId();
                    userCollectionNewUser.setGroupId(groups);
                    userCollectionNewUser = em.merge(userCollectionNewUser);
                    if (oldGroupIdOfUserCollectionNewUser != null && !oldGroupIdOfUserCollectionNewUser.equals(groups)) {
                        oldGroupIdOfUserCollectionNewUser.getUserCollection().remove(userCollectionNewUser);
                        oldGroupIdOfUserCollectionNewUser = em.merge(oldGroupIdOfUserCollectionNewUser);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = groups.getId();
                if (findGroups(id) == null) {
                    throw new NonexistentEntityException("The groups with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Groups groups;
            try {
                groups = em.getReference(Groups.class, id);
                groups.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The groups with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<GroupsPermissions> groupsPermissionsCollectionOrphanCheck = groups.getGroupsPermissionsCollection();
            for (GroupsPermissions groupsPermissionsCollectionOrphanCheckGroupsPermissions : groupsPermissionsCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Groups (" + groups + ") cannot be destroyed since the GroupsPermissions " + groupsPermissionsCollectionOrphanCheckGroupsPermissions + " in its groupsPermissionsCollection field has a non-nullable groupsId field.");
            }
            Collection<User> userCollectionOrphanCheck = groups.getUserCollection();
            for (User userCollectionOrphanCheckUser : userCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Groups (" + groups + ") cannot be destroyed since the User " + userCollectionOrphanCheckUser + " in its userCollection field has a non-nullable groupId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(groups);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Groups> findGroupsEntities() {
        return findGroupsEntities(true, -1, -1);
    }

    public List<Groups> findGroupsEntities(int maxResults, int firstResult) {
        return findGroupsEntities(false, maxResults, firstResult);
    }

    private List<Groups> findGroupsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Groups.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Groups findGroups(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Groups.class, id);
        } finally {
            em.close();
        }
    }

    public int getGroupsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Groups> rt = cq.from(Groups.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
