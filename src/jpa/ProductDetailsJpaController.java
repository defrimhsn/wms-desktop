/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class ProductDetailsJpaController implements Serializable {

    public ProductDetailsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ProductDetails productDetails) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Product productId = productDetails.getProductId();
            if (productId != null) {
                productId = em.getReference(productId.getClass(), productId.getId());
                productDetails.setProductId(productId);
            }
            em.persist(productDetails);
            if (productId != null) {
                productId.getProductDetailsCollection().add(productDetails);
                productId = em.merge(productId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ProductDetails productDetails) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProductDetails persistentProductDetails = em.find(ProductDetails.class, productDetails.getId());
            Product productIdOld = persistentProductDetails.getProductId();
            Product productIdNew = productDetails.getProductId();
            if (productIdNew != null) {
                productIdNew = em.getReference(productIdNew.getClass(), productIdNew.getId());
                productDetails.setProductId(productIdNew);
            }
            productDetails = em.merge(productDetails);
            if (productIdOld != null && !productIdOld.equals(productIdNew)) {
                productIdOld.getProductDetailsCollection().remove(productDetails);
                productIdOld = em.merge(productIdOld);
            }
            if (productIdNew != null && !productIdNew.equals(productIdOld)) {
                productIdNew.getProductDetailsCollection().add(productDetails);
                productIdNew = em.merge(productIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = productDetails.getId();
                if (findProductDetails(id) == null) {
                    throw new NonexistentEntityException("The productDetails with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProductDetails productDetails;
            try {
                productDetails = em.getReference(ProductDetails.class, id);
                productDetails.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The productDetails with id " + id + " no longer exists.", enfe);
            }
            Product productId = productDetails.getProductId();
            if (productId != null) {
                productId.getProductDetailsCollection().remove(productDetails);
                productId = em.merge(productId);
            }
            em.remove(productDetails);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ProductDetails> findProductDetailsEntities() {
        return findProductDetailsEntities(true, -1, -1);
    }

    public List<ProductDetails> findProductDetailsEntities(int maxResults, int firstResult) {
        return findProductDetailsEntities(false, maxResults, firstResult);
    }

    private List<ProductDetails> findProductDetailsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ProductDetails.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ProductDetails findProductDetails(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ProductDetails.class, id);
        } finally {
            em.close();
        }
    }

    public int getProductDetailsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ProductDetails> rt = cq.from(ProductDetails.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
