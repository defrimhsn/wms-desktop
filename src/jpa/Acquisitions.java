/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Deathrow
 */
@Entity
@Table(name = "acquisitions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Acquisitions.findAll", query = "SELECT a FROM Acquisitions a"),
    @NamedQuery(name = "Acquisitions.findById", query = "SELECT a FROM Acquisitions a WHERE a.id = :id"),
    @NamedQuery(name = "Acquisitions.findByQuantity", query = "SELECT a FROM Acquisitions a WHERE a.quantity = :quantity"),
    @NamedQuery(name = "Acquisitions.findByAcqPrice", query = "SELECT a FROM Acquisitions a WHERE a.acqPrice = :acqPrice"),
    @NamedQuery(name = "Acquisitions.findByAcqDate", query = "SELECT a FROM Acquisitions a WHERE a.acqDate = :acqDate")})
public class Acquisitions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "quantity")
    private int quantity;
    @Basic(optional = false)
    @Column(name = "acq_price")
    private double acqPrice;
    @Basic(optional = false)
    @Column(name = "acq_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date acqDate;
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Product productId;

    public Acquisitions() {
    }

    public Acquisitions(Integer id) {
        this.id = id;
    }

    public Acquisitions(Integer id, int quantity, double acqPrice, Date acqDate) {
        this.id = id;
        this.quantity = quantity;
        this.acqPrice = acqPrice;
        this.acqDate = acqDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getAcqPrice() {
        return acqPrice;
    }

    public void setAcqPrice(double acqPrice) {
        this.acqPrice = acqPrice;
    }

    public Date getAcqDate() {
        return acqDate;
    }

    public void setAcqDate(Date acqDate) {
        this.acqDate = acqDate;
    }

    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acquisitions)) {
            return false;
        }
        Acquisitions other = (Acquisitions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.Acquisitions[ id=" + id + " ]";
    }

}
