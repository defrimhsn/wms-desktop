/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class LocationJpaController implements Serializable {

    public LocationJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Location location) {
        if (location.getWarehouseCollection() == null) {
            location.setWarehouseCollection(new ArrayList<Warehouse>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Warehouse> attachedWarehouseCollection = new ArrayList<Warehouse>();
            for (Warehouse warehouseCollectionWarehouseToAttach : location.getWarehouseCollection()) {
                warehouseCollectionWarehouseToAttach = em.getReference(warehouseCollectionWarehouseToAttach.getClass(), warehouseCollectionWarehouseToAttach.getId());
                attachedWarehouseCollection.add(warehouseCollectionWarehouseToAttach);
            }
            location.setWarehouseCollection(attachedWarehouseCollection);
            em.persist(location);
            for (Warehouse warehouseCollectionWarehouse : location.getWarehouseCollection()) {
                Location oldLocationIdOfWarehouseCollectionWarehouse = warehouseCollectionWarehouse.getLocationId();
                warehouseCollectionWarehouse.setLocationId(location);
                warehouseCollectionWarehouse = em.merge(warehouseCollectionWarehouse);
                if (oldLocationIdOfWarehouseCollectionWarehouse != null) {
                    oldLocationIdOfWarehouseCollectionWarehouse.getWarehouseCollection().remove(warehouseCollectionWarehouse);
                    oldLocationIdOfWarehouseCollectionWarehouse = em.merge(oldLocationIdOfWarehouseCollectionWarehouse);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Location location) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Location persistentLocation = em.find(Location.class, location.getId());
            Collection<Warehouse> warehouseCollectionOld = persistentLocation.getWarehouseCollection();
            Collection<Warehouse> warehouseCollectionNew = location.getWarehouseCollection();
            List<String> illegalOrphanMessages = null;
            for (Warehouse warehouseCollectionOldWarehouse : warehouseCollectionOld) {
                if (!warehouseCollectionNew.contains(warehouseCollectionOldWarehouse)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Warehouse " + warehouseCollectionOldWarehouse + " since its locationId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Warehouse> attachedWarehouseCollectionNew = new ArrayList<Warehouse>();
            for (Warehouse warehouseCollectionNewWarehouseToAttach : warehouseCollectionNew) {
                warehouseCollectionNewWarehouseToAttach = em.getReference(warehouseCollectionNewWarehouseToAttach.getClass(), warehouseCollectionNewWarehouseToAttach.getId());
                attachedWarehouseCollectionNew.add(warehouseCollectionNewWarehouseToAttach);
            }
            warehouseCollectionNew = attachedWarehouseCollectionNew;
            location.setWarehouseCollection(warehouseCollectionNew);
            location = em.merge(location);
            for (Warehouse warehouseCollectionNewWarehouse : warehouseCollectionNew) {
                if (!warehouseCollectionOld.contains(warehouseCollectionNewWarehouse)) {
                    Location oldLocationIdOfWarehouseCollectionNewWarehouse = warehouseCollectionNewWarehouse.getLocationId();
                    warehouseCollectionNewWarehouse.setLocationId(location);
                    warehouseCollectionNewWarehouse = em.merge(warehouseCollectionNewWarehouse);
                    if (oldLocationIdOfWarehouseCollectionNewWarehouse != null && !oldLocationIdOfWarehouseCollectionNewWarehouse.equals(location)) {
                        oldLocationIdOfWarehouseCollectionNewWarehouse.getWarehouseCollection().remove(warehouseCollectionNewWarehouse);
                        oldLocationIdOfWarehouseCollectionNewWarehouse = em.merge(oldLocationIdOfWarehouseCollectionNewWarehouse);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = location.getId();
                if (findLocation(id) == null) {
                    throw new NonexistentEntityException("The location with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Location location;
            try {
                location = em.getReference(Location.class, id);
                location.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The location with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Warehouse> warehouseCollectionOrphanCheck = location.getWarehouseCollection();
            for (Warehouse warehouseCollectionOrphanCheckWarehouse : warehouseCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Location (" + location + ") cannot be destroyed since the Warehouse " + warehouseCollectionOrphanCheckWarehouse + " in its warehouseCollection field has a non-nullable locationId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(location);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Location> findLocationEntities() {
        return findLocationEntities(true, -1, -1);
    }

    public List<Location> findLocationEntities(int maxResults, int firstResult) {
        return findLocationEntities(false, maxResults, firstResult);
    }

    private List<Location> findLocationEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Location.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Location findLocation(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Location.class, id);
        } finally {
            em.close();
        }
    }

    public int getLocationCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Location> rt = cq.from(Location.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
