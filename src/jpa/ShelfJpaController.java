/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class ShelfJpaController implements Serializable {

    public ShelfJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Shelf shelf) {
        if (shelf.getProductCollection() == null) {
            shelf.setProductCollection(new ArrayList<Product>());
        }
        if (shelf.getShelfDetailsCollection() == null) {
            shelf.setShelfDetailsCollection(new ArrayList<ShelfDetails>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sector sectorId = shelf.getSectorId();
            if (sectorId != null) {
                sectorId = em.getReference(sectorId.getClass(), sectorId.getId());
                shelf.setSectorId(sectorId);
            }
            Collection<Product> attachedProductCollection = new ArrayList<Product>();
            for (Product productCollectionProductToAttach : shelf.getProductCollection()) {
                productCollectionProductToAttach = em.getReference(productCollectionProductToAttach.getClass(), productCollectionProductToAttach.getId());
                attachedProductCollection.add(productCollectionProductToAttach);
            }
            shelf.setProductCollection(attachedProductCollection);
            Collection<ShelfDetails> attachedShelfDetailsCollection = new ArrayList<ShelfDetails>();
            for (ShelfDetails shelfDetailsCollectionShelfDetailsToAttach : shelf.getShelfDetailsCollection()) {
                shelfDetailsCollectionShelfDetailsToAttach = em.getReference(shelfDetailsCollectionShelfDetailsToAttach.getClass(), shelfDetailsCollectionShelfDetailsToAttach.getId());
                attachedShelfDetailsCollection.add(shelfDetailsCollectionShelfDetailsToAttach);
            }
            shelf.setShelfDetailsCollection(attachedShelfDetailsCollection);
            em.persist(shelf);
            if (sectorId != null) {
                sectorId.getShelfCollection().add(shelf);
                sectorId = em.merge(sectorId);
            }
            for (Product productCollectionProduct : shelf.getProductCollection()) {
                Shelf oldShelfIdOfProductCollectionProduct = productCollectionProduct.getShelfId();
                productCollectionProduct.setShelfId(shelf);
                productCollectionProduct = em.merge(productCollectionProduct);
                if (oldShelfIdOfProductCollectionProduct != null) {
                    oldShelfIdOfProductCollectionProduct.getProductCollection().remove(productCollectionProduct);
                    oldShelfIdOfProductCollectionProduct = em.merge(oldShelfIdOfProductCollectionProduct);
                }
            }
            for (ShelfDetails shelfDetailsCollectionShelfDetails : shelf.getShelfDetailsCollection()) {
                Shelf oldShelfIdOfShelfDetailsCollectionShelfDetails = shelfDetailsCollectionShelfDetails.getShelfId();
                shelfDetailsCollectionShelfDetails.setShelfId(shelf);
                shelfDetailsCollectionShelfDetails = em.merge(shelfDetailsCollectionShelfDetails);
                if (oldShelfIdOfShelfDetailsCollectionShelfDetails != null) {
                    oldShelfIdOfShelfDetailsCollectionShelfDetails.getShelfDetailsCollection().remove(shelfDetailsCollectionShelfDetails);
                    oldShelfIdOfShelfDetailsCollectionShelfDetails = em.merge(oldShelfIdOfShelfDetailsCollectionShelfDetails);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Shelf shelf) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Shelf persistentShelf = em.find(Shelf.class, shelf.getId());
            Sector sectorIdOld = persistentShelf.getSectorId();
            Sector sectorIdNew = shelf.getSectorId();
            Collection<Product> productCollectionOld = persistentShelf.getProductCollection();
            Collection<Product> productCollectionNew = shelf.getProductCollection();
            Collection<ShelfDetails> shelfDetailsCollectionOld = persistentShelf.getShelfDetailsCollection();
            Collection<ShelfDetails> shelfDetailsCollectionNew = shelf.getShelfDetailsCollection();
            List<String> illegalOrphanMessages = null;
            for (ShelfDetails shelfDetailsCollectionOldShelfDetails : shelfDetailsCollectionOld) {
                if (!shelfDetailsCollectionNew.contains(shelfDetailsCollectionOldShelfDetails)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ShelfDetails " + shelfDetailsCollectionOldShelfDetails + " since its shelfId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (sectorIdNew != null) {
                sectorIdNew = em.getReference(sectorIdNew.getClass(), sectorIdNew.getId());
                shelf.setSectorId(sectorIdNew);
            }
            Collection<Product> attachedProductCollectionNew = new ArrayList<Product>();
            for (Product productCollectionNewProductToAttach : productCollectionNew) {
                productCollectionNewProductToAttach = em.getReference(productCollectionNewProductToAttach.getClass(), productCollectionNewProductToAttach.getId());
                attachedProductCollectionNew.add(productCollectionNewProductToAttach);
            }
            productCollectionNew = attachedProductCollectionNew;
            shelf.setProductCollection(productCollectionNew);
            Collection<ShelfDetails> attachedShelfDetailsCollectionNew = new ArrayList<ShelfDetails>();
            for (ShelfDetails shelfDetailsCollectionNewShelfDetailsToAttach : shelfDetailsCollectionNew) {
                shelfDetailsCollectionNewShelfDetailsToAttach = em.getReference(shelfDetailsCollectionNewShelfDetailsToAttach.getClass(), shelfDetailsCollectionNewShelfDetailsToAttach.getId());
                attachedShelfDetailsCollectionNew.add(shelfDetailsCollectionNewShelfDetailsToAttach);
            }
            shelfDetailsCollectionNew = attachedShelfDetailsCollectionNew;
            shelf.setShelfDetailsCollection(shelfDetailsCollectionNew);
            shelf = em.merge(shelf);
            if (sectorIdOld != null && !sectorIdOld.equals(sectorIdNew)) {
                sectorIdOld.getShelfCollection().remove(shelf);
                sectorIdOld = em.merge(sectorIdOld);
            }
            if (sectorIdNew != null && !sectorIdNew.equals(sectorIdOld)) {
                sectorIdNew.getShelfCollection().add(shelf);
                sectorIdNew = em.merge(sectorIdNew);
            }
            for (Product productCollectionOldProduct : productCollectionOld) {
                if (!productCollectionNew.contains(productCollectionOldProduct)) {
                    productCollectionOldProduct.setShelfId(null);
                    productCollectionOldProduct = em.merge(productCollectionOldProduct);
                }
            }
            for (Product productCollectionNewProduct : productCollectionNew) {
                if (!productCollectionOld.contains(productCollectionNewProduct)) {
                    Shelf oldShelfIdOfProductCollectionNewProduct = productCollectionNewProduct.getShelfId();
                    productCollectionNewProduct.setShelfId(shelf);
                    productCollectionNewProduct = em.merge(productCollectionNewProduct);
                    if (oldShelfIdOfProductCollectionNewProduct != null && !oldShelfIdOfProductCollectionNewProduct.equals(shelf)) {
                        oldShelfIdOfProductCollectionNewProduct.getProductCollection().remove(productCollectionNewProduct);
                        oldShelfIdOfProductCollectionNewProduct = em.merge(oldShelfIdOfProductCollectionNewProduct);
                    }
                }
            }
            for (ShelfDetails shelfDetailsCollectionNewShelfDetails : shelfDetailsCollectionNew) {
                if (!shelfDetailsCollectionOld.contains(shelfDetailsCollectionNewShelfDetails)) {
                    Shelf oldShelfIdOfShelfDetailsCollectionNewShelfDetails = shelfDetailsCollectionNewShelfDetails.getShelfId();
                    shelfDetailsCollectionNewShelfDetails.setShelfId(shelf);
                    shelfDetailsCollectionNewShelfDetails = em.merge(shelfDetailsCollectionNewShelfDetails);
                    if (oldShelfIdOfShelfDetailsCollectionNewShelfDetails != null && !oldShelfIdOfShelfDetailsCollectionNewShelfDetails.equals(shelf)) {
                        oldShelfIdOfShelfDetailsCollectionNewShelfDetails.getShelfDetailsCollection().remove(shelfDetailsCollectionNewShelfDetails);
                        oldShelfIdOfShelfDetailsCollectionNewShelfDetails = em.merge(oldShelfIdOfShelfDetailsCollectionNewShelfDetails);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = shelf.getId();
                if (findShelf(id) == null) {
                    throw new NonexistentEntityException("The shelf with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Shelf shelf;
            try {
                shelf = em.getReference(Shelf.class, id);
                shelf.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The shelf with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<ShelfDetails> shelfDetailsCollectionOrphanCheck = shelf.getShelfDetailsCollection();
            for (ShelfDetails shelfDetailsCollectionOrphanCheckShelfDetails : shelfDetailsCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Shelf (" + shelf + ") cannot be destroyed since the ShelfDetails " + shelfDetailsCollectionOrphanCheckShelfDetails + " in its shelfDetailsCollection field has a non-nullable shelfId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Sector sectorId = shelf.getSectorId();
            if (sectorId != null) {
                sectorId.getShelfCollection().remove(shelf);
                sectorId = em.merge(sectorId);
            }
            Collection<Product> productCollection = shelf.getProductCollection();
            for (Product productCollectionProduct : productCollection) {
                productCollectionProduct.setShelfId(null);
                productCollectionProduct = em.merge(productCollectionProduct);
            }
            em.remove(shelf);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Shelf> findShelfEntities() {
        return findShelfEntities(true, -1, -1);
    }

    public List<Shelf> findShelfEntities(int maxResults, int firstResult) {
        return findShelfEntities(false, maxResults, firstResult);
    }

    private List<Shelf> findShelfEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Shelf.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Shelf findShelf(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Shelf.class, id);
        } finally {
            em.close();
        }
    }

    public int getShelfCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Shelf> rt = cq.from(Shelf.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
