/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class CategoryDetailsJpaController implements Serializable {

    public CategoryDetailsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CategoryDetails categoryDetails) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Category categoryId = categoryDetails.getCategoryId();
            if (categoryId != null) {
                categoryId = em.getReference(categoryId.getClass(), categoryId.getId());
                categoryDetails.setCategoryId(categoryId);
            }
            em.persist(categoryDetails);
            if (categoryId != null) {
                categoryId.getCategoryDetailsCollection().add(categoryDetails);
                categoryId = em.merge(categoryId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CategoryDetails categoryDetails) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CategoryDetails persistentCategoryDetails = em.find(CategoryDetails.class, categoryDetails.getId());
            Category categoryIdOld = persistentCategoryDetails.getCategoryId();
            Category categoryIdNew = categoryDetails.getCategoryId();
            if (categoryIdNew != null) {
                categoryIdNew = em.getReference(categoryIdNew.getClass(), categoryIdNew.getId());
                categoryDetails.setCategoryId(categoryIdNew);
            }
            categoryDetails = em.merge(categoryDetails);
            if (categoryIdOld != null && !categoryIdOld.equals(categoryIdNew)) {
                categoryIdOld.getCategoryDetailsCollection().remove(categoryDetails);
                categoryIdOld = em.merge(categoryIdOld);
            }
            if (categoryIdNew != null && !categoryIdNew.equals(categoryIdOld)) {
                categoryIdNew.getCategoryDetailsCollection().add(categoryDetails);
                categoryIdNew = em.merge(categoryIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = categoryDetails.getId();
                if (findCategoryDetails(id) == null) {
                    throw new NonexistentEntityException("The categoryDetails with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CategoryDetails categoryDetails;
            try {
                categoryDetails = em.getReference(CategoryDetails.class, id);
                categoryDetails.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The categoryDetails with id " + id + " no longer exists.", enfe);
            }
            Category categoryId = categoryDetails.getCategoryId();
            if (categoryId != null) {
                categoryId.getCategoryDetailsCollection().remove(categoryDetails);
                categoryId = em.merge(categoryId);
            }
            em.remove(categoryDetails);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CategoryDetails> findCategoryDetailsEntities() {
        return findCategoryDetailsEntities(true, -1, -1);
    }

    public List<CategoryDetails> findCategoryDetailsEntities(int maxResults, int firstResult) {
        return findCategoryDetailsEntities(false, maxResults, firstResult);
    }

    private List<CategoryDetails> findCategoryDetailsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CategoryDetails.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CategoryDetails findCategoryDetails(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CategoryDetails.class, id);
        } finally {
            em.close();
        }
    }

    public int getCategoryDetailsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CategoryDetails> rt = cq.from(CategoryDetails.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
