/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class ProductJpaController implements Serializable {

    public ProductJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Product product) {
        if (product.getProductUnitCollection() == null) {
            product.setProductUnitCollection(new ArrayList<ProductUnit>());
        }
        if (product.getSalesCollection() == null) {
            product.setSalesCollection(new ArrayList<Sales>());
        }
        if (product.getProductDetailsCollection() == null) {
            product.setProductDetailsCollection(new ArrayList<ProductDetails>());
        }
        if (product.getStockCollection() == null) {
            product.setStockCollection(new ArrayList<Stock>());
        }
        if (product.getAcquisitionsCollection() == null) {
            product.setAcquisitionsCollection(new ArrayList<Acquisitions>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Category categoryId = product.getCategoryId();
            if (categoryId != null) {
                categoryId = em.getReference(categoryId.getClass(), categoryId.getId());
                product.setCategoryId(categoryId);
            }
            Shelf shelfId = product.getShelfId();
            if (shelfId != null) {
                shelfId = em.getReference(shelfId.getClass(), shelfId.getId());
                product.setShelfId(shelfId);
            }
            Collection<ProductUnit> attachedProductUnitCollection = new ArrayList<ProductUnit>();
            for (ProductUnit productUnitCollectionProductUnitToAttach : product.getProductUnitCollection()) {
                productUnitCollectionProductUnitToAttach = em.getReference(productUnitCollectionProductUnitToAttach.getClass(), productUnitCollectionProductUnitToAttach.getId());
                attachedProductUnitCollection.add(productUnitCollectionProductUnitToAttach);
            }
            product.setProductUnitCollection(attachedProductUnitCollection);
            Collection<Sales> attachedSalesCollection = new ArrayList<Sales>();
            for (Sales salesCollectionSalesToAttach : product.getSalesCollection()) {
                salesCollectionSalesToAttach = em.getReference(salesCollectionSalesToAttach.getClass(), salesCollectionSalesToAttach.getId());
                attachedSalesCollection.add(salesCollectionSalesToAttach);
            }
            product.setSalesCollection(attachedSalesCollection);
            Collection<ProductDetails> attachedProductDetailsCollection = new ArrayList<ProductDetails>();
            for (ProductDetails productDetailsCollectionProductDetailsToAttach : product.getProductDetailsCollection()) {
                productDetailsCollectionProductDetailsToAttach = em.getReference(productDetailsCollectionProductDetailsToAttach.getClass(), productDetailsCollectionProductDetailsToAttach.getId());
                attachedProductDetailsCollection.add(productDetailsCollectionProductDetailsToAttach);
            }
            product.setProductDetailsCollection(attachedProductDetailsCollection);
            Collection<Stock> attachedStockCollection = new ArrayList<Stock>();
            for (Stock stockCollectionStockToAttach : product.getStockCollection()) {
                stockCollectionStockToAttach = em.getReference(stockCollectionStockToAttach.getClass(), stockCollectionStockToAttach.getId());
                attachedStockCollection.add(stockCollectionStockToAttach);
            }
            product.setStockCollection(attachedStockCollection);
            Collection<Acquisitions> attachedAcquisitionsCollection = new ArrayList<Acquisitions>();
            for (Acquisitions acquisitionsCollectionAcquisitionsToAttach : product.getAcquisitionsCollection()) {
                acquisitionsCollectionAcquisitionsToAttach = em.getReference(acquisitionsCollectionAcquisitionsToAttach.getClass(), acquisitionsCollectionAcquisitionsToAttach.getId());
                attachedAcquisitionsCollection.add(acquisitionsCollectionAcquisitionsToAttach);
            }
            product.setAcquisitionsCollection(attachedAcquisitionsCollection);
            em.persist(product);
            if (categoryId != null) {
                categoryId.getProductCollection().add(product);
                categoryId = em.merge(categoryId);
            }
            if (shelfId != null) {
                shelfId.getProductCollection().add(product);
                shelfId = em.merge(shelfId);
            }
            for (ProductUnit productUnitCollectionProductUnit : product.getProductUnitCollection()) {
                Product oldProductIdOfProductUnitCollectionProductUnit = productUnitCollectionProductUnit.getProductId();
                productUnitCollectionProductUnit.setProductId(product);
                productUnitCollectionProductUnit = em.merge(productUnitCollectionProductUnit);
                if (oldProductIdOfProductUnitCollectionProductUnit != null) {
                    oldProductIdOfProductUnitCollectionProductUnit.getProductUnitCollection().remove(productUnitCollectionProductUnit);
                    oldProductIdOfProductUnitCollectionProductUnit = em.merge(oldProductIdOfProductUnitCollectionProductUnit);
                }
            }
            for (Sales salesCollectionSales : product.getSalesCollection()) {
                Product oldProductIdOfSalesCollectionSales = salesCollectionSales.getProductId();
                salesCollectionSales.setProductId(product);
                salesCollectionSales = em.merge(salesCollectionSales);
                if (oldProductIdOfSalesCollectionSales != null) {
                    oldProductIdOfSalesCollectionSales.getSalesCollection().remove(salesCollectionSales);
                    oldProductIdOfSalesCollectionSales = em.merge(oldProductIdOfSalesCollectionSales);
                }
            }
            for (ProductDetails productDetailsCollectionProductDetails : product.getProductDetailsCollection()) {
                Product oldProductIdOfProductDetailsCollectionProductDetails = productDetailsCollectionProductDetails.getProductId();
                productDetailsCollectionProductDetails.setProductId(product);
                productDetailsCollectionProductDetails = em.merge(productDetailsCollectionProductDetails);
                if (oldProductIdOfProductDetailsCollectionProductDetails != null) {
                    oldProductIdOfProductDetailsCollectionProductDetails.getProductDetailsCollection().remove(productDetailsCollectionProductDetails);
                    oldProductIdOfProductDetailsCollectionProductDetails = em.merge(oldProductIdOfProductDetailsCollectionProductDetails);
                }
            }
            for (Stock stockCollectionStock : product.getStockCollection()) {
                Product oldProductIdOfStockCollectionStock = stockCollectionStock.getProductId();
                stockCollectionStock.setProductId(product);
                stockCollectionStock = em.merge(stockCollectionStock);
                if (oldProductIdOfStockCollectionStock != null) {
                    oldProductIdOfStockCollectionStock.getStockCollection().remove(stockCollectionStock);
                    oldProductIdOfStockCollectionStock = em.merge(oldProductIdOfStockCollectionStock);
                }
            }
            for (Acquisitions acquisitionsCollectionAcquisitions : product.getAcquisitionsCollection()) {
                Product oldProductIdOfAcquisitionsCollectionAcquisitions = acquisitionsCollectionAcquisitions.getProductId();
                acquisitionsCollectionAcquisitions.setProductId(product);
                acquisitionsCollectionAcquisitions = em.merge(acquisitionsCollectionAcquisitions);
                if (oldProductIdOfAcquisitionsCollectionAcquisitions != null) {
                    oldProductIdOfAcquisitionsCollectionAcquisitions.getAcquisitionsCollection().remove(acquisitionsCollectionAcquisitions);
                    oldProductIdOfAcquisitionsCollectionAcquisitions = em.merge(oldProductIdOfAcquisitionsCollectionAcquisitions);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Product product) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Product persistentProduct = em.find(Product.class, product.getId());
            Category categoryIdOld = persistentProduct.getCategoryId();
            Category categoryIdNew = product.getCategoryId();
            Shelf shelfIdOld = persistentProduct.getShelfId();
            Shelf shelfIdNew = product.getShelfId();
            Collection<ProductUnit> productUnitCollectionOld = persistentProduct.getProductUnitCollection();
            Collection<ProductUnit> productUnitCollectionNew = product.getProductUnitCollection();
            Collection<Sales> salesCollectionOld = persistentProduct.getSalesCollection();
            Collection<Sales> salesCollectionNew = product.getSalesCollection();
            Collection<ProductDetails> productDetailsCollectionOld = persistentProduct.getProductDetailsCollection();
            Collection<ProductDetails> productDetailsCollectionNew = product.getProductDetailsCollection();
            Collection<Stock> stockCollectionOld = persistentProduct.getStockCollection();
            Collection<Stock> stockCollectionNew = product.getStockCollection();
            Collection<Acquisitions> acquisitionsCollectionOld = persistentProduct.getAcquisitionsCollection();
            Collection<Acquisitions> acquisitionsCollectionNew = product.getAcquisitionsCollection();
            List<String> illegalOrphanMessages = null;
            for (ProductUnit productUnitCollectionOldProductUnit : productUnitCollectionOld) {
                if (!productUnitCollectionNew.contains(productUnitCollectionOldProductUnit)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ProductUnit " + productUnitCollectionOldProductUnit + " since its productId field is not nullable.");
                }
            }
            for (Sales salesCollectionOldSales : salesCollectionOld) {
                if (!salesCollectionNew.contains(salesCollectionOldSales)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Sales " + salesCollectionOldSales + " since its productId field is not nullable.");
                }
            }
            for (ProductDetails productDetailsCollectionOldProductDetails : productDetailsCollectionOld) {
                if (!productDetailsCollectionNew.contains(productDetailsCollectionOldProductDetails)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ProductDetails " + productDetailsCollectionOldProductDetails + " since its productId field is not nullable.");
                }
            }
            for (Stock stockCollectionOldStock : stockCollectionOld) {
                if (!stockCollectionNew.contains(stockCollectionOldStock)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Stock " + stockCollectionOldStock + " since its productId field is not nullable.");
                }
            }
            for (Acquisitions acquisitionsCollectionOldAcquisitions : acquisitionsCollectionOld) {
                if (!acquisitionsCollectionNew.contains(acquisitionsCollectionOldAcquisitions)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Acquisitions " + acquisitionsCollectionOldAcquisitions + " since its productId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (categoryIdNew != null) {
                categoryIdNew = em.getReference(categoryIdNew.getClass(), categoryIdNew.getId());
                product.setCategoryId(categoryIdNew);
            }
            if (shelfIdNew != null) {
                shelfIdNew = em.getReference(shelfIdNew.getClass(), shelfIdNew.getId());
                product.setShelfId(shelfIdNew);
            }
            Collection<ProductUnit> attachedProductUnitCollectionNew = new ArrayList<ProductUnit>();
            for (ProductUnit productUnitCollectionNewProductUnitToAttach : productUnitCollectionNew) {
                productUnitCollectionNewProductUnitToAttach = em.getReference(productUnitCollectionNewProductUnitToAttach.getClass(), productUnitCollectionNewProductUnitToAttach.getId());
                attachedProductUnitCollectionNew.add(productUnitCollectionNewProductUnitToAttach);
            }
            productUnitCollectionNew = attachedProductUnitCollectionNew;
            product.setProductUnitCollection(productUnitCollectionNew);
            Collection<Sales> attachedSalesCollectionNew = new ArrayList<Sales>();
            for (Sales salesCollectionNewSalesToAttach : salesCollectionNew) {
                salesCollectionNewSalesToAttach = em.getReference(salesCollectionNewSalesToAttach.getClass(), salesCollectionNewSalesToAttach.getId());
                attachedSalesCollectionNew.add(salesCollectionNewSalesToAttach);
            }
            salesCollectionNew = attachedSalesCollectionNew;
            product.setSalesCollection(salesCollectionNew);
            Collection<ProductDetails> attachedProductDetailsCollectionNew = new ArrayList<ProductDetails>();
            for (ProductDetails productDetailsCollectionNewProductDetailsToAttach : productDetailsCollectionNew) {
                productDetailsCollectionNewProductDetailsToAttach = em.getReference(productDetailsCollectionNewProductDetailsToAttach.getClass(), productDetailsCollectionNewProductDetailsToAttach.getId());
                attachedProductDetailsCollectionNew.add(productDetailsCollectionNewProductDetailsToAttach);
            }
            productDetailsCollectionNew = attachedProductDetailsCollectionNew;
            product.setProductDetailsCollection(productDetailsCollectionNew);
            Collection<Stock> attachedStockCollectionNew = new ArrayList<Stock>();
            for (Stock stockCollectionNewStockToAttach : stockCollectionNew) {
                stockCollectionNewStockToAttach = em.getReference(stockCollectionNewStockToAttach.getClass(), stockCollectionNewStockToAttach.getId());
                attachedStockCollectionNew.add(stockCollectionNewStockToAttach);
            }
            stockCollectionNew = attachedStockCollectionNew;
            product.setStockCollection(stockCollectionNew);
            Collection<Acquisitions> attachedAcquisitionsCollectionNew = new ArrayList<Acquisitions>();
            for (Acquisitions acquisitionsCollectionNewAcquisitionsToAttach : acquisitionsCollectionNew) {
                acquisitionsCollectionNewAcquisitionsToAttach = em.getReference(acquisitionsCollectionNewAcquisitionsToAttach.getClass(), acquisitionsCollectionNewAcquisitionsToAttach.getId());
                attachedAcquisitionsCollectionNew.add(acquisitionsCollectionNewAcquisitionsToAttach);
            }
            acquisitionsCollectionNew = attachedAcquisitionsCollectionNew;
            product.setAcquisitionsCollection(acquisitionsCollectionNew);
            product = em.merge(product);
            if (categoryIdOld != null && !categoryIdOld.equals(categoryIdNew)) {
                categoryIdOld.getProductCollection().remove(product);
                categoryIdOld = em.merge(categoryIdOld);
            }
            if (categoryIdNew != null && !categoryIdNew.equals(categoryIdOld)) {
                categoryIdNew.getProductCollection().add(product);
                categoryIdNew = em.merge(categoryIdNew);
            }
            if (shelfIdOld != null && !shelfIdOld.equals(shelfIdNew)) {
                shelfIdOld.getProductCollection().remove(product);
                shelfIdOld = em.merge(shelfIdOld);
            }
            if (shelfIdNew != null && !shelfIdNew.equals(shelfIdOld)) {
                shelfIdNew.getProductCollection().add(product);
                shelfIdNew = em.merge(shelfIdNew);
            }
            for (ProductUnit productUnitCollectionNewProductUnit : productUnitCollectionNew) {
                if (!productUnitCollectionOld.contains(productUnitCollectionNewProductUnit)) {
                    Product oldProductIdOfProductUnitCollectionNewProductUnit = productUnitCollectionNewProductUnit.getProductId();
                    productUnitCollectionNewProductUnit.setProductId(product);
                    productUnitCollectionNewProductUnit = em.merge(productUnitCollectionNewProductUnit);
                    if (oldProductIdOfProductUnitCollectionNewProductUnit != null && !oldProductIdOfProductUnitCollectionNewProductUnit.equals(product)) {
                        oldProductIdOfProductUnitCollectionNewProductUnit.getProductUnitCollection().remove(productUnitCollectionNewProductUnit);
                        oldProductIdOfProductUnitCollectionNewProductUnit = em.merge(oldProductIdOfProductUnitCollectionNewProductUnit);
                    }
                }
            }
            for (Sales salesCollectionNewSales : salesCollectionNew) {
                if (!salesCollectionOld.contains(salesCollectionNewSales)) {
                    Product oldProductIdOfSalesCollectionNewSales = salesCollectionNewSales.getProductId();
                    salesCollectionNewSales.setProductId(product);
                    salesCollectionNewSales = em.merge(salesCollectionNewSales);
                    if (oldProductIdOfSalesCollectionNewSales != null && !oldProductIdOfSalesCollectionNewSales.equals(product)) {
                        oldProductIdOfSalesCollectionNewSales.getSalesCollection().remove(salesCollectionNewSales);
                        oldProductIdOfSalesCollectionNewSales = em.merge(oldProductIdOfSalesCollectionNewSales);
                    }
                }
            }
            for (ProductDetails productDetailsCollectionNewProductDetails : productDetailsCollectionNew) {
                if (!productDetailsCollectionOld.contains(productDetailsCollectionNewProductDetails)) {
                    Product oldProductIdOfProductDetailsCollectionNewProductDetails = productDetailsCollectionNewProductDetails.getProductId();
                    productDetailsCollectionNewProductDetails.setProductId(product);
                    productDetailsCollectionNewProductDetails = em.merge(productDetailsCollectionNewProductDetails);
                    if (oldProductIdOfProductDetailsCollectionNewProductDetails != null && !oldProductIdOfProductDetailsCollectionNewProductDetails.equals(product)) {
                        oldProductIdOfProductDetailsCollectionNewProductDetails.getProductDetailsCollection().remove(productDetailsCollectionNewProductDetails);
                        oldProductIdOfProductDetailsCollectionNewProductDetails = em.merge(oldProductIdOfProductDetailsCollectionNewProductDetails);
                    }
                }
            }
            for (Stock stockCollectionNewStock : stockCollectionNew) {
                if (!stockCollectionOld.contains(stockCollectionNewStock)) {
                    Product oldProductIdOfStockCollectionNewStock = stockCollectionNewStock.getProductId();
                    stockCollectionNewStock.setProductId(product);
                    stockCollectionNewStock = em.merge(stockCollectionNewStock);
                    if (oldProductIdOfStockCollectionNewStock != null && !oldProductIdOfStockCollectionNewStock.equals(product)) {
                        oldProductIdOfStockCollectionNewStock.getStockCollection().remove(stockCollectionNewStock);
                        oldProductIdOfStockCollectionNewStock = em.merge(oldProductIdOfStockCollectionNewStock);
                    }
                }
            }
            for (Acquisitions acquisitionsCollectionNewAcquisitions : acquisitionsCollectionNew) {
                if (!acquisitionsCollectionOld.contains(acquisitionsCollectionNewAcquisitions)) {
                    Product oldProductIdOfAcquisitionsCollectionNewAcquisitions = acquisitionsCollectionNewAcquisitions.getProductId();
                    acquisitionsCollectionNewAcquisitions.setProductId(product);
                    acquisitionsCollectionNewAcquisitions = em.merge(acquisitionsCollectionNewAcquisitions);
                    if (oldProductIdOfAcquisitionsCollectionNewAcquisitions != null && !oldProductIdOfAcquisitionsCollectionNewAcquisitions.equals(product)) {
                        oldProductIdOfAcquisitionsCollectionNewAcquisitions.getAcquisitionsCollection().remove(acquisitionsCollectionNewAcquisitions);
                        oldProductIdOfAcquisitionsCollectionNewAcquisitions = em.merge(oldProductIdOfAcquisitionsCollectionNewAcquisitions);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = product.getId();
                if (findProduct(id) == null) {
                    throw new NonexistentEntityException("The product with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Product product;
            try {
                product = em.getReference(Product.class, id);
                product.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The product with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<ProductUnit> productUnitCollectionOrphanCheck = product.getProductUnitCollection();
            for (ProductUnit productUnitCollectionOrphanCheckProductUnit : productUnitCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Product (" + product + ") cannot be destroyed since the ProductUnit " + productUnitCollectionOrphanCheckProductUnit + " in its productUnitCollection field has a non-nullable productId field.");
            }
            Collection<Sales> salesCollectionOrphanCheck = product.getSalesCollection();
            for (Sales salesCollectionOrphanCheckSales : salesCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Product (" + product + ") cannot be destroyed since the Sales " + salesCollectionOrphanCheckSales + " in its salesCollection field has a non-nullable productId field.");
            }
            Collection<ProductDetails> productDetailsCollectionOrphanCheck = product.getProductDetailsCollection();
            for (ProductDetails productDetailsCollectionOrphanCheckProductDetails : productDetailsCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Product (" + product + ") cannot be destroyed since the ProductDetails " + productDetailsCollectionOrphanCheckProductDetails + " in its productDetailsCollection field has a non-nullable productId field.");
            }
            Collection<Stock> stockCollectionOrphanCheck = product.getStockCollection();
            for (Stock stockCollectionOrphanCheckStock : stockCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Product (" + product + ") cannot be destroyed since the Stock " + stockCollectionOrphanCheckStock + " in its stockCollection field has a non-nullable productId field.");
            }
            Collection<Acquisitions> acquisitionsCollectionOrphanCheck = product.getAcquisitionsCollection();
            for (Acquisitions acquisitionsCollectionOrphanCheckAcquisitions : acquisitionsCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Product (" + product + ") cannot be destroyed since the Acquisitions " + acquisitionsCollectionOrphanCheckAcquisitions + " in its acquisitionsCollection field has a non-nullable productId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Category categoryId = product.getCategoryId();
            if (categoryId != null) {
                categoryId.getProductCollection().remove(product);
                categoryId = em.merge(categoryId);
            }
            Shelf shelfId = product.getShelfId();
            if (shelfId != null) {
                shelfId.getProductCollection().remove(product);
                shelfId = em.merge(shelfId);
            }
            em.remove(product);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Product> findProductEntities() {
        return findProductEntities(true, -1, -1);
    }

    public List<Product> findProductEntities(int maxResults, int firstResult) {
        return findProductEntities(false, maxResults, firstResult);
    }

    private List<Product> findProductEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Product.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Product findProduct(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Product.class, id);
        } finally {
            em.close();
        }
    }

    public int getProductCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Product> rt = cq.from(Product.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
