/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class WarehouseDetailsJpaController implements Serializable {

    public WarehouseDetailsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(WarehouseDetails warehouseDetails) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Warehouse warehouseId = warehouseDetails.getWarehouseId();
            if (warehouseId != null) {
                warehouseId = em.getReference(warehouseId.getClass(), warehouseId.getId());
                warehouseDetails.setWarehouseId(warehouseId);
            }
            em.persist(warehouseDetails);
            if (warehouseId != null) {
                warehouseId.getWarehouseDetailsCollection().add(warehouseDetails);
                warehouseId = em.merge(warehouseId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(WarehouseDetails warehouseDetails) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            WarehouseDetails persistentWarehouseDetails = em.find(WarehouseDetails.class, warehouseDetails.getId());
            Warehouse warehouseIdOld = persistentWarehouseDetails.getWarehouseId();
            Warehouse warehouseIdNew = warehouseDetails.getWarehouseId();
            if (warehouseIdNew != null) {
                warehouseIdNew = em.getReference(warehouseIdNew.getClass(), warehouseIdNew.getId());
                warehouseDetails.setWarehouseId(warehouseIdNew);
            }
            warehouseDetails = em.merge(warehouseDetails);
            if (warehouseIdOld != null && !warehouseIdOld.equals(warehouseIdNew)) {
                warehouseIdOld.getWarehouseDetailsCollection().remove(warehouseDetails);
                warehouseIdOld = em.merge(warehouseIdOld);
            }
            if (warehouseIdNew != null && !warehouseIdNew.equals(warehouseIdOld)) {
                warehouseIdNew.getWarehouseDetailsCollection().add(warehouseDetails);
                warehouseIdNew = em.merge(warehouseIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = warehouseDetails.getId();
                if (findWarehouseDetails(id) == null) {
                    throw new NonexistentEntityException("The warehouseDetails with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            WarehouseDetails warehouseDetails;
            try {
                warehouseDetails = em.getReference(WarehouseDetails.class, id);
                warehouseDetails.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The warehouseDetails with id " + id + " no longer exists.", enfe);
            }
            Warehouse warehouseId = warehouseDetails.getWarehouseId();
            if (warehouseId != null) {
                warehouseId.getWarehouseDetailsCollection().remove(warehouseDetails);
                warehouseId = em.merge(warehouseId);
            }
            em.remove(warehouseDetails);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<WarehouseDetails> findWarehouseDetailsEntities() {
        return findWarehouseDetailsEntities(true, -1, -1);
    }

    public List<WarehouseDetails> findWarehouseDetailsEntities(int maxResults, int firstResult) {
        return findWarehouseDetailsEntities(false, maxResults, firstResult);
    }

    private List<WarehouseDetails> findWarehouseDetailsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(WarehouseDetails.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public WarehouseDetails findWarehouseDetails(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(WarehouseDetails.class, id);
        } finally {
            em.close();
        }
    }

    public int getWarehouseDetailsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<WarehouseDetails> rt = cq.from(WarehouseDetails.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
