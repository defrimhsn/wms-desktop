/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class SectorJpaController implements Serializable {

    public SectorJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Sector sector) {
        if (sector.getSectorDetailsCollection() == null) {
            sector.setSectorDetailsCollection(new ArrayList<SectorDetails>());
        }
        if (sector.getShelfCollection() == null) {
            sector.setShelfCollection(new ArrayList<Shelf>());
        }
        if (sector.getCategoryCollection() == null) {
            sector.setCategoryCollection(new ArrayList<Category>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Warehouse warehouseId = sector.getWarehouseId();
            if (warehouseId != null) {
                warehouseId = em.getReference(warehouseId.getClass(), warehouseId.getId());
                sector.setWarehouseId(warehouseId);
            }
            Collection<SectorDetails> attachedSectorDetailsCollection = new ArrayList<SectorDetails>();
            for (SectorDetails sectorDetailsCollectionSectorDetailsToAttach : sector.getSectorDetailsCollection()) {
                sectorDetailsCollectionSectorDetailsToAttach = em.getReference(sectorDetailsCollectionSectorDetailsToAttach.getClass(), sectorDetailsCollectionSectorDetailsToAttach.getId());
                attachedSectorDetailsCollection.add(sectorDetailsCollectionSectorDetailsToAttach);
            }
            sector.setSectorDetailsCollection(attachedSectorDetailsCollection);
            Collection<Shelf> attachedShelfCollection = new ArrayList<Shelf>();
            for (Shelf shelfCollectionShelfToAttach : sector.getShelfCollection()) {
                shelfCollectionShelfToAttach = em.getReference(shelfCollectionShelfToAttach.getClass(), shelfCollectionShelfToAttach.getId());
                attachedShelfCollection.add(shelfCollectionShelfToAttach);
            }
            sector.setShelfCollection(attachedShelfCollection);
            Collection<Category> attachedCategoryCollection = new ArrayList<Category>();
            for (Category categoryCollectionCategoryToAttach : sector.getCategoryCollection()) {
                categoryCollectionCategoryToAttach = em.getReference(categoryCollectionCategoryToAttach.getClass(), categoryCollectionCategoryToAttach.getId());
                attachedCategoryCollection.add(categoryCollectionCategoryToAttach);
            }
            sector.setCategoryCollection(attachedCategoryCollection);
            em.persist(sector);
            if (warehouseId != null) {
                warehouseId.getSectorCollection().add(sector);
                warehouseId = em.merge(warehouseId);
            }
            for (SectorDetails sectorDetailsCollectionSectorDetails : sector.getSectorDetailsCollection()) {
                Sector oldSectorIdOfSectorDetailsCollectionSectorDetails = sectorDetailsCollectionSectorDetails.getSectorId();
                sectorDetailsCollectionSectorDetails.setSectorId(sector);
                sectorDetailsCollectionSectorDetails = em.merge(sectorDetailsCollectionSectorDetails);
                if (oldSectorIdOfSectorDetailsCollectionSectorDetails != null) {
                    oldSectorIdOfSectorDetailsCollectionSectorDetails.getSectorDetailsCollection().remove(sectorDetailsCollectionSectorDetails);
                    oldSectorIdOfSectorDetailsCollectionSectorDetails = em.merge(oldSectorIdOfSectorDetailsCollectionSectorDetails);
                }
            }
            for (Shelf shelfCollectionShelf : sector.getShelfCollection()) {
                Sector oldSectorIdOfShelfCollectionShelf = shelfCollectionShelf.getSectorId();
                shelfCollectionShelf.setSectorId(sector);
                shelfCollectionShelf = em.merge(shelfCollectionShelf);
                if (oldSectorIdOfShelfCollectionShelf != null) {
                    oldSectorIdOfShelfCollectionShelf.getShelfCollection().remove(shelfCollectionShelf);
                    oldSectorIdOfShelfCollectionShelf = em.merge(oldSectorIdOfShelfCollectionShelf);
                }
            }
            for (Category categoryCollectionCategory : sector.getCategoryCollection()) {
                Sector oldSectorIdOfCategoryCollectionCategory = categoryCollectionCategory.getSectorId();
                categoryCollectionCategory.setSectorId(sector);
                categoryCollectionCategory = em.merge(categoryCollectionCategory);
                if (oldSectorIdOfCategoryCollectionCategory != null) {
                    oldSectorIdOfCategoryCollectionCategory.getCategoryCollection().remove(categoryCollectionCategory);
                    oldSectorIdOfCategoryCollectionCategory = em.merge(oldSectorIdOfCategoryCollectionCategory);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Sector sector) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sector persistentSector = em.find(Sector.class, sector.getId());
            Warehouse warehouseIdOld = persistentSector.getWarehouseId();
            Warehouse warehouseIdNew = sector.getWarehouseId();
            Collection<SectorDetails> sectorDetailsCollectionOld = persistentSector.getSectorDetailsCollection();
            Collection<SectorDetails> sectorDetailsCollectionNew = sector.getSectorDetailsCollection();
            Collection<Shelf> shelfCollectionOld = persistentSector.getShelfCollection();
            Collection<Shelf> shelfCollectionNew = sector.getShelfCollection();
            Collection<Category> categoryCollectionOld = persistentSector.getCategoryCollection();
            Collection<Category> categoryCollectionNew = sector.getCategoryCollection();
            List<String> illegalOrphanMessages = null;
            for (SectorDetails sectorDetailsCollectionOldSectorDetails : sectorDetailsCollectionOld) {
                if (!sectorDetailsCollectionNew.contains(sectorDetailsCollectionOldSectorDetails)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain SectorDetails " + sectorDetailsCollectionOldSectorDetails + " since its sectorId field is not nullable.");
                }
            }
            for (Shelf shelfCollectionOldShelf : shelfCollectionOld) {
                if (!shelfCollectionNew.contains(shelfCollectionOldShelf)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Shelf " + shelfCollectionOldShelf + " since its sectorId field is not nullable.");
                }
            }
            for (Category categoryCollectionOldCategory : categoryCollectionOld) {
                if (!categoryCollectionNew.contains(categoryCollectionOldCategory)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Category " + categoryCollectionOldCategory + " since its sectorId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (warehouseIdNew != null) {
                warehouseIdNew = em.getReference(warehouseIdNew.getClass(), warehouseIdNew.getId());
                sector.setWarehouseId(warehouseIdNew);
            }
            Collection<SectorDetails> attachedSectorDetailsCollectionNew = new ArrayList<SectorDetails>();
            for (SectorDetails sectorDetailsCollectionNewSectorDetailsToAttach : sectorDetailsCollectionNew) {
                sectorDetailsCollectionNewSectorDetailsToAttach = em.getReference(sectorDetailsCollectionNewSectorDetailsToAttach.getClass(), sectorDetailsCollectionNewSectorDetailsToAttach.getId());
                attachedSectorDetailsCollectionNew.add(sectorDetailsCollectionNewSectorDetailsToAttach);
            }
            sectorDetailsCollectionNew = attachedSectorDetailsCollectionNew;
            sector.setSectorDetailsCollection(sectorDetailsCollectionNew);
            Collection<Shelf> attachedShelfCollectionNew = new ArrayList<Shelf>();
            for (Shelf shelfCollectionNewShelfToAttach : shelfCollectionNew) {
                shelfCollectionNewShelfToAttach = em.getReference(shelfCollectionNewShelfToAttach.getClass(), shelfCollectionNewShelfToAttach.getId());
                attachedShelfCollectionNew.add(shelfCollectionNewShelfToAttach);
            }
            shelfCollectionNew = attachedShelfCollectionNew;
            sector.setShelfCollection(shelfCollectionNew);
            Collection<Category> attachedCategoryCollectionNew = new ArrayList<Category>();
            for (Category categoryCollectionNewCategoryToAttach : categoryCollectionNew) {
                categoryCollectionNewCategoryToAttach = em.getReference(categoryCollectionNewCategoryToAttach.getClass(), categoryCollectionNewCategoryToAttach.getId());
                attachedCategoryCollectionNew.add(categoryCollectionNewCategoryToAttach);
            }
            categoryCollectionNew = attachedCategoryCollectionNew;
            sector.setCategoryCollection(categoryCollectionNew);
            sector = em.merge(sector);
            if (warehouseIdOld != null && !warehouseIdOld.equals(warehouseIdNew)) {
                warehouseIdOld.getSectorCollection().remove(sector);
                warehouseIdOld = em.merge(warehouseIdOld);
            }
            if (warehouseIdNew != null && !warehouseIdNew.equals(warehouseIdOld)) {
                warehouseIdNew.getSectorCollection().add(sector);
                warehouseIdNew = em.merge(warehouseIdNew);
            }
            for (SectorDetails sectorDetailsCollectionNewSectorDetails : sectorDetailsCollectionNew) {
                if (!sectorDetailsCollectionOld.contains(sectorDetailsCollectionNewSectorDetails)) {
                    Sector oldSectorIdOfSectorDetailsCollectionNewSectorDetails = sectorDetailsCollectionNewSectorDetails.getSectorId();
                    sectorDetailsCollectionNewSectorDetails.setSectorId(sector);
                    sectorDetailsCollectionNewSectorDetails = em.merge(sectorDetailsCollectionNewSectorDetails);
                    if (oldSectorIdOfSectorDetailsCollectionNewSectorDetails != null && !oldSectorIdOfSectorDetailsCollectionNewSectorDetails.equals(sector)) {
                        oldSectorIdOfSectorDetailsCollectionNewSectorDetails.getSectorDetailsCollection().remove(sectorDetailsCollectionNewSectorDetails);
                        oldSectorIdOfSectorDetailsCollectionNewSectorDetails = em.merge(oldSectorIdOfSectorDetailsCollectionNewSectorDetails);
                    }
                }
            }
            for (Shelf shelfCollectionNewShelf : shelfCollectionNew) {
                if (!shelfCollectionOld.contains(shelfCollectionNewShelf)) {
                    Sector oldSectorIdOfShelfCollectionNewShelf = shelfCollectionNewShelf.getSectorId();
                    shelfCollectionNewShelf.setSectorId(sector);
                    shelfCollectionNewShelf = em.merge(shelfCollectionNewShelf);
                    if (oldSectorIdOfShelfCollectionNewShelf != null && !oldSectorIdOfShelfCollectionNewShelf.equals(sector)) {
                        oldSectorIdOfShelfCollectionNewShelf.getShelfCollection().remove(shelfCollectionNewShelf);
                        oldSectorIdOfShelfCollectionNewShelf = em.merge(oldSectorIdOfShelfCollectionNewShelf);
                    }
                }
            }
            for (Category categoryCollectionNewCategory : categoryCollectionNew) {
                if (!categoryCollectionOld.contains(categoryCollectionNewCategory)) {
                    Sector oldSectorIdOfCategoryCollectionNewCategory = categoryCollectionNewCategory.getSectorId();
                    categoryCollectionNewCategory.setSectorId(sector);
                    categoryCollectionNewCategory = em.merge(categoryCollectionNewCategory);
                    if (oldSectorIdOfCategoryCollectionNewCategory != null && !oldSectorIdOfCategoryCollectionNewCategory.equals(sector)) {
                        oldSectorIdOfCategoryCollectionNewCategory.getCategoryCollection().remove(categoryCollectionNewCategory);
                        oldSectorIdOfCategoryCollectionNewCategory = em.merge(oldSectorIdOfCategoryCollectionNewCategory);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = sector.getId();
                if (findSector(id) == null) {
                    throw new NonexistentEntityException("The sector with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sector sector;
            try {
                sector = em.getReference(Sector.class, id);
                sector.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sector with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<SectorDetails> sectorDetailsCollectionOrphanCheck = sector.getSectorDetailsCollection();
            for (SectorDetails sectorDetailsCollectionOrphanCheckSectorDetails : sectorDetailsCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sector (" + sector + ") cannot be destroyed since the SectorDetails " + sectorDetailsCollectionOrphanCheckSectorDetails + " in its sectorDetailsCollection field has a non-nullable sectorId field.");
            }
            Collection<Shelf> shelfCollectionOrphanCheck = sector.getShelfCollection();
            for (Shelf shelfCollectionOrphanCheckShelf : shelfCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sector (" + sector + ") cannot be destroyed since the Shelf " + shelfCollectionOrphanCheckShelf + " in its shelfCollection field has a non-nullable sectorId field.");
            }
            Collection<Category> categoryCollectionOrphanCheck = sector.getCategoryCollection();
            for (Category categoryCollectionOrphanCheckCategory : categoryCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sector (" + sector + ") cannot be destroyed since the Category " + categoryCollectionOrphanCheckCategory + " in its categoryCollection field has a non-nullable sectorId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Warehouse warehouseId = sector.getWarehouseId();
            if (warehouseId != null) {
                warehouseId.getSectorCollection().remove(sector);
                warehouseId = em.merge(warehouseId);
            }
            em.remove(sector);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Sector> findSectorEntities() {
        return findSectorEntities(true, -1, -1);
    }

    public List<Sector> findSectorEntities(int maxResults, int firstResult) {
        return findSectorEntities(false, maxResults, firstResult);
    }

    private List<Sector> findSectorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Sector.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Sector findSector(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Sector.class, id);
        } finally {
            em.close();
        }
    }

    public int getSectorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Sector> rt = cq.from(Sector.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
