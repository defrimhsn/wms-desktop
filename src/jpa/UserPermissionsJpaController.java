/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class UserPermissionsJpaController implements Serializable {

    public UserPermissionsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(UserPermissions userPermissions) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Permissions permissionsId = userPermissions.getPermissionsId();
            if (permissionsId != null) {
                permissionsId = em.getReference(permissionsId.getClass(), permissionsId.getId());
                userPermissions.setPermissionsId(permissionsId);
            }
            User userId = userPermissions.getUserId();
            if (userId != null) {
                userId = em.getReference(userId.getClass(), userId.getId());
                userPermissions.setUserId(userId);
            }
            em.persist(userPermissions);
            if (permissionsId != null) {
                permissionsId.getUserPermissionsCollection().add(userPermissions);
                permissionsId = em.merge(permissionsId);
            }
            if (userId != null) {
                userId.getUserPermissionsCollection().add(userPermissions);
                userId = em.merge(userId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UserPermissions userPermissions) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserPermissions persistentUserPermissions = em.find(UserPermissions.class, userPermissions.getId());
            Permissions permissionsIdOld = persistentUserPermissions.getPermissionsId();
            Permissions permissionsIdNew = userPermissions.getPermissionsId();
            User userIdOld = persistentUserPermissions.getUserId();
            User userIdNew = userPermissions.getUserId();
            if (permissionsIdNew != null) {
                permissionsIdNew = em.getReference(permissionsIdNew.getClass(), permissionsIdNew.getId());
                userPermissions.setPermissionsId(permissionsIdNew);
            }
            if (userIdNew != null) {
                userIdNew = em.getReference(userIdNew.getClass(), userIdNew.getId());
                userPermissions.setUserId(userIdNew);
            }
            userPermissions = em.merge(userPermissions);
            if (permissionsIdOld != null && !permissionsIdOld.equals(permissionsIdNew)) {
                permissionsIdOld.getUserPermissionsCollection().remove(userPermissions);
                permissionsIdOld = em.merge(permissionsIdOld);
            }
            if (permissionsIdNew != null && !permissionsIdNew.equals(permissionsIdOld)) {
                permissionsIdNew.getUserPermissionsCollection().add(userPermissions);
                permissionsIdNew = em.merge(permissionsIdNew);
            }
            if (userIdOld != null && !userIdOld.equals(userIdNew)) {
                userIdOld.getUserPermissionsCollection().remove(userPermissions);
                userIdOld = em.merge(userIdOld);
            }
            if (userIdNew != null && !userIdNew.equals(userIdOld)) {
                userIdNew.getUserPermissionsCollection().add(userPermissions);
                userIdNew = em.merge(userIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = userPermissions.getId();
                if (findUserPermissions(id) == null) {
                    throw new NonexistentEntityException("The userPermissions with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserPermissions userPermissions;
            try {
                userPermissions = em.getReference(UserPermissions.class, id);
                userPermissions.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The userPermissions with id " + id + " no longer exists.", enfe);
            }
            Permissions permissionsId = userPermissions.getPermissionsId();
            if (permissionsId != null) {
                permissionsId.getUserPermissionsCollection().remove(userPermissions);
                permissionsId = em.merge(permissionsId);
            }
            User userId = userPermissions.getUserId();
            if (userId != null) {
                userId.getUserPermissionsCollection().remove(userPermissions);
                userId = em.merge(userId);
            }
            em.remove(userPermissions);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UserPermissions> findUserPermissionsEntities() {
        return findUserPermissionsEntities(true, -1, -1);
    }

    public List<UserPermissions> findUserPermissionsEntities(int maxResults, int firstResult) {
        return findUserPermissionsEntities(false, maxResults, firstResult);
    }

    private List<UserPermissions> findUserPermissionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(UserPermissions.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UserPermissions findUserPermissions(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UserPermissions.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserPermissionsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<UserPermissions> rt = cq.from(UserPermissions.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
