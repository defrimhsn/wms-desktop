/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class ProductUnitJpaController implements Serializable {

    public ProductUnitJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ProductUnit productUnit) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Product productId = productUnit.getProductId();
            if (productId != null) {
                productId = em.getReference(productId.getClass(), productId.getId());
                productUnit.setProductId(productId);
            }
            Unit unitId = productUnit.getUnitId();
            if (unitId != null) {
                unitId = em.getReference(unitId.getClass(), unitId.getId());
                productUnit.setUnitId(unitId);
            }
            em.persist(productUnit);
            if (productId != null) {
                productId.getProductUnitCollection().add(productUnit);
                productId = em.merge(productId);
            }
            if (unitId != null) {
                unitId.getProductUnitCollection().add(productUnit);
                unitId = em.merge(unitId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ProductUnit productUnit) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProductUnit persistentProductUnit = em.find(ProductUnit.class, productUnit.getId());
            Product productIdOld = persistentProductUnit.getProductId();
            Product productIdNew = productUnit.getProductId();
            Unit unitIdOld = persistentProductUnit.getUnitId();
            Unit unitIdNew = productUnit.getUnitId();
            if (productIdNew != null) {
                productIdNew = em.getReference(productIdNew.getClass(), productIdNew.getId());
                productUnit.setProductId(productIdNew);
            }
            if (unitIdNew != null) {
                unitIdNew = em.getReference(unitIdNew.getClass(), unitIdNew.getId());
                productUnit.setUnitId(unitIdNew);
            }
            productUnit = em.merge(productUnit);
            if (productIdOld != null && !productIdOld.equals(productIdNew)) {
                productIdOld.getProductUnitCollection().remove(productUnit);
                productIdOld = em.merge(productIdOld);
            }
            if (productIdNew != null && !productIdNew.equals(productIdOld)) {
                productIdNew.getProductUnitCollection().add(productUnit);
                productIdNew = em.merge(productIdNew);
            }
            if (unitIdOld != null && !unitIdOld.equals(unitIdNew)) {
                unitIdOld.getProductUnitCollection().remove(productUnit);
                unitIdOld = em.merge(unitIdOld);
            }
            if (unitIdNew != null && !unitIdNew.equals(unitIdOld)) {
                unitIdNew.getProductUnitCollection().add(productUnit);
                unitIdNew = em.merge(unitIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = productUnit.getId();
                if (findProductUnit(id) == null) {
                    throw new NonexistentEntityException("The productUnit with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProductUnit productUnit;
            try {
                productUnit = em.getReference(ProductUnit.class, id);
                productUnit.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The productUnit with id " + id + " no longer exists.", enfe);
            }
            Product productId = productUnit.getProductId();
            if (productId != null) {
                productId.getProductUnitCollection().remove(productUnit);
                productId = em.merge(productId);
            }
            Unit unitId = productUnit.getUnitId();
            if (unitId != null) {
                unitId.getProductUnitCollection().remove(productUnit);
                unitId = em.merge(unitId);
            }
            em.remove(productUnit);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ProductUnit> findProductUnitEntities() {
        return findProductUnitEntities(true, -1, -1);
    }

    public List<ProductUnit> findProductUnitEntities(int maxResults, int firstResult) {
        return findProductUnitEntities(false, maxResults, firstResult);
    }

    private List<ProductUnit> findProductUnitEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ProductUnit.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ProductUnit findProductUnit(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ProductUnit.class, id);
        } finally {
            em.close();
        }
    }

    public int getProductUnitCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ProductUnit> rt = cq.from(ProductUnit.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
