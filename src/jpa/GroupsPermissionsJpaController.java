/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class GroupsPermissionsJpaController implements Serializable {

    public GroupsPermissionsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(GroupsPermissions groupsPermissions) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Groups groupsId = groupsPermissions.getGroupsId();
            if (groupsId != null) {
                groupsId = em.getReference(groupsId.getClass(), groupsId.getId());
                groupsPermissions.setGroupsId(groupsId);
            }
            Permissions permissionsId = groupsPermissions.getPermissionsId();
            if (permissionsId != null) {
                permissionsId = em.getReference(permissionsId.getClass(), permissionsId.getId());
                groupsPermissions.setPermissionsId(permissionsId);
            }
            em.persist(groupsPermissions);
            if (groupsId != null) {
                groupsId.getGroupsPermissionsCollection().add(groupsPermissions);
                groupsId = em.merge(groupsId);
            }
            if (permissionsId != null) {
                permissionsId.getGroupsPermissionsCollection().add(groupsPermissions);
                permissionsId = em.merge(permissionsId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(GroupsPermissions groupsPermissions) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            GroupsPermissions persistentGroupsPermissions = em.find(GroupsPermissions.class, groupsPermissions.getId());
            Groups groupsIdOld = persistentGroupsPermissions.getGroupsId();
            Groups groupsIdNew = groupsPermissions.getGroupsId();
            Permissions permissionsIdOld = persistentGroupsPermissions.getPermissionsId();
            Permissions permissionsIdNew = groupsPermissions.getPermissionsId();
            if (groupsIdNew != null) {
                groupsIdNew = em.getReference(groupsIdNew.getClass(), groupsIdNew.getId());
                groupsPermissions.setGroupsId(groupsIdNew);
            }
            if (permissionsIdNew != null) {
                permissionsIdNew = em.getReference(permissionsIdNew.getClass(), permissionsIdNew.getId());
                groupsPermissions.setPermissionsId(permissionsIdNew);
            }
            groupsPermissions = em.merge(groupsPermissions);
            if (groupsIdOld != null && !groupsIdOld.equals(groupsIdNew)) {
                groupsIdOld.getGroupsPermissionsCollection().remove(groupsPermissions);
                groupsIdOld = em.merge(groupsIdOld);
            }
            if (groupsIdNew != null && !groupsIdNew.equals(groupsIdOld)) {
                groupsIdNew.getGroupsPermissionsCollection().add(groupsPermissions);
                groupsIdNew = em.merge(groupsIdNew);
            }
            if (permissionsIdOld != null && !permissionsIdOld.equals(permissionsIdNew)) {
                permissionsIdOld.getGroupsPermissionsCollection().remove(groupsPermissions);
                permissionsIdOld = em.merge(permissionsIdOld);
            }
            if (permissionsIdNew != null && !permissionsIdNew.equals(permissionsIdOld)) {
                permissionsIdNew.getGroupsPermissionsCollection().add(groupsPermissions);
                permissionsIdNew = em.merge(permissionsIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = groupsPermissions.getId();
                if (findGroupsPermissions(id) == null) {
                    throw new NonexistentEntityException("The groupsPermissions with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            GroupsPermissions groupsPermissions;
            try {
                groupsPermissions = em.getReference(GroupsPermissions.class, id);
                groupsPermissions.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The groupsPermissions with id " + id + " no longer exists.", enfe);
            }
            Groups groupsId = groupsPermissions.getGroupsId();
            if (groupsId != null) {
                groupsId.getGroupsPermissionsCollection().remove(groupsPermissions);
                groupsId = em.merge(groupsId);
            }
            Permissions permissionsId = groupsPermissions.getPermissionsId();
            if (permissionsId != null) {
                permissionsId.getGroupsPermissionsCollection().remove(groupsPermissions);
                permissionsId = em.merge(permissionsId);
            }
            em.remove(groupsPermissions);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<GroupsPermissions> findGroupsPermissionsEntities() {
        return findGroupsPermissionsEntities(true, -1, -1);
    }

    public List<GroupsPermissions> findGroupsPermissionsEntities(int maxResults, int firstResult) {
        return findGroupsPermissionsEntities(false, maxResults, firstResult);
    }

    private List<GroupsPermissions> findGroupsPermissionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(GroupsPermissions.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public GroupsPermissions findGroupsPermissions(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(GroupsPermissions.class, id);
        } finally {
            em.close();
        }
    }

    public int getGroupsPermissionsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<GroupsPermissions> rt = cq.from(GroupsPermissions.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
