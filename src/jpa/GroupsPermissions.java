/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Deathrow
 */
@Entity
@Table(name = "groups_permissions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GroupsPermissions.findAll", query = "SELECT g FROM GroupsPermissions g"),
    @NamedQuery(name = "GroupsPermissions.findById", query = "SELECT g FROM GroupsPermissions g WHERE g.id = :id")})
public class GroupsPermissions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "groups_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Groups groupsId;
    @JoinColumn(name = "permissions_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Permissions permissionsId;

    public GroupsPermissions() {
    }

    public GroupsPermissions(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Groups getGroupsId() {
        return groupsId;
    }

    public void setGroupsId(Groups groupsId) {
        this.groupsId = groupsId;
    }

    public Permissions getPermissionsId() {
        return permissionsId;
    }

    public void setPermissionsId(Permissions permissionsId) {
        this.permissionsId = permissionsId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupsPermissions)) {
            return false;
        }
        GroupsPermissions other = (GroupsPermissions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.GroupsPermissions[ id=" + id + " ]";
    }

}
