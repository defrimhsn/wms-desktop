/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class UserJpaController implements Serializable {

    public UserJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(User user) {
        if (user.getUserPermissionsCollection() == null) {
            user.setUserPermissionsCollection(new ArrayList<UserPermissions>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Groups groupId = user.getGroupId();
            if (groupId != null) {
                groupId = em.getReference(groupId.getClass(), groupId.getId());
                user.setGroupId(groupId);
            }
            Collection<UserPermissions> attachedUserPermissionsCollection = new ArrayList<UserPermissions>();
            for (UserPermissions userPermissionsCollectionUserPermissionsToAttach : user.getUserPermissionsCollection()) {
                userPermissionsCollectionUserPermissionsToAttach = em.getReference(userPermissionsCollectionUserPermissionsToAttach.getClass(), userPermissionsCollectionUserPermissionsToAttach.getId());
                attachedUserPermissionsCollection.add(userPermissionsCollectionUserPermissionsToAttach);
            }
            user.setUserPermissionsCollection(attachedUserPermissionsCollection);
            em.persist(user);
            if (groupId != null) {
                groupId.getUserCollection().add(user);
                groupId = em.merge(groupId);
            }
            for (UserPermissions userPermissionsCollectionUserPermissions : user.getUserPermissionsCollection()) {
                User oldUserIdOfUserPermissionsCollectionUserPermissions = userPermissionsCollectionUserPermissions.getUserId();
                userPermissionsCollectionUserPermissions.setUserId(user);
                userPermissionsCollectionUserPermissions = em.merge(userPermissionsCollectionUserPermissions);
                if (oldUserIdOfUserPermissionsCollectionUserPermissions != null) {
                    oldUserIdOfUserPermissionsCollectionUserPermissions.getUserPermissionsCollection().remove(userPermissionsCollectionUserPermissions);
                    oldUserIdOfUserPermissionsCollectionUserPermissions = em.merge(oldUserIdOfUserPermissionsCollectionUserPermissions);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(User user) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            User persistentUser = em.find(User.class, user.getId());
            Groups groupIdOld = persistentUser.getGroupId();
            Groups groupIdNew = user.getGroupId();
            Collection<UserPermissions> userPermissionsCollectionOld = persistentUser.getUserPermissionsCollection();
            Collection<UserPermissions> userPermissionsCollectionNew = user.getUserPermissionsCollection();
            List<String> illegalOrphanMessages = null;
            for (UserPermissions userPermissionsCollectionOldUserPermissions : userPermissionsCollectionOld) {
                if (!userPermissionsCollectionNew.contains(userPermissionsCollectionOldUserPermissions)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UserPermissions " + userPermissionsCollectionOldUserPermissions + " since its userId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (groupIdNew != null) {
                groupIdNew = em.getReference(groupIdNew.getClass(), groupIdNew.getId());
                user.setGroupId(groupIdNew);
            }
            Collection<UserPermissions> attachedUserPermissionsCollectionNew = new ArrayList<UserPermissions>();
            for (UserPermissions userPermissionsCollectionNewUserPermissionsToAttach : userPermissionsCollectionNew) {
                userPermissionsCollectionNewUserPermissionsToAttach = em.getReference(userPermissionsCollectionNewUserPermissionsToAttach.getClass(), userPermissionsCollectionNewUserPermissionsToAttach.getId());
                attachedUserPermissionsCollectionNew.add(userPermissionsCollectionNewUserPermissionsToAttach);
            }
            userPermissionsCollectionNew = attachedUserPermissionsCollectionNew;
            user.setUserPermissionsCollection(userPermissionsCollectionNew);
            user = em.merge(user);
            if (groupIdOld != null && !groupIdOld.equals(groupIdNew)) {
                groupIdOld.getUserCollection().remove(user);
                groupIdOld = em.merge(groupIdOld);
            }
            if (groupIdNew != null && !groupIdNew.equals(groupIdOld)) {
                groupIdNew.getUserCollection().add(user);
                groupIdNew = em.merge(groupIdNew);
            }
            for (UserPermissions userPermissionsCollectionNewUserPermissions : userPermissionsCollectionNew) {
                if (!userPermissionsCollectionOld.contains(userPermissionsCollectionNewUserPermissions)) {
                    User oldUserIdOfUserPermissionsCollectionNewUserPermissions = userPermissionsCollectionNewUserPermissions.getUserId();
                    userPermissionsCollectionNewUserPermissions.setUserId(user);
                    userPermissionsCollectionNewUserPermissions = em.merge(userPermissionsCollectionNewUserPermissions);
                    if (oldUserIdOfUserPermissionsCollectionNewUserPermissions != null && !oldUserIdOfUserPermissionsCollectionNewUserPermissions.equals(user)) {
                        oldUserIdOfUserPermissionsCollectionNewUserPermissions.getUserPermissionsCollection().remove(userPermissionsCollectionNewUserPermissions);
                        oldUserIdOfUserPermissionsCollectionNewUserPermissions = em.merge(oldUserIdOfUserPermissionsCollectionNewUserPermissions);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = user.getId();
                if (findUser(id) == null) {
                    throw new NonexistentEntityException("The user with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            User user;
            try {
                user = em.getReference(User.class, id);
                user.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The user with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<UserPermissions> userPermissionsCollectionOrphanCheck = user.getUserPermissionsCollection();
            for (UserPermissions userPermissionsCollectionOrphanCheckUserPermissions : userPermissionsCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This User (" + user + ") cannot be destroyed since the UserPermissions " + userPermissionsCollectionOrphanCheckUserPermissions + " in its userPermissionsCollection field has a non-nullable userId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Groups groupId = user.getGroupId();
            if (groupId != null) {
                groupId.getUserCollection().remove(user);
                groupId = em.merge(groupId);
            }
            em.remove(user);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<User> findUserEntities() {
        return findUserEntities(true, -1, -1);
    }

    public List<User> findUserEntities(int maxResults, int firstResult) {
        return findUserEntities(false, maxResults, firstResult);
    }

    private List<User> findUserEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(User.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public User findUser(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(User.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<User> rt = cq.from(User.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
