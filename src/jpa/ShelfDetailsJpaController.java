/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class ShelfDetailsJpaController implements Serializable {

    public ShelfDetailsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ShelfDetails shelfDetails) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Shelf shelfId = shelfDetails.getShelfId();
            if (shelfId != null) {
                shelfId = em.getReference(shelfId.getClass(), shelfId.getId());
                shelfDetails.setShelfId(shelfId);
            }
            em.persist(shelfDetails);
            if (shelfId != null) {
                shelfId.getShelfDetailsCollection().add(shelfDetails);
                shelfId = em.merge(shelfId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ShelfDetails shelfDetails) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ShelfDetails persistentShelfDetails = em.find(ShelfDetails.class, shelfDetails.getId());
            Shelf shelfIdOld = persistentShelfDetails.getShelfId();
            Shelf shelfIdNew = shelfDetails.getShelfId();
            if (shelfIdNew != null) {
                shelfIdNew = em.getReference(shelfIdNew.getClass(), shelfIdNew.getId());
                shelfDetails.setShelfId(shelfIdNew);
            }
            shelfDetails = em.merge(shelfDetails);
            if (shelfIdOld != null && !shelfIdOld.equals(shelfIdNew)) {
                shelfIdOld.getShelfDetailsCollection().remove(shelfDetails);
                shelfIdOld = em.merge(shelfIdOld);
            }
            if (shelfIdNew != null && !shelfIdNew.equals(shelfIdOld)) {
                shelfIdNew.getShelfDetailsCollection().add(shelfDetails);
                shelfIdNew = em.merge(shelfIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = shelfDetails.getId();
                if (findShelfDetails(id) == null) {
                    throw new NonexistentEntityException("The shelfDetails with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ShelfDetails shelfDetails;
            try {
                shelfDetails = em.getReference(ShelfDetails.class, id);
                shelfDetails.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The shelfDetails with id " + id + " no longer exists.", enfe);
            }
            Shelf shelfId = shelfDetails.getShelfId();
            if (shelfId != null) {
                shelfId.getShelfDetailsCollection().remove(shelfDetails);
                shelfId = em.merge(shelfId);
            }
            em.remove(shelfDetails);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ShelfDetails> findShelfDetailsEntities() {
        return findShelfDetailsEntities(true, -1, -1);
    }

    public List<ShelfDetails> findShelfDetailsEntities(int maxResults, int firstResult) {
        return findShelfDetailsEntities(false, maxResults, firstResult);
    }

    private List<ShelfDetails> findShelfDetailsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ShelfDetails.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ShelfDetails findShelfDetails(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ShelfDetails.class, id);
        } finally {
            em.close();
        }
    }

    public int getShelfDetailsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ShelfDetails> rt = cq.from(ShelfDetails.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
