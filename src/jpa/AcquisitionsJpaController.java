/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class AcquisitionsJpaController implements Serializable {

    public AcquisitionsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Acquisitions acquisitions) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Product productId = acquisitions.getProductId();
            if (productId != null) {
                productId = em.getReference(productId.getClass(), productId.getId());
                acquisitions.setProductId(productId);
            }
            em.persist(acquisitions);
            if (productId != null) {
                productId.getAcquisitionsCollection().add(acquisitions);
                productId = em.merge(productId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Acquisitions acquisitions) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Acquisitions persistentAcquisitions = em.find(Acquisitions.class, acquisitions.getId());
            Product productIdOld = persistentAcquisitions.getProductId();
            Product productIdNew = acquisitions.getProductId();
            if (productIdNew != null) {
                productIdNew = em.getReference(productIdNew.getClass(), productIdNew.getId());
                acquisitions.setProductId(productIdNew);
            }
            acquisitions = em.merge(acquisitions);
            if (productIdOld != null && !productIdOld.equals(productIdNew)) {
                productIdOld.getAcquisitionsCollection().remove(acquisitions);
                productIdOld = em.merge(productIdOld);
            }
            if (productIdNew != null && !productIdNew.equals(productIdOld)) {
                productIdNew.getAcquisitionsCollection().add(acquisitions);
                productIdNew = em.merge(productIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = acquisitions.getId();
                if (findAcquisitions(id) == null) {
                    throw new NonexistentEntityException("The acquisitions with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Acquisitions acquisitions;
            try {
                acquisitions = em.getReference(Acquisitions.class, id);
                acquisitions.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The acquisitions with id " + id + " no longer exists.", enfe);
            }
            Product productId = acquisitions.getProductId();
            if (productId != null) {
                productId.getAcquisitionsCollection().remove(acquisitions);
                productId = em.merge(productId);
            }
            em.remove(acquisitions);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Acquisitions> findAcquisitionsEntities() {
        return findAcquisitionsEntities(true, -1, -1);
    }

    public List<Acquisitions> findAcquisitionsEntities(int maxResults, int firstResult) {
        return findAcquisitionsEntities(false, maxResults, firstResult);
    }

    private List<Acquisitions> findAcquisitionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Acquisitions.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Acquisitions findAcquisitions(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Acquisitions.class, id);
        } finally {
            em.close();
        }
    }

    public int getAcquisitionsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Acquisitions> rt = cq.from(Acquisitions.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
