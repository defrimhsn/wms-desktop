/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class PermissionsJpaController implements Serializable {

    public PermissionsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Permissions permissions) {
        if (permissions.getUserPermissionsCollection() == null) {
            permissions.setUserPermissionsCollection(new ArrayList<UserPermissions>());
        }
        if (permissions.getGroupsPermissionsCollection() == null) {
            permissions.setGroupsPermissionsCollection(new ArrayList<GroupsPermissions>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<UserPermissions> attachedUserPermissionsCollection = new ArrayList<UserPermissions>();
            for (UserPermissions userPermissionsCollectionUserPermissionsToAttach : permissions.getUserPermissionsCollection()) {
                userPermissionsCollectionUserPermissionsToAttach = em.getReference(userPermissionsCollectionUserPermissionsToAttach.getClass(), userPermissionsCollectionUserPermissionsToAttach.getId());
                attachedUserPermissionsCollection.add(userPermissionsCollectionUserPermissionsToAttach);
            }
            permissions.setUserPermissionsCollection(attachedUserPermissionsCollection);
            Collection<GroupsPermissions> attachedGroupsPermissionsCollection = new ArrayList<GroupsPermissions>();
            for (GroupsPermissions groupsPermissionsCollectionGroupsPermissionsToAttach : permissions.getGroupsPermissionsCollection()) {
                groupsPermissionsCollectionGroupsPermissionsToAttach = em.getReference(groupsPermissionsCollectionGroupsPermissionsToAttach.getClass(), groupsPermissionsCollectionGroupsPermissionsToAttach.getId());
                attachedGroupsPermissionsCollection.add(groupsPermissionsCollectionGroupsPermissionsToAttach);
            }
            permissions.setGroupsPermissionsCollection(attachedGroupsPermissionsCollection);
            em.persist(permissions);
            for (UserPermissions userPermissionsCollectionUserPermissions : permissions.getUserPermissionsCollection()) {
                Permissions oldPermissionsIdOfUserPermissionsCollectionUserPermissions = userPermissionsCollectionUserPermissions.getPermissionsId();
                userPermissionsCollectionUserPermissions.setPermissionsId(permissions);
                userPermissionsCollectionUserPermissions = em.merge(userPermissionsCollectionUserPermissions);
                if (oldPermissionsIdOfUserPermissionsCollectionUserPermissions != null) {
                    oldPermissionsIdOfUserPermissionsCollectionUserPermissions.getUserPermissionsCollection().remove(userPermissionsCollectionUserPermissions);
                    oldPermissionsIdOfUserPermissionsCollectionUserPermissions = em.merge(oldPermissionsIdOfUserPermissionsCollectionUserPermissions);
                }
            }
            for (GroupsPermissions groupsPermissionsCollectionGroupsPermissions : permissions.getGroupsPermissionsCollection()) {
                Permissions oldPermissionsIdOfGroupsPermissionsCollectionGroupsPermissions = groupsPermissionsCollectionGroupsPermissions.getPermissionsId();
                groupsPermissionsCollectionGroupsPermissions.setPermissionsId(permissions);
                groupsPermissionsCollectionGroupsPermissions = em.merge(groupsPermissionsCollectionGroupsPermissions);
                if (oldPermissionsIdOfGroupsPermissionsCollectionGroupsPermissions != null) {
                    oldPermissionsIdOfGroupsPermissionsCollectionGroupsPermissions.getGroupsPermissionsCollection().remove(groupsPermissionsCollectionGroupsPermissions);
                    oldPermissionsIdOfGroupsPermissionsCollectionGroupsPermissions = em.merge(oldPermissionsIdOfGroupsPermissionsCollectionGroupsPermissions);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Permissions permissions) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Permissions persistentPermissions = em.find(Permissions.class, permissions.getId());
            Collection<UserPermissions> userPermissionsCollectionOld = persistentPermissions.getUserPermissionsCollection();
            Collection<UserPermissions> userPermissionsCollectionNew = permissions.getUserPermissionsCollection();
            Collection<GroupsPermissions> groupsPermissionsCollectionOld = persistentPermissions.getGroupsPermissionsCollection();
            Collection<GroupsPermissions> groupsPermissionsCollectionNew = permissions.getGroupsPermissionsCollection();
            List<String> illegalOrphanMessages = null;
            for (UserPermissions userPermissionsCollectionOldUserPermissions : userPermissionsCollectionOld) {
                if (!userPermissionsCollectionNew.contains(userPermissionsCollectionOldUserPermissions)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UserPermissions " + userPermissionsCollectionOldUserPermissions + " since its permissionsId field is not nullable.");
                }
            }
            for (GroupsPermissions groupsPermissionsCollectionOldGroupsPermissions : groupsPermissionsCollectionOld) {
                if (!groupsPermissionsCollectionNew.contains(groupsPermissionsCollectionOldGroupsPermissions)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain GroupsPermissions " + groupsPermissionsCollectionOldGroupsPermissions + " since its permissionsId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<UserPermissions> attachedUserPermissionsCollectionNew = new ArrayList<UserPermissions>();
            for (UserPermissions userPermissionsCollectionNewUserPermissionsToAttach : userPermissionsCollectionNew) {
                userPermissionsCollectionNewUserPermissionsToAttach = em.getReference(userPermissionsCollectionNewUserPermissionsToAttach.getClass(), userPermissionsCollectionNewUserPermissionsToAttach.getId());
                attachedUserPermissionsCollectionNew.add(userPermissionsCollectionNewUserPermissionsToAttach);
            }
            userPermissionsCollectionNew = attachedUserPermissionsCollectionNew;
            permissions.setUserPermissionsCollection(userPermissionsCollectionNew);
            Collection<GroupsPermissions> attachedGroupsPermissionsCollectionNew = new ArrayList<GroupsPermissions>();
            for (GroupsPermissions groupsPermissionsCollectionNewGroupsPermissionsToAttach : groupsPermissionsCollectionNew) {
                groupsPermissionsCollectionNewGroupsPermissionsToAttach = em.getReference(groupsPermissionsCollectionNewGroupsPermissionsToAttach.getClass(), groupsPermissionsCollectionNewGroupsPermissionsToAttach.getId());
                attachedGroupsPermissionsCollectionNew.add(groupsPermissionsCollectionNewGroupsPermissionsToAttach);
            }
            groupsPermissionsCollectionNew = attachedGroupsPermissionsCollectionNew;
            permissions.setGroupsPermissionsCollection(groupsPermissionsCollectionNew);
            permissions = em.merge(permissions);
            for (UserPermissions userPermissionsCollectionNewUserPermissions : userPermissionsCollectionNew) {
                if (!userPermissionsCollectionOld.contains(userPermissionsCollectionNewUserPermissions)) {
                    Permissions oldPermissionsIdOfUserPermissionsCollectionNewUserPermissions = userPermissionsCollectionNewUserPermissions.getPermissionsId();
                    userPermissionsCollectionNewUserPermissions.setPermissionsId(permissions);
                    userPermissionsCollectionNewUserPermissions = em.merge(userPermissionsCollectionNewUserPermissions);
                    if (oldPermissionsIdOfUserPermissionsCollectionNewUserPermissions != null && !oldPermissionsIdOfUserPermissionsCollectionNewUserPermissions.equals(permissions)) {
                        oldPermissionsIdOfUserPermissionsCollectionNewUserPermissions.getUserPermissionsCollection().remove(userPermissionsCollectionNewUserPermissions);
                        oldPermissionsIdOfUserPermissionsCollectionNewUserPermissions = em.merge(oldPermissionsIdOfUserPermissionsCollectionNewUserPermissions);
                    }
                }
            }
            for (GroupsPermissions groupsPermissionsCollectionNewGroupsPermissions : groupsPermissionsCollectionNew) {
                if (!groupsPermissionsCollectionOld.contains(groupsPermissionsCollectionNewGroupsPermissions)) {
                    Permissions oldPermissionsIdOfGroupsPermissionsCollectionNewGroupsPermissions = groupsPermissionsCollectionNewGroupsPermissions.getPermissionsId();
                    groupsPermissionsCollectionNewGroupsPermissions.setPermissionsId(permissions);
                    groupsPermissionsCollectionNewGroupsPermissions = em.merge(groupsPermissionsCollectionNewGroupsPermissions);
                    if (oldPermissionsIdOfGroupsPermissionsCollectionNewGroupsPermissions != null && !oldPermissionsIdOfGroupsPermissionsCollectionNewGroupsPermissions.equals(permissions)) {
                        oldPermissionsIdOfGroupsPermissionsCollectionNewGroupsPermissions.getGroupsPermissionsCollection().remove(groupsPermissionsCollectionNewGroupsPermissions);
                        oldPermissionsIdOfGroupsPermissionsCollectionNewGroupsPermissions = em.merge(oldPermissionsIdOfGroupsPermissionsCollectionNewGroupsPermissions);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = permissions.getId();
                if (findPermissions(id) == null) {
                    throw new NonexistentEntityException("The permissions with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Permissions permissions;
            try {
                permissions = em.getReference(Permissions.class, id);
                permissions.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The permissions with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<UserPermissions> userPermissionsCollectionOrphanCheck = permissions.getUserPermissionsCollection();
            for (UserPermissions userPermissionsCollectionOrphanCheckUserPermissions : userPermissionsCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Permissions (" + permissions + ") cannot be destroyed since the UserPermissions " + userPermissionsCollectionOrphanCheckUserPermissions + " in its userPermissionsCollection field has a non-nullable permissionsId field.");
            }
            Collection<GroupsPermissions> groupsPermissionsCollectionOrphanCheck = permissions.getGroupsPermissionsCollection();
            for (GroupsPermissions groupsPermissionsCollectionOrphanCheckGroupsPermissions : groupsPermissionsCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Permissions (" + permissions + ") cannot be destroyed since the GroupsPermissions " + groupsPermissionsCollectionOrphanCheckGroupsPermissions + " in its groupsPermissionsCollection field has a non-nullable permissionsId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(permissions);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Permissions> findPermissionsEntities() {
        return findPermissionsEntities(true, -1, -1);
    }

    public List<Permissions> findPermissionsEntities(int maxResults, int firstResult) {
        return findPermissionsEntities(false, maxResults, firstResult);
    }

    private List<Permissions> findPermissionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Permissions.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Permissions findPermissions(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Permissions.class, id);
        } finally {
            em.close();
        }
    }

    public int getPermissionsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Permissions> rt = cq.from(Permissions.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
