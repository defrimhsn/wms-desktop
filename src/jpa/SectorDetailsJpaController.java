/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class SectorDetailsJpaController implements Serializable {

    public SectorDetailsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(SectorDetails sectorDetails) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sector sectorId = sectorDetails.getSectorId();
            if (sectorId != null) {
                sectorId = em.getReference(sectorId.getClass(), sectorId.getId());
                sectorDetails.setSectorId(sectorId);
            }
            em.persist(sectorDetails);
            if (sectorId != null) {
                sectorId.getSectorDetailsCollection().add(sectorDetails);
                sectorId = em.merge(sectorId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SectorDetails sectorDetails) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SectorDetails persistentSectorDetails = em.find(SectorDetails.class, sectorDetails.getId());
            Sector sectorIdOld = persistentSectorDetails.getSectorId();
            Sector sectorIdNew = sectorDetails.getSectorId();
            if (sectorIdNew != null) {
                sectorIdNew = em.getReference(sectorIdNew.getClass(), sectorIdNew.getId());
                sectorDetails.setSectorId(sectorIdNew);
            }
            sectorDetails = em.merge(sectorDetails);
            if (sectorIdOld != null && !sectorIdOld.equals(sectorIdNew)) {
                sectorIdOld.getSectorDetailsCollection().remove(sectorDetails);
                sectorIdOld = em.merge(sectorIdOld);
            }
            if (sectorIdNew != null && !sectorIdNew.equals(sectorIdOld)) {
                sectorIdNew.getSectorDetailsCollection().add(sectorDetails);
                sectorIdNew = em.merge(sectorIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = sectorDetails.getId();
                if (findSectorDetails(id) == null) {
                    throw new NonexistentEntityException("The sectorDetails with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SectorDetails sectorDetails;
            try {
                sectorDetails = em.getReference(SectorDetails.class, id);
                sectorDetails.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sectorDetails with id " + id + " no longer exists.", enfe);
            }
            Sector sectorId = sectorDetails.getSectorId();
            if (sectorId != null) {
                sectorId.getSectorDetailsCollection().remove(sectorDetails);
                sectorId = em.merge(sectorId);
            }
            em.remove(sectorDetails);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SectorDetails> findSectorDetailsEntities() {
        return findSectorDetailsEntities(true, -1, -1);
    }

    public List<SectorDetails> findSectorDetailsEntities(int maxResults, int firstResult) {
        return findSectorDetailsEntities(false, maxResults, firstResult);
    }

    private List<SectorDetails> findSectorDetailsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(SectorDetails.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SectorDetails findSectorDetails(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SectorDetails.class, id);
        } finally {
            em.close();
        }
    }

    public int getSectorDetailsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<SectorDetails> rt = cq.from(SectorDetails.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
