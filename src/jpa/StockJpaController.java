/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import jpa.exceptions.NonexistentEntityException;

/**
 *
 * @author Deathrow
 */
public class StockJpaController implements Serializable {

    public StockJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Stock stock) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Product productId = stock.getProductId();
            if (productId != null) {
                productId = em.getReference(productId.getClass(), productId.getId());
                stock.setProductId(productId);
            }
            Unit unitId = stock.getUnitId();
            if (unitId != null) {
                unitId = em.getReference(unitId.getClass(), unitId.getId());
                stock.setUnitId(unitId);
            }
            em.persist(stock);
            if (productId != null) {
                productId.getStockCollection().add(stock);
                productId = em.merge(productId);
            }
            if (unitId != null) {
                unitId.getStockCollection().add(stock);
                unitId = em.merge(unitId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Stock stock) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Stock persistentStock = em.find(Stock.class, stock.getId());
            Product productIdOld = persistentStock.getProductId();
            Product productIdNew = stock.getProductId();
            Unit unitIdOld = persistentStock.getUnitId();
            Unit unitIdNew = stock.getUnitId();
            if (productIdNew != null) {
                productIdNew = em.getReference(productIdNew.getClass(), productIdNew.getId());
                stock.setProductId(productIdNew);
            }
            if (unitIdNew != null) {
                unitIdNew = em.getReference(unitIdNew.getClass(), unitIdNew.getId());
                stock.setUnitId(unitIdNew);
            }
            stock = em.merge(stock);
            if (productIdOld != null && !productIdOld.equals(productIdNew)) {
                productIdOld.getStockCollection().remove(stock);
                productIdOld = em.merge(productIdOld);
            }
            if (productIdNew != null && !productIdNew.equals(productIdOld)) {
                productIdNew.getStockCollection().add(stock);
                productIdNew = em.merge(productIdNew);
            }
            if (unitIdOld != null && !unitIdOld.equals(unitIdNew)) {
                unitIdOld.getStockCollection().remove(stock);
                unitIdOld = em.merge(unitIdOld);
            }
            if (unitIdNew != null && !unitIdNew.equals(unitIdOld)) {
                unitIdNew.getStockCollection().add(stock);
                unitIdNew = em.merge(unitIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = stock.getId();
                if (findStock(id) == null) {
                    throw new NonexistentEntityException("The stock with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Stock stock;
            try {
                stock = em.getReference(Stock.class, id);
                stock.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The stock with id " + id + " no longer exists.", enfe);
            }
            Product productId = stock.getProductId();
            if (productId != null) {
                productId.getStockCollection().remove(stock);
                productId = em.merge(productId);
            }
            Unit unitId = stock.getUnitId();
            if (unitId != null) {
                unitId.getStockCollection().remove(stock);
                unitId = em.merge(unitId);
            }
            em.remove(stock);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Stock> findStockEntities() {
        return findStockEntities(true, -1, -1);
    }

    public List<Stock> findStockEntities(int maxResults, int firstResult) {
        return findStockEntities(false, maxResults, firstResult);
    }

    private List<Stock> findStockEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Stock.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Stock findStock(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Stock.class, id);
        } finally {
            em.close();
        }
    }

    public int getStockCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Stock> rt = cq.from(Stock.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
