/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Deathrow
 */
@Entity
@Table(name = "sector_details")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SectorDetails.findAll", query = "SELECT s FROM SectorDetails s"),
    @NamedQuery(name = "SectorDetails.findById", query = "SELECT s FROM SectorDetails s WHERE s.id = :id"),
    @NamedQuery(name = "SectorDetails.findByName", query = "SELECT s FROM SectorDetails s WHERE s.name = :name"),
    @NamedQuery(name = "SectorDetails.findByValue", query = "SELECT s FROM SectorDetails s WHERE s.value = :value"),
    @NamedQuery(name = "SectorDetails.findByCreatedDate", query = "SELECT s FROM SectorDetails s WHERE s.createdDate = :createdDate"),
    @NamedQuery(name = "SectorDetails.findByUpdatedDate", query = "SELECT s FROM SectorDetails s WHERE s.updatedDate = :updatedDate"),
    @NamedQuery(name = "SectorDetails.findByIsActive", query = "SELECT s FROM SectorDetails s WHERE s.isActive = :isActive")})
public class SectorDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "value")
    private String value;
    @Basic(optional = false)
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "updated_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Basic(optional = false)
    @Column(name = "is_active")
    private int isActive;
    @JoinColumn(name = "sector_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Sector sectorId;

    public SectorDetails() {
    }

    public SectorDetails(Integer id) {
        this.id = id;
    }

    public SectorDetails(Integer id, String name, String value, Date createdDate, int isActive) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.createdDate = createdDate;
        this.isActive = isActive;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public Sector getSectorId() {
        return sectorId;
    }

    public void setSectorId(Sector sectorId) {
        this.sectorId = sectorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SectorDetails)) {
            return false;
        }
        SectorDetails other = (SectorDetails) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.SectorDetails[ id=" + id + " ]";
    }

}
